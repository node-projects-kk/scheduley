const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const multer = require('multer');

const twitterController = require('socialfeed/controllers/twitter.api.controller');

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		//console.log("12>>",file);
		if ((file.fieldname).indexOf('image') > -1) {
			// check if directory exists
			if (!fs.existsSync('./public/uploads/tweet')) {
				// if not create directory
				fs.mkdirSync('./public/uploads/tweet');
			}
			cb(null, './public/uploads/tweet')
		}
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
	}
})
const request_param = multer({
	storage: storage
});


namedRouter.post("api.twitter", '/twitter/tweet', request_param.any(), function (req, res) {
	twitterController.tweet(req).then(function (success) {
			res.status(success.status).send(success);
		},
		function (failure) {
			res.status(failure.status).send(failure);
		});
});

module.exports = router;