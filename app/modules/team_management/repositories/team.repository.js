const Team = require('team_management/models/team.model');
const perPage = config.PAGINATION_PERPAGE;

class TeamRepository {
	constructor() {}

	async getAll(searchQuery, sortOrder = {
		'_id': -1
	}, page) {
		try {
			const query = [{
				"isDeleted": false,
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'firstName': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};

			const aggregate = Team.aggregate([{
					"$sort": sortOrder
				},
				{
					$project: {
						_id: "$_id",
						firstName: "$firstName",
						lastName: "$lastName",
						image: "$image",
						status: "$status",
						designation: "$designation",
						facebook: "$facebook",
						instagram: "$instagram",
						email: "$email",
						isDeleted: "$isDeleted",
					}
				},
				{
					$match: searchQuery
				},
			]);

			return await Team.aggregatePaginate(aggregate, {
				page: page,
				limit: perPage
			}); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			return await Team.create(data).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await Team.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Team.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Team.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllActiveTeamMembers(searchQuery, sortOrder = {
		'_id': -1
	}, page) {
		try {
			const query = [{
				"isDeleted": false,
				"status": "Active"
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'firstName': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};
			const aggregate = Team.aggregate([{
					"$sort": sortOrder
				},
				{
					$project: {
						_id: "$_id",
						firstName: "$firstName",
						lastName: "$lastName",
						image: "$image",
						status: "$status",
						designation: "$designation",
						facebook: "$facebook",
						instagram: "$instagram",
						email: "$email",
						isDeleted: "$isDeleted",
					}
				},
				{
					$match: searchQuery
				},
			]);

			return await Team.aggregatePaginate(aggregate, {
				page: page,
				limit: perPage
			}); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async getAllTeam() {
		try {
			return await Team.find().lean().exec();
		} catch (error) {
			return error;
		}
	}


}

module.exports = new TeamRepository();