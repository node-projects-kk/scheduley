const userReportRepo = require('user_report/repositories/userreport.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');
var moment = require('moment');



exports.userChart = async req => {
	try {
		let from = req.params.from;
		let to = req.params.to;
		let condition = {};
		if (req.params.status == "filter") {
			condition = {
				$and: [{
						"date": {
							$gte: from
						}
					},
					{
						"date": {
							$lte: to
						}
					}
				]
			}
		} else {
			condition = {
				"date": {
					$lte: to
				}
			}
		}
		/* var currentyear = moment().format('YYYY');
		var currentMonth = 0; */
		const responseObj = {};
		/* let year = currentyear;
		if (req.params.year) {
			year = req.params.year;
		} */
		const newUsers = await userReportRepo.getAllNewUser(condition);
		responseObj.new_user_count = newUsers;

		const planRevenue = await userReportRepo.getTotalRevenue(condition);
		responseObj.planRevenue = planRevenue;
		/* 		let promoYear = currentyear;
				if (req.params.promoYear) {
					promoYear = req.params.promoYear;
				} */
		const promoDetails = await userReportRepo.getPromoCodeUsageStatus(condition);
		responseObj.promo_usage_count = promoDetails;
		/* responseObj.promoYear = promoYear; */
		/* let trialUserDetails = await userReportRepo.getTrialUserCount({
					'role_info.role': {
						$ne: 'admin'
					},
					"year": paymentYear
				}); */
		let trialUserDetails = await userReportRepo.getTrialUserCount(condition);
		/* trialUserDetails = await userReportRepo.getTrialUserCount(paymentYear, paymentMonth); */
		responseObj.trial_user_count = trialUserDetails;
		/* const paidUserDetails = await userReportRepo.getPaidUserCount(); */
		const activeUserCount = await userReportRepo.getActiveUsers();
		responseObj.activeUserCount = activeUserCount.length;
		//responseObj.paid_user_count = paidUserDetails;
		/* responseObj.subscriptionYear = currentyear;
		if (req.params.subscriptionYear) {
			responseObj.subscriptionYear = req.params.subscriptionYear;
		} */
		const transactionDetails = await userReportRepo.getTransactionDetails(condition);
		let planDetails = [];
		let statusPlan = "-";
		let temp = "";
		// console.log(transactionDetails)
		transactionDetails.forEach((items, index) => {
			temp = "N/A";
			if (items.plan_title.length > 1) {
				if (items.plan_price[items.plan_price.length - 2] > items.plan_price[items.plan_price.length - 1]) {
					statusPlan = "Downgrade";
				} else if (items.plan_price[items.plan_price.length - 2] < items.plan_price[items.plan_price.length - 1]) {
					statusPlan = "Upgrade";
				}
				temp = items.plan_title[items.plan_title.length - 2];
			}
			planDetails[index] = {
				planFrom: temp,
				planTo: items.plan_title[items.plan_title.length - 1],
				name: items.firstName + ' ' + items.lastName,
				email: items.email,
				status: statusPlan
			}
		});

		responseObj.planDetails = planDetails;

		const subscribeDetails = await userReportRepo.getAllSubscribeUser(condition);
		responseObj.subscribe_user_count = subscribeDetails;
		return {
			"status": 200,
			"data": responseObj,
		};
	} catch (error) {
		console.log(e);
		throw error;
	}
};



/* exports.subscriptionChart = async req => {
	try {
		const responseObj1 = {};
		const subscribeDetails = await userReportRepo.getAllSubscribeUser();
		responseObj1.subscribe_user_count = subscribeDetails;
		return {
			"status": 200,
			"data": responseObj1,
		};
	} catch (error) {
		throw error;
	}
}; */