const mongoose = require('mongoose');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const userModel = require('user/models/user.model');
const moment = require('moment');
const User = new userModel();
const paymentRepo = require('payment/repositories/payment.repository');

var gm = require('gm').subClass({
    imageMagick: true
});
var fs = require('fs');
const jwt = require('jsonwebtoken');

/* @Method: login
// @Description: user Login Render
*/
exports.login = (req, res) => res.render('user/views/login.ejs');


/* @Method: Create
// @Description: user create Render
*/
exports.create = async req => {
    try {
        const roles = await roleRepo.getAllByField({
            'roleType': {
                $nin: ["frontend"]
            },
            'role': {
                $nin: ["admin"]
            }
        });
        return {
            "status": 200,
            data: roles,
            "message": req.flash('info')
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: store
// @Description: user create action
*/
exports.store = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false
        });
        if (_.isEmpty(result)) {
            /*const role = await roleRepo.getByField({
                "role": req.body.role
            });
            req.body.role = role._id;*/
            const userPassword = req.body.password;
            req.body.password = req.user.generateHash(req.body.password);

            const result = await userRepo.save(req.body);

            const setting_data = await settingRepo.getAllSetting();
            var settingObj = {};
            if (!_.isEmpty(setting_data)) {
                setting_data.forEach(function (element) {
                    settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                });
                //req.session.setting = settingObj;
            }

            var mailOptions = {
                from: settingObj.Site_Title + '<support@beashark.com>',
                to: result.email,
                subject: "Welcome to " + settingObj.Site_Title,
                html: 'Hello ' + result.firstName + ' ' + result.lastName + ',<br><br>Your profile has been created. Please login with this details below.<br>Email : ' + result.email + '<br>Password : ' + userPassword + '<br><br>Thank you.'
            };

            await transporter.sendMail(mailOptions);

            return {
                "status": 200,
                data: result,
                "message": "User Created Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: edit
// @Description:  render user update page
*/
exports.edit = async req => {
    try {
        const result = await userRepo.getById(req.params.id);


        var routeNmPrefx = '';
        var routeLnkPrefx = '';
        if (req.roleType == 'frontend' && req.roleName == 'users') {
            routeNmPrefx = 'frontend.';
            routeLnkPrefx = 'frontend-';
        } else if (req.roleType == 'frontend' && req.roleName == 'companies') {
            routeNmPrefx = 'company.';
            routeLnkPrefx = 'company-';
        }
        return {
            "status": 200,
            data: result,
            routeNmPrefx: routeNmPrefx,
            routeLnkPrefx: routeLnkPrefx,
            "message": "User Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: update
// @Description: user update action
*/
exports.update = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false,
            '_id': {
                $ne: req.body.user_id
            }
        });
        if (_.isEmpty(result)) {
            await userRepo.getUser(req.body.user_id);
            const result2 = await userRepo.updateById(req.body, req.body.user_id);
            return {
                "status": 200,
                data: result2,
                "message": "User Updated Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/* @Method: updateCompany
// @Description: user update action
*/
exports.updateCompany = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false,
            '_id': {
                $ne: req.body.user_id
            }
        });
        if (_.isEmpty(result)) {
            const data = await userRepo.getCompanyById(req.body.company_id);

            if (req.files.length > 0) {

                // if (!_.isEmpty(data)) {
                //     fs.unlink('public/uploads/company/' + data.company_details[0].logo, function (err) {
                //         if (err) throw new Error(err.message);
                //     });

                //     fs.unlink('public/uploads/company/thumb/' + data.company_details[0].logo, function (err) {
                //         if (err) throw new Error(err.message);
                //     });
                // }

                gm('public/uploads/company/' + req.files[0].filename)
                    .resize(100)
                    .write('public/uploads/company/thumb/' + req.files[0].filename, function (err) {
                        if (err)
                            throw new Error(err.message);
                    });

                req.body.logo = req.files[0].filename;
            } else {
                req.body.logo = data.company_details[0].logo;
            }

            const result2 = await userRepo.updateById(req.body, req.body.user_id);

            var companyDetObj = {
                'name': req.body.company_name,
                'industry': req.body.industry_type,
                'logo': req.body.logo
            }
            var companyObj = {
                'company_details': []
            }
            companyObj.company_details.push(companyDetObj);

            const resultCompany = await userRepo.updateCompanyById(companyObj, req.body.company_id);

            return {
                "status": 200,
                data: result2,
                "message": "Company Updated Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/* @Method: list
// @Description: To get all the users from DB
*/
exports.list = async req => {
    try {
        const data = await userRepo.getAllUsersByRolename(req.query, req.user, {
            '_id': -1
        });
        return {
            "status": 200,
            data: data,
            "message": "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: updateprofile
// @Description: Update My Profile 
*/
exports.updateprofile = async req => {
    try {
        const id = req.body.id;

        const user = await userRepo.getById(id);

        if (req.files.length > 0) {

            if (!_.isEmpty(user) && user.profile_image != '') {
                if (fs.existsSync('public/uploads/user/' + user.profile_image)) {
                    const upl = await fs.unlinkSync('./public/uploads/user/' + user.profile_image);
                }
                if (fs.existsSync('./public/uploads/user/thumb/' + user.profile_image)) {
                    const upl_thumb = await fs.unlinkSync('./public/uploads/user/thumb/' + user.profile_image);
                }
            }

            gm('public/uploads/user/' + req.files[0].filename)
                .resize(100)
                .write('public/uploads/user/thumb/' + req.files[0].filename, function (err) {
                    if (err)
                        deferred.reject({
                            "success": false,
                            "status": 500,
                            data: [],
                            "message": err.message
                        });
                });

            req.body.profile_image = req.files[0].filename;
        }

        const result = await userRepo.updateById(req.body, id);
        return {
            "status": 200,
            data: result,
            "message": "Profile updated successfully"
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: statusChange
// @Description: User status change action
*/
exports.statusChange = async req => {
    try {
        const result = await userRepo.getById(req.body.id);
        const updatedStatus = (_.has(result, 'status') && result.status == "Inactive") ? "Active" : "Inactive";
        await userRepo.updateById({
            'status': updatedStatus
        }, req.body.id);
        return {
            "status": 200,
            data: result,
            "message": "Status has changed successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: delete
// @Description: User delete
*/
exports.destroy = async req => {
    try {
        await userRepo.delete(req.params.id);
        return {
            "status": 200,
            data: [],
            "message": "Data Removed Successfully"
        };
    } catch (error) {
        throw error;
    }
};