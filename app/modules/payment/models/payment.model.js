const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const PaymentSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  price: {
    type: Schema.Types.Double,
    default: 0.0
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  status: {
    type: String,
    default: '',
    enum: ['success', 'pending', 'cancel']
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

// For pagination
PaymentSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Payment', PaymentSchema);