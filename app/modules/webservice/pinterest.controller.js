const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const moment = require('moment');
const userRepo = require('user/repositories/user.repository');
const social_postRepo = require('social_post/repositories/social_post.repository');

var schedule = require('node-schedule');
const fs = require('fs');
class pinterestController {
    constructor() { }

    /*
    // @Method: FacebookContentShare
    // @Purpose: Share Content in Facebook Page
    */
    async pinterestPostSchedule(req, res) {
        try {
            var tempDate = req.body.date.split('-');
            var day = tempDate[2].split(' ');
            var upl_resume;
            tempDate[1] = Number(tempDate[1]) -1 ;
            var time = day[1].split(':');
            console.log(tempDate[0], tempDate[1], day[0], time[0], time[1], 0)
            var date = new Date(tempDate[0], tempDate[1], day[0], time[0], time[1], 0);
            /* var data = moment(req.body.date).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]"); */
            /* console.log("281", dd);
            var date = new Date(2019, 10, 26, 21, 35, 0);
            console.log("282", date); */
            var j = await schedule.scheduleJob(date, async () =>{
                if (req.files) {
                    let start = async () => {
                        await asyncForEach(req.files, async (file) => {
                            let userDetails = await userRepo.getById(req.user._id);
                            var pinterest = require('pinterest-node-api')(userDetails.pinterest.access_token);
                            pinterest.setUserToken(userDetails.pinterest.access_token);
                            var replaced = userDetails.pinterest.board_name.toLowerCase().split(' ').join('-');
                            var data = {
                                board: userDetails.pinterest.first_name.toLowerCase() + userDetails.pinterest.last_name.toLowerCase() + "/" + replaced,
                                note: req.body.post_content,
                                image_url: process.env.SERVER_LINK + '/uploads/social_post/pinterest/' + file.filename

                            };
                            var response = await pinterest.pins.createPin(data);
                            console.log("42", response);
                            upl_resume = await fs.unlinkSync('./public/uploads/social_post/pinterest/' + file.filename);
                            let savedata = {
                                user_id: req.user._id,
                                post_content: req.body.post_content,
                                social_id: response.data.id,
                                url: response.data.url,
                                ctype: 'image',
                                post_schedule_time: req.body.date,
                                post_status: 'posted',
                                socialtype: 'pinterest'
                            }
                            await social_postRepo.save(savedata);
                        });
                    }
                    await start();
                    async function asyncForEach(array, callback) {
                        for (let index = 0; index < array.length; index++) {
                            await callback(array[index], index, array);
                        }
                    }
                }
                   
                });
            // console.log("286", req.files[0].filename)

            /* var data = {};
            var response = await pinterest.boards.getUserBoards(data); */
            return {
                status: 200,
                data: [],
                "message": 'Image schedulled successfully.'
            }

        }
        catch (e) {
            console.log("268", e);
            return {
                status: 500,
                data: e.message,
                "message": ' Something went wrong, please try again. '
            }
        }
    }
}

module.exports = new pinterestController();