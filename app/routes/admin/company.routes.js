const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const companyController = require('company_management/controllers/company.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/promocode/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});
const uploadFile = multer({
	storage: Storage,
	fileFilter: (req, file, cb) => {
		// accept image only
		if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
			return cb(new Error('Only image files are allowed!'), false);
		}
		cb(null, true);
	}
});
const request_param = multer();


namedRouter.all('/company*', auth.authenticate);

/*
// @Route: Company List
*/
namedRouter.get("company.list", '/company/list', request_param.any(), async (req, res) => {
	try {
		const success = await companyController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('company_management/views/list.ejs', {
			page_name: 'company-management',
			page_title: 'Company List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('company.list'));
	}
});

/*
// @Route: Render Create Company
*/
namedRouter.get("company.create", '/company/create', async (req, res) => {
	try {
		const success = await companyController.create(req);
		return res.render('company_management/views/add.ejs', {
			page_name: 'company-management',
			page_title: 'Create Company',
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('company.list'));
	}
});

/*
// @Route: Create CMS Action
*/
namedRouter.post("company.store", '/company/store', uploadFile.any(), async (req, res) => {
	try {
		const success = await companyController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('company.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('company.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: Edit Company
*/
namedRouter.get("company.edit", '/company/edit/:id', async (req, res) => {
	try {
		const success = await companyController.edit(req);
		return res.render('company_management/views/edit.ejs', {
			page_name: 'company-management',
			page_title: 'Update Company',
			setting: req.session.setting,
			user: req.user,
			response: success.data
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update Company Action
*/
namedRouter.post("company.update", '/company/update', uploadFile.any(), async (req, res) => {
	const companyId = req.body.id;
	try {
		const success = await companyController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('company.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('company.edit', {
			id: companyId
		}));
	}
});

/*
// @Route: Company status change
*/
namedRouter.post('company.status.change', '/company/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await companyController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
	}
});

/*
// @Route: Delete Company
*/
namedRouter.get('company.delete', '/company/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await companyController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.company', {
			id: success.data.user_id
		}));
	} catch (error) {
		req.flash('error', failure.message);
		// return res.redirect(namedRouter.urlFor('company.list'));
	}
});

namedRouter.post("company.listDashboard", '/company/listDashboard', async (req, res) => {
	try {
        const success = await companyController.listDashboard(req, res);
        res.send({ "meta": success.meta, "data": success.data });
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// Export the express.Router() instance
module.exports = router;