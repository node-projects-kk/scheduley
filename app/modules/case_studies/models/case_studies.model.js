const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["active", "inactive"];
const deletestatus = [false, true];

const CaseStudySchema = new Schema({
	logo_image: {
		type: String,
		default: ''
	},
	case_content: {
		type: String,
		default: ''
	},
	button_text: {
		type: String,
		default: 0
	},
	button_link: {
		type: String,
		default: ''
	},
	status: {
		type: String,
		default: "active",
		enum: status
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deletestatus
	},
	createdAt: {
		type: Date,
		default: Date.now(),
	}
});

// For pagination
CaseStudySchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('CaseStudy', CaseStudySchema);