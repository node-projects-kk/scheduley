const SocialFeed = require('socialfeed/models/socialfeed.model');
const perPage = config.PAGINATION_PERPAGE;

class SocialFeedRepository {
    constructor() {}

    async getAll(searchQuery, sortOrder = { _id: -1}, page) {
        try {
            const query = [{}];
            // serach by keyword
            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    const search = searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                                'setting_name': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'setting_value': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            }
                        ]
                    });
                }
            }
            searchQuery = {
                "$and": query
            };
            const aggregate = SocialFeed.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $project: {
                        _id: "$_id",
                        setting_name: "$setting_name",
                        setting_value: "$setting_value"
                    }
                },
                {
                    $match: searchQuery
                },
            ]);
            return await SocialFeed.aggregatePaginate(aggregate, {
                page: page,
                limit: perPage
            }); // { data, pageCount, totalCount }
        } catch (error) {
            return error;
        }
    }

    async getAllSocialFeed() {
        try {
            return await SocialFeed.find().exec();
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await SocialFeed.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await SocialFeed.findOne(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await SocialFeed.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await SocialFeed.findById(id).lean().exec();
            return await SocialFeed.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await SocialFeed.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    /*async getSocialFeedCount() {
        try {
            return await SocialFeed.count({
                'status': "Active"
            });
        } catch (error) {
            return error;
        }
    }*/

	
    
	async save(data){
		try {
			console.log("133>>",data);
			let sfeed = await SocialFeed.create(data);
			if (!sfeed) {
				return null;
			}
			return sfeed;  
		}
		catch (e){
			console.log("140>>", e.message);
			return e;  
		}     
	}
}

module.exports = new SocialFeedRepository();