const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();

const teamController = require('team_management/controllers/team.api.controller');


/**
 * @api {get} /team/list/ Get Team Members
 * @apiVersion 1.0.0
 * @apiGroup TeamMembers
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cecf9cabb7b742baaec2c87",
            "firstName": "Test",
            "lastName": "Demo",
            "image": "image_1559034314203_featured-small-circular.jpg",
            "status": "Active",
            "designation": "CEO",
            "facebook": "www",
            "instagram": "Test Demo",
            "email": "test@demo.com",
            "isDeleted": false
        }
    ],
    "current": 1,
    "pages": 1,
    "message": "team list fetched successfully",
    "search_param": ""
}
*/
namedRouter.get("api.team.list", '/team/list/', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.list(req);
		let searchStr = {
			'keyword': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
	}
});

module.exports = router;