const settingRepo = require('setting/repositories/setting.repository');
const slug = require('slug');
var gm = require('gm').subClass({
    imageMagick: true
});
var fs = require('fs');
/*
// @Method: create
// @Description:  render add setting page
*/
exports.create = async req => {
    try {
        // const data = await settingRepo.getById(req.params.id);
        return {
            "status": 200,
            "message": "Setting Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/*
// @Method: store
// @Description:  render add setting page
*/
exports.store = async req => {
    try {
        req.body.slug = slug(req.body.setting_name, {
            replacement: '_',
            lower: true
        });
        const data = await settingRepo.save(req.body);
        return {
            "status": 200,
            data,
            "message": "Setting Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/*
// @Method: edit
// @Description:  render setting edit page
*/
exports.edit = async req => {
    try {
        const data = await settingRepo.getById(req.params.id);
        return {
            "status": 200,
            data,
            "message": "Setting Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* 
// @Method: update
// @Description: Setting update action
*/
exports.update = async req => {
    try {
        const result = await settingRepo.getByField({
            'setting_name': req.body.setting_name,
            '_id': {
                $ne: req.body.setting_id
            }
        });
        if (_.isEmpty(result)) {
            if (req.files.length > 0) {
                const data = await settingRepo.getById(req.body.setting_id);
                if (!_.isEmpty(data)) {
                    fs.unlink('public/uploads/settings/sitelogo/' + data.setting_value, function (err) {
                        if (err) throw new Error(err.message);
                    });
                }
                req.body.setting_value = req.files[0].filename;
            }

            const result2 = await settingRepo.updateById(req.body, req.body.setting_id);

            /***************/
            const setting_data = await settingRepo.getAllSetting();
            var setting = {};
            if (!_.isEmpty(setting_data)) {
                setting_data.forEach(function (element) {
                    setting[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                });
                req.session.setting = setting;
            }
            /***************/
            return {
                "status": 200,
                data: result2,
                "message": "Setting Updated Successfully"
            };
        } else {
            return new Error("This Setting is already exist!");
        }
    } catch (error) {
        console.log('erererererer', error);
        throw error;
    }
};


/* 
// @Method: list
// @Description: To get all the cmss from DB
*/
exports.list = async req => {
    try {
        const currentPage = req.query.page || 1;
        const {
            data,
            pageCount,
            totalCount
        } = await settingRepo.getAll(req.query, {
            '_id': -1
        }, (req.query.page || 1));

        return {
            "status": 200,
            data,
            current: currentPage,
            pages: pageCount,
            "message": "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/*
// @Method: statusChange
// @Description: Cms status change action
*/
exports.statusChange = async req => {
    try {
        const result = await settingRepo.getById(req.body.id);
        const updatedStatus = (_.has(result, 'status') && result.status == "Inactive") ? "Active" : "Inactive";
        const result2 = await settingRepo.updateById({
            'status': updatedStatus
        }, req.body.id);
        return {
            "status": 200,
            data: result2,
            "message": "Cms status has changed successfully"
        }
    } catch (error) {
        throw error;
    }
};

/* 
// @Method: delete
// @Description: Cms delete
*/
exports.destroy = async req => {
    try {
        const result = await settingRepo.delete(req.params.id);
        return {
            "status": 200,
            data: result,
            "message": "Settings Removed Successfully"
        };
    } catch (error) {
        throw error;
    }
};