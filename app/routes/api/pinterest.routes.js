const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
var multer = require('multer');
const fs = require('fs');

var pinterestController = require('webservice/pinterest.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, cb) {
        
        if (!fs.existsSync('./public/uploads/social_post/pinterest/')) {
            // if not create directory
            fs.mkdirSync('./public/uploads/social_post/pinterest/');

        }
        cb(null, './public/uploads/social_post/pinterest/')
    },
    filename: function (req, file, cb) {
       // console.log("39", file.originalname)
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})

const uploadFile = multer({
    storage: Storage
});

const request_param = multer({
    storage: Storage
});

namedRouter.all('/pinterest*', auth.authenticateAPI);

/**
 * @api {post} /pinterest/schedule Schedule posts for Pinterest
 * @apiVersion 1.0.0
 * @apiGroup Pinterest 
 * @apiParam post Upload post
 * @apiParam date Post Schedule Time 
 * @apiParam post_content User's Content or Message or caption
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "success": true
    },
    "message": "Media schedulled successfully."
}
*/
namedRouter.post('pinterest.schedule', '/pinterest/schedule', uploadFile.any(), async (req, res) => {
    try {
        const success = await pinterestController.pinterestPostSchedule(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('ererererererre <><>122<Route><>', error);
        return {
            status: 500,
            message: error.message
        }
    }
});

module.exports = router;