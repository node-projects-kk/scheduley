const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const promocodeController = require('promo_code/controllers/promocode.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/promocode/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});
const uploadFile = multer({
	storage: Storage,
	fileFilter: (req, file, cb) => {
		// accept image only
		if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
			return cb(new Error('Only image files are allowed!'), false);
		}
		cb(null, true);
	}
});
const request_param = multer();


namedRouter.all('/promocode*', auth.authenticate);

/*
// @Route: CMS List
*/
namedRouter.get("promocode.list", '/promocode/list/:from?/:to?', request_param.any(), async (req, res) => {
	try {
		const success = await promocodeController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('promo_code/views/list.ejs', {
			page_name: 'promocode-management',
			page_title: 'Promo Code List',
			user: req.user,
			setting: req.session.setting,
			from: req.params.from,
			to: req.params.to,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	}
});

/*
// @Route: Render Create CMS
*/
namedRouter.get("promocode.create", '/promocode/create', async (req, res) => {
	try {
		const success = await promocodeController.create(req);
		return res.render('promo_code/views/add.ejs', {
			page_name: 'promocode-management',
			page_title: 'Create Plan',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('promocode.list'));
	}
});

/*
// @Route: Create CMS Action
*/
namedRouter.post("promocode.store", '/promocode/store', uploadFile.any(), async (req, res) => {
	try {
		const success = await promocodeController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('promocode.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: Render Edit CMS
*/
namedRouter.get("promocode.edit", '/promocode/edit/:id', async (req, res) => {
	try {
		const success = await promocodeController.edit(req);
		return res.render('promo_code/views/edit.ejs', {
			page_name: 'promocode-management',
			page_title: 'Update Plan',
			setting: req.session.setting,
			user: req.user,
			response: success.data
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update CMS Action
*/
namedRouter.post("promocode.update", '/promocode/update', uploadFile.any(), async (req, res) => {
	const promocodeId = req.body.promocode_id;
	try {
		const success = await promocodeController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('promocode.edit', {
			id: promocodeId
		}));
	}
});

/*
// @Route: CMS status change
*/
namedRouter.post('promocode.status.change', '/promocode/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await promocodeController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	}
});

/*
// @Route: Delete CMS
*/
namedRouter.get('promocode.delete', '/promocode/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await promocodeController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('promocode.list'));
	}
});

// Export the express.Router() instance
module.exports = router;