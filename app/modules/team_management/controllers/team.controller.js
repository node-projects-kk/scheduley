const teamRepo = require('team_management/repositories/team.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');

/* 
// @Method: list
// @Description: To get all the teams from DB
*/
exports.list = async req => {
	try {
		return await teamRepo.getAll(req.query, {
			'createdAt': -1
		});
		return {
			"status": 200,
			data,
			"message": "Team list fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};


exports.create = async (req, res) => {
	try {
		return {
			"status": 200,
			data: [],
			"message": "Team meber details fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};

exports.store = async req => {
	try {
		if (req.files) {
			if (req.files[0].fieldname == 'image') {
				gm('public/uploads/team/' + req.files[0].filename).resize(100).write('public/uploads/team/thumb/' + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				req.body.image = req.files[0].filename;
			}
		}
		const result = await teamRepo.save(req.body);
		return {
			"status": 200,
			data: result,
			"message": 'Team member details stored successfully'
		};
	} catch (error) {
		throw error;
	}
};

/*
// @Method: statusChange
// @Description: Cms status change action
*/
exports.statusChange = async req => {
	try {
		const result = await teamRepo.getById(req.body.id);
		const updatedStatus = (_.has(result, 'status') && result.status == "Inactive") ? "Active" : "Inactive";
		const result2 = await teamRepo.updateById({
			'status': updatedStatus
		}, req.body.id);
		return {
			"status": 200,
			data: result2,
			"message": "Team member status has changed successfully"
		}
	} catch (error) {
		throw error;
	}
};

/*
// @Method: edit
// @Description:  render team update team
*/
exports.edit = async req => {
	try {
		const data = await teamRepo.getById(req.params.id);
		return {
			"status": 200,
			data,
			"message": "Team member details fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};


/* 
// @Method: update
// @Description: Cms update action
*/
exports.update = async req => {
	try {
		let arr = [];
		const olddata = await teamRepo.getById(req.body.id);

		if (req.files.length > 0) {
			if (!_.isEmpty(olddata)) {

				fs.unlink('public/uploads/team/' + olddata.image, function (err) {
					if (err) throw new Error(err.message);
				});

				fs.unlink('public/uploads/team/thumb/' + olddata.image, function (err) {
					if (err) throw new Error(err.message);
				});
			}
			gm('public/uploads/team/' + req.files[0].filename).resize(100).write('public/uploads/team/thumb/' + req.files[0].filename, function (err) {
				if (err) throw new Error(err.message);
			});

			req.body.image = req.files[0].filename;
		}
		const result = await teamRepo.updateById(req.body, req.body.id);
		return {
			"status": 200,
			data: result,
			"message": "team updated Successfully"
		};

	} catch (error) {
		throw error;
	}
};

/* 
// @Method: delete
// @Description: Cms delete
*/
exports.destroy = async req => {
	try {
		const olddata = await teamRepo.getById(req.params.id);
		if (!_.isEmpty(olddata)) {

			fs.unlink('public/uploads/team/' + olddata.image, function (err) {
				if (err) throw new Error(err.message);
			});

			fs.unlink('public/uploads/team/thumb/' + olddata.image, function (err) {
				if (err) throw new Error(err.message);
			});
		}
		const result = await teamRepo.updateById({
			'isDeleted': true
		}, req.params.id);
		return {
			"status": 200,
			data: result,
			"message": "Team member Removed Successfully"
		};
	} catch (error) {
		throw error;
	}
};