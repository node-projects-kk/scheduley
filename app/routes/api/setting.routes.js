const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
// const multer = require('multer');
// const request_param = multer();

const settingController = require('setting/controllers/setting.api.controller');

// namedRouter.all('/setting*', auth.authenticateAPI);
/**
 * @api {get} /setting Get Setting
 * @apiVersion 1.0.0
 * @apiGroup Setting
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "Contact_Phone": "(928) 000-5555",
        "Site_Title": "Be a Shark",
        "Site_Logo": "site_logo_1555324594326_logo.png",
        "Stripe_Secret_Key": "sk_test_XXDuAFknVN4W9lXXXXXXX12",
        "Company_Sign_Up_Price": "500",
        "Investment_Price": "500",
        "Service_Price": "100"
    },
    "message": "Setting fetched Successfully"
}
*/
namedRouter.get("api.setting", '/setting', function (req, res) {
    settingController.getSettingAll(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

module.exports = router;