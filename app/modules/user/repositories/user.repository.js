const mongoose = require('mongoose');
const User = require('user/models/user.model');
const planRepo = require('plan/models/plan.model');
const companyRepo = require('company_management/models/company.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;
const transactionRepo = require('user/models/transaction.model');

class UserRepository {
    constructor() {}

	async fineOneWithRole(params) {
		try {
			const user = await User.findOne({email: params.email,isDeleted: false}).populate('role').exec();
			//console.log("15>>",user); process.exit();
			if (!user) {
				return new Error('Authentication failed. User not found.');
			}
			else if (user) {
				if (!user.validPassword(params.password, user.password)) {
					return new Error('Authentication failed. Wrong password.');
				}
				else {
					return user;
				}
			}
		}
		catch (error) {
			return error;
		}
	}

    async findIsAccess(user_role, permission_id) {
        try {
            const roleInfo = await rolePermission.findOne({
                $and: [{
                    'role': user_role
                }, {
                    'permissionall': {
                        $in: [permission_id]
                    }
                }]
            }).exec();
            return (roleInfo != null) ? true : false;
        } catch (error) {
            return error;
        }
    }

    async getAll(_searchQuery, user, sortOrder, page) {
        try {
            const query = [{
                "isDeleted": false,
                _id: {
                    $ne: mongoose.Types.ObjectId(user._id)
                },
                'user_role.role': {
                    $nin: ["admin", "sub-admin"]
                }
            }];
            // serach by keyword
            if (_.has(_searchQuery, "keyword")) {
                if (_searchQuery.keyword != '') {
                    const search = _searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                                'first_name': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'last_name': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'email': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'document.phone': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            }
                        ]
                    });
                }
            }

            // serach by role
            if (_.has(_searchQuery, "role")) {
                if (_searchQuery.role != '') {
                    query.push({
                        'role': mongoose.Types.ObjectId(_searchQuery.role)
                    });
                }
            }

            const searchQuery = {
                "$and": query
            };
            const aggregate = await User.aggregate([{
                    $unwind: "$role"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    "$sort": sortOrder
                },
                {
                    $project: {
                        first_name: "$first_name",
                        last_name: "$last_name",
                        email: "$email",
                        phone: "$phone",
                        role: "$role",
                        isDeleted: "$isDeleted",
                        user_role: "$user_role",
                        document: "$$ROOT"
                    }
                },
                {
                    $match: searchQuery
                },
            ]);
            return await User.aggregatePaginate(aggregate, {
                page: page,
                limit: perPage
            });
        } catch (error) {
            return error;
        }
    }

    async getAllUsers(_searchQuery = {}) {

        try {
            const query = [{
                "isDeleted": false,
                'user_role.role': {
                    $nin: ["admin"]
                }
            }];



            const searchQuery = {
                "$and": query
            };
            const aggregate = await User.aggregate([{
                    $unwind: "$role"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    $project: {
                        first_name: "$first_name",
                        last_name: "$last_name",
                        email: "$email",
                        phone: "$phone",
                        role: "$role",
                        isDeleted: "$isDeleted",
                        user_role: "$user_role",
                        document: "$$ROOT"
                    }
                },
                {
                    $match: searchQuery
                },
            ]);

            return aggregate;
        } catch (error) {
            return error;
        }
    }

    async getAllUsersByRolename(_searchQuery, user, sortOrder = {
        _id: -1
    }, page) {
        try {
            const query = [{
                "isDeleted": false,
                // _id: {
                //     $ne: mongoose.Types.ObjectId(user._id)
                // },
                'user_role.role': {
                    $nin: ["admin"]
                }
            }];

            // serach by keyword //
            if (_.has(_searchQuery, "keyword")) {
                if (_searchQuery.keyword != '') {
                    let search = _searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                                'firstName': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'lastName': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'email': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'contactNumber': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'user_role.roleDisplayName': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            }
                        ]
                    });
                }
            }

            // serach by role //
            if (_.has(_searchQuery, "role")) {
                if (_searchQuery.role != '') {
                    query.push({
                        'role': mongoose.Types.ObjectId(_searchQuery.role)
                    });
                }
            }

            const searchQuery = {
                "$and": query
            };

            let user_list = User.aggregate([{
                    $unwind: "$role"
                },
                {
                    $unwind: "$plan"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },
                {
                    "$lookup": {
                        "from": "plans",
                        "localField": "plan",
                        "foreignField": "_id",
                        "as": "plan_details"
                    },
                },
                {
                    $unwind: {
                        path: "$plan_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "companymangements",
                        "localField": "_id",
                        "foreignField": "user_id",
                        "as": "company_details"
                    },
                },
                {
                    $unwind: {
                        path: "$company_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        "_id": "$_id",
                        "role": {
                            $first: "$user_role"
                        },
                        "firstName": {
                            $first: "$firstName"
                        },
                        "lastName": {
                            $first: "$lastName"
                        },
                        "email": {
                            $first: "$email"
                        },
                        "password": {
                            $first: "$password"
                        },
                        "profile_image": {
                            $first: "$profile_image"
                        },
                        "deviceToken": {
                            $first: "$deviceToken"
                        },
                        "deviceType": {
                            $first: "$deviceType"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "isDeleted": {
                            $first: "$isDeleted"
                        },
                        "contactNumber": {
                            $first: "$contactNumber"
                        },
                        "plan": {
                            $addToSet: "$plan_details"
                        },
                        "company": {
                            $addToSet: "$company_details"
                        },
                        "regType": {
                            $first: "$regType"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        role: 1,
                        firstName: 1,
                        lastName: 1,
                        email: 1,
                        password: 1,
                        profile_image: 1,
                        deviceToken: 1,
                        deviceType: 1,
                        status: 1,
                        isDeleted: 1,
                        contactNumber: 1,
                        company: 1,
                        plan: 1,
                        regType: 1,
                        createdAt: 1,
                        //user_role: "$role",
                    }
                },
                {
                    $match: searchQuery
                },
                {
                    "$sort": sortOrder
                }
            ]);

            return await User.aggregatePaginate(user_list, {
                page: page,
                limit: perPage
            });
        } catch (error) {
            return error;
        }
    }

    async getAllUsersByCompany(_searchQuery, user, sortOrder = {
        _id: -1
    }) {

        try {

            let query = [{
                "isDeleted": false,
                "company_info.isDeleted": false,
                "paid_financial_service": "yes"
            }];

            const searchQuery = {
                "$and": query
            };

            return await User.aggregate([

                {
                    "$lookup": {
                        "from": "company_infos",
                        "localField": "_id",
                        "foreignField": "company",
                        "as": "company_info"
                    },
                },
                {
                    $unwind: "$company_info"
                },

                {
                    "$lookup": {
                        "from": "financial_infos",
                        "localField": "company_info._id",
                        "foreignField": "company_id",
                        "as": "financial_info"
                    },
                },
                {
                    $unwind: "$financial_info"
                },

                {
                    "$match": {
                        "company_info.final_status": {
                            "$in": ["Under_Review", "Revision_Received", "Pending_Review", "Revision_Needed", "Approved", "Declined"]
                        },
                    }
                },
                {
                    $match: searchQuery
                },
                {
                    $addFields: {
                        "__order": {
                            $indexOfArray: [
                                ["Under_Review", "Revision_Received", "Pending_Review", "Revision_Needed", "Approved", "Declined"], "$company_info.final_status"
                            ]
                        }
                    }
                },
                {
                    $sort: {
                        "__order": 1
                    }
                },
            ]);



        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await User.findById(id).populate('role').lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getCompanyById(id) {
        try {
            return await Company.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await User.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }

    async getCompanyByField(params) {
        try {
            return await Company.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }
    async getOfferInfoByField(params) {
        try {
            return await Offering.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }
    async getLegalInfoByField(params) {
        try {
            return await Legal.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }
    async getFinancialInfoByField(params) {
        try {
            return await Financial.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }
    async getPitchInfoByField(params) {
        try {
            return await Pitch.findOne(params).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await User.find(params).exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await User.findById(id).exec();
            return await User.findByIdAndUpdate(id, {
                isDeleted: true
            }).exec();
        } catch (error) {
            return error;
        }
    }

	async updateById(data, id) {
		try {
			return await User.findByIdAndUpdate(id, data, {new: true,upsert: true}).populate('role').exec();
		}
		catch (error) {
			return error;
		}
	}

    async save(data) {
        try {
            
            const saveUser = await User.create(data);
            
            return saveUser;
        } catch (error) {
            return error;
        }
    }

    async forgotPassword(params) {
        try {
            const user = await User.findOne({
                email: params.email_p_c
            }).exec();
            if (!user) {
                return new Error('Authentication failed. User not found.');
            } else {
                let random_pass = Math.random().toString(36).substr(2, 9);
                const readable_pass = random_pass;
                random_pass = user.generateHash(random_pass);
                await User.findByIdAndUpdate(user._id, {
                    password: random_pass
                }).exec();
                return readable_pass;
            }
        } catch (error) {
            return error;
        }
    }

    async getUser(id) {
        try {
            return await User.findOne({
                id
            }).exec();
        } catch (error) {
            return error;
        }
    }

    async getBrowseList(sortOrder = {
        _id: -1
    }, page) {
        try {
            let aggregate = User.aggregate([{
                    "$lookup": {
                        "from": "company_infos",
                        "localField": "_id",
                        "foreignField": "company",
                        "as": "company_info"
                    },
                },
                {
                    $unwind: "$company_info"
                },
                {
                    "$match": {
                        "isDeleted": false,
                        "visit": {
                            $gte: 0
                        }
                    }
                },
                {
                    "$sort": sortOrder
                }
            ]);

            let options = {
                page: page,
                limit: perPage
            };
            return await User.aggregatePaginate(aggregate, options);
        } catch (error) {
            return error;
        }
    }

    async getUsersByRole(userRole, userId) {
        try {
            return await User.find({
                'role': mongoose.Types.ObjectId(userRole),
                '_id': {
                    $ne: mongoose.Types.ObjectId(userId)
                }
            }).lean().exec();

        } catch (error) {
            throw error;
        }
    }

    async getAllActiveUsers(searchYear, searchMonth) {
        try {
            var query = [{}];
            query.push({})
            var searchQuery = {
                "$and": query
            };

            return await User.aggregate([{
                    $unwind: "$role"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_details"
                    }
                },
                {
                    $unwind: {
                        path: "$user_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $project: {
                        'month': {
                            $dateToString: {
                                format: "%m",
                                date: "$createdAt"
                            }
                        },
                        'year': {
                            $dateToString: {
                                format: "%Y",
                                date: "$createdAt"
                            }
                        },
                        'user_details.role': 1,
                        'isDeleted': 1,
                        'status': 1
                    }
                },
                {
                    "$match": {
                        "user_details.role": 'users',
                        "isDeleted": false,
                        "status": 'Active',
                        "year": searchYear
                    }
                },
                {
                    $match: searchQuery
                },
                {
                    $group: {
                        '_id': {
                            month: "$month",
                            year: "$year"
                        },
                        'count': {
                            $sum: 1
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllCompanybyUserId(id) {
        try {

            return await User.aggregate([{
                    "$match": {
                        '_id': mongoose.Types.ObjectId(id),
                        'isDeleted': false
                    }
                },
                {
                    "$lookup": {
                        "from": "companymangements",
                        "localField": "_id",
                        "foreignField": "user_id",
                        "as": "company_details"
                    },
                },
                {
                    $unwind: {
                        path: "$company_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: '$_id',
                        firstName: {
                            $first: '$firstName'
                        },
                        lastName: {
                            $first: '$lastName'
                        },
                        email: {
                            $first: '$email'
                        },
                        status: {
                            $first: '$status'
                        },
                        contactNumber: {
                            $first: '$contactNumber'
                        },
                        createdAt: {
                            $first: '$createdAt'
                        },
                        updatedAt: {
                            $first: '$updatedAt'
                        },
                        paymentStatus: {
                            $first: '$paymentStatus'
                        },
                        isDeleted: {
                            $first: '$isDeleted'
                        },
                        company_details: {
                            $addToSet: '$company_details'
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            throw error;
        }
    }


    async getNewUserSignUp(searchYear, searchMonth) {
        try {

            return await User.aggregate([{
                    $unwind: "$role"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },

                {
                    $project: {
                        'month': {
                            $dateToString: {
                                format: "%m",
                                date: "$createdAt"
                            }
                        },
                        'year': {
                            $dateToString: {
                                format: "%Y",
                                date: "$createdAt"
                            }
                        },
                        'user_role.role': 1
                    }
                },

                {
                    "$match": {
                        'user_role.role': "users",
                        "year": searchYear
                    }
                },

                {
                    $group: {
                        '_id': {
                            month: "$month",
                            year: "$year"
                        },
                        'count': {
                            $sum: 1
                        }
                    }
                }

            ]).exec();

        } catch (error) {
            return error;
        }
    }

    async getNewCompanySignUp(searchYear) {
        try {

            return await User.aggregate([{
                    $unwind: "$role"
                },
                {
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: "$user_role"
                },

                {
                    $project: {
                        'month': {
                            $dateToString: {
                                format: "%m",
                                date: "$createdAt"
                            }
                        },
                        'year': {
                            $dateToString: {
                                format: "%Y",
                                date: "$createdAt"
                            }
                        },
                        'user_role.role': 1
                    }
                },

                {
                    "$match": {
                        "year": searchYear,
                        'user_role.role': "companies"
                    }
                },

                {
                    $group: {
                        '_id': {
                            month: "$month",
                            year: "$year"
                        },
                        'count': {
                            $sum: 1
                        }
                    }
                }

            ]).exec();

        } catch (error) {
            return error;
        }
    }

    async getTotalSignUpCount(condition) {
        try {
            return await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: {
                        path: "$role_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$match": {
                        "isDeleted": false,
                        "status": 'Active',
                        'user_role.role': {
                            $ne: 'admin'
                        },
                    }
                },
                {
                    $project: {
                        // 'month': {
                        //     $dateToString: {
                        //         format: "%m",
                        //         date: "$createdAt"
                        //     }
                        // },
                        // 'year': {
                        //     $dateToString: {
                        //         format: "%Y",
                        //         date: "$createdAt",
                        //     }
                        // },
                        '_id': 1,
                        'isDeleted': 1,
                        'status': 1,
                        'user_role.role': 1,
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$createdAt"
                            }
                        },
                    }
                },
                {
                    $match: condition
                },
                {
                    $group: {
                        // '_id': {
                        //     month: "$month",
                        //     year: "$year"
                        // },
                        '_id': "$_id",
                        'count': {
                            $sum: 1
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

    async getActiveUserCount(condition) {
        try {
            return await User.aggregate([{
                    "$lookup": {
                        "from": "roles",
                        "localField": "role",
                        "foreignField": "_id",
                        "as": "user_role"
                    },
                },
                {
                    $unwind: {
                        path: "$role_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$match": {
                        "isDeleted": false,
                        "status": 'Active',
                        'user_role.role': {
                            $ne: 'admin'
                        }
                    }
                },
                {
                    $project: {
                        // 'month': {
                        //     $dateToString: {
                        //         format: "%m",
                        //         date: "$createdAt"
                        //     }
                        // },
                        // 'year': {
                        //     $dateToString: {
                        //         format: "%Y",
                        //         date: "$createdAt",
                        //     }
                        // },
                        '_id': 1,
                        'isDeleted': 1,
                        'status': 1,
                        'user_role.role': 1,
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$createdAt"
                            }
                        }
                    }
                },
                {
                    $match: condition
                },
                {
                    $group: {
                        '_id': null,
                        'count': {
                            $sum: 1
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

    async getAllPlanUserCount(condition) {
        try {
            return await planRepo.aggregate([{
                    "$lookup": {
                        "from": "users",
                        "localField": "_id",
                        "foreignField": "plan",
                        "as": "user_details"
                    },
                },
                {
                    $unwind: {
                        path: "$user_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$match": {
                        "isDeleted": false,
                        "status": 'Active',
                    }
                },
                {
                    $project: {
                        'user_details.plan': 1,
                        'title': 1,
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$user_details.createdAt"
                            }
                        }
                    }
                },
                {
                    $match: condition
                },
                {
                    $group: {
                        '_id': {
                            title: '$title',
                        },
                        "count": {
                            "$sum": {
                                "$cond": {
                                    if: {
                                        $eq: ["$user_details", '$user_details.plan']
                                    },
                                    then: 0,
                                    else: 1
                                }
                            }
                        },
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

    async getSalesToday(today) {
        try {
            return await transactionRepo.aggregate([{
                    $project: {
                        _id: null,
                        plan_price: "$plan_price",
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$createdAt"
                            }
                        },
                    }

                },
                {
                    $match: {
                        $and: [{
                                "date": {
                                    $gte: today
                                }
                            },
                            {
                                "date": {
                                    $lte: today
                                }
                            }
                        ]
                    }
                },
                {
                    $group: {
                        '_id': null,
                        'sum': {
                            $sum: '$plan_price'
                        },
                    }
                }
            ]);

        } catch (e) {
            console.log("1113", e);
        }
    }

    async getByIdWithPlan(id) {
        try {
            return await User.findById(id).populate('plan').lean().exec();
        } catch (error) {
            return error;
        }
    }

}

module.exports = new UserRepository();