const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["active", "inactive"];
const social_type = ["facebook","instagram","twitter","pinterest"];
const deletestatus = [false, true];

const SocialSchema = new Schema({
	company_id: {type: Schema.Types.ObjectId,ref: 'CompanyMangement',	default: null},
	user_id: {type: Schema.Types.ObjectId,ref: 'User',	default: null},
	social_id: {type: String,default: ''},
	url: {type: String,default: ''},
	upload_session_id: {type: String,default: ''},
	post_content: {type: String,default: ''},
	ctype: {type: String,default: ''},
	post_schedule_time: {type: Date,default: Date.now()},
	post_status: {type: String,default: 'pending',enum: ['pending', 'posted', 'cancel']},
	socialtype:{type:String,default:'facebook', enum:social_type},
	status: {type: String,default: 'active',enum: status},
	isDeleted: {type: Boolean,default: false,enum: deletestatus},
	createdAt: {type: Date,default: Date.now()}
});

// For pagination
SocialSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('SocialPost', SocialSchema);