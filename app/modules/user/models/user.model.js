var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const active = [true, false];
const regType = ['normal', 'facebook', 'google','pinterest','instagram','twitter'];
const status = ["Active", "Inactive"];
const subscriptionstatus = [true,false];
const package_type = ["Monthly","Annual"];
const device_type = ["", "android","ios"];

var UserSchema = new Schema({
	firstName: {type: String, default: ''},
	lastName: { type: String, default: '' },
	email: {  type: String, default: '' },
	password: { type: String, default: '' },
	contactNumber: { type: String,  default: '' },
	role: [{ type: Schema.Types.ObjectId, ref: 'Role' }],
	
	regType: { type: String, default: 'normal',  enum: regType },
	social_id:{type: String, default: ''},
	parent_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null},
	plan: {type: mongoose.Schema.Types.ObjectId, ref: 'Plan'},
	promo_code: {type: mongoose.Schema.Types.ObjectId,ref: 'PromoCode', default: null},
	profile_image: {type: String,default: ''},
	no_of_account_access:{type:Number,default:0},
	deviceToken: {type: String,default: ''},
	deviceType: {type: String,default: "",enum: device_type},
	token: {type:String,default:''},

	stripe_subscription_id:{type:String,default:''},
	stripe_customer_id :{type:String,default:''},
	stripe_plan_id:{type:String,default:''},

	
	isSubscribed: {type:Boolean ,default: false,enum:subscriptionstatus },
	isDeleted: {type: Boolean,default: false,enum: deleted},
	paymentStatus: {type: Boolean,default: 'false',enum: active},
	initialPaymentStatus: {type: Boolean,default: 'false', enum: active},
	status: {type: String,default: "Active",enum: status},

	facebook: {
		name: {type:String,default:''},
		access_token: {type:String,default:''},
		expiry_time: {type:String,default:''},
		facebook_id: {type:String,default:''},
		page_id: {type:String,default:''},
		page_name: {type:String,default:''},
		page_access_token: {type:String,default:''}
	},
	twitter: {
		name: {type:String,default:''},
		access_token: {type:String,default:''},
		expiry_time: {type:String,default:''},
		twitter_id: {type:String,default:''}
	},
	pinterest: {
		first_name: {type:String,default:''},
		last_name: {type:String,default:''},
		pinterest_id: {type:String,default:''},
		access_token: {type:String,default:''},
		expiry_time: {type:String,default:''},
		board_name: {type:String,default:''},
		board_url: {type:String,default:''},
		board_id: {type:String,default:''},
	},
	instagram: {
		name: {type:String,default:''},
		access_token: {type:String,default:''},
		expiry_time: {type:String,default:''},
		instagram_id: {type:String,default:''}
	}
}, {timestamps: true});

// generating a hash
UserSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password, checkPassword) {
  return bcrypt.compareSync(password, checkPassword);
  //bcrypt.compare(jsonData.password, result[0].pass
};

// For pagination
UserSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('User', UserSchema);