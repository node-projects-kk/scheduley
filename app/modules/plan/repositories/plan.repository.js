const Plan = require('plan/models/plan.model');
const perPage = config.PAGINATION_PERPAGE;
const transactionRepo = require('user/models/transaction.model');

class PlanRepository {
	constructor() {}

	async getAll(searchQuery, sortOrder = {
		'_id': -1
	}, page) {
		try {
			const query = [{
				"isDeleted": false
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'title': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};
			// console.log("19>>",searchQuery);
			const aggregate = Plan.aggregate([{
					"$sort": sortOrder
				},
				{
					$project: {
						_id: "$_id",
						title: "$title",
						content: "$content",
						image: "$image",
						status: "$status",
						slug: "$slug",
						price: "$price",
						inactiveDate: { $dateToString: { format: "%m-%d-%Y", date: "$inactiveDate" } },
						createdAt: "$createdAt",
						isDeleted: "$isDeleted",
					}
				},
				{
					$match: searchQuery
				},
			]);

			return await Plan.aggregatePaginate(aggregate, {
				page: page,
				limit: perPage
			}); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Plan.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await Plan.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			return await Plan.find(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Plan.findById(id).lean().exec();
			return await Plan.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Plan.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			});
		} catch (error) {
			return error;
		}
	}

	async getCmsCount() {
		try {
			return await Plan.count({
				'status': "Active"
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			return await Plan.create(data);
		} catch (error) {
			return error;
		}
	}

	async getALLTransaction(condition)
	{
		return await transactionRepo.aggregate([
		{
			$project: {
				_id: "$_id",
				title: "$plan_title",
				createdAt: "$createdAt",
				plan_price: "$plan_price",
				'date': {
					$dateToString: {
						format: "%Y-%m-%d",
						date: "$createdAt"
					}
				},
			}
			
		},
		{
			$match: condition
		},
		{
		$group: {
			'_id': {
				title: "$title"
			},
			'sum': {
				$sum: '$plan_price'
			},
		}
	}
	]);

	} 

	async upgradablePlanList(condition)
	{
		return await Plan.aggregate([
		{
			$match: { price : { $gt: condition } }
		}
	]);

	} 

	async downgradablePlanList(condition)
	{
		return await Plan.aggregate([
		{
			$match: { price : { $lt: condition } }
		}
	]);

	} 
}

module.exports = new PlanRepository();