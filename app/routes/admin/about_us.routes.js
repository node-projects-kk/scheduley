const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const about_usController = require('about_us/controllers/about_us.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/about_us/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({
	storage: Storage
});

const request_param = multer();

namedRouter.all('/about_us*', auth.authenticate);

namedRouter.get("about_us.list", '/about_us/list', request_param.any(), async (req, res) => {
	try {
		const success = await about_usController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('about_us/views/list.ejs', {
			page_name: 'about_us-management',
			page_title: 'About Us List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		// console.log('error', error);
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('about_us.list'));
	}
});




/*
// @Route: Render Edit about_us
*/
namedRouter.get("about_us.edit", '/about_us/edit/:id', async (req, res) => {
	try {
		const success = await about_usController.edit(req);
		//  console.log(req.user)        
		return res.render('about_us/views/edit.ejs', {
			page_name: 'about_us-management',
			page_title: 'Update about_us',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		return res.status(error.status).send(error);
	}
});

/*
// @Route: Update about_us Action
*/
namedRouter.post("about_us.update", '/aboutUs/update', uploadFile.any(), async (req, res) => {
	const about_usId = req.body.pro_id;
	try {
		// console.log("REQ BODY",req.body)
		const success = await about_usController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('about_us.list'));
	} catch (error) {
		// console.log('error', error);
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('about_us.edit'));
	}
});



// Export the express.Router() instance
module.exports = router;