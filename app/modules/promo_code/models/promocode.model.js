const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["Active", "Inactive"];
const deletestatus = [false, true];
const ptype = ["General", "User", "Subscription"];

const PromoCodeSchema = new Schema({
	promo_type: {
		type: String,
		default: "General",
		enum: ptype
	},
	title: {
		type: String,
		default: ''
	},
	slug: {
		type: String,
		default: ''
	},
	price: {
		type: Number,
		default: 0
	},
	expiry_date: {
		type: Date,
		default: ''
	},
	promo_count: {
		type: Number,
		default: 0
	},
	user_id: [{
		type: Schema.Types.ObjectId,
		ref: 'User',
		default: null
	}],
	plan_id: [{
		type: Schema.Types.ObjectId,
		ref: 'Plan',
		default: null
	}],
	status: {
		type: String,
		default: "Active",
		enum: status
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deletestatus
	},
}, {
	timestamps: true
});

// For pagination
PromoCodeSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('PromoCode', PromoCodeSchema);