const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const VisitSchema = new Schema({
  count: {
    type: Number,
    default: 1
  },
  country: {
    type: String,
    default: ''
  },
  ip: {
    type: String,
    default: ''
  },
  status: {
    type: Boolean,
    default: true,
    enum: [true, false]
  },
}, {
  timestamps: true
});

// For pagination
VisitSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('DailyVisit', VisitSchema);