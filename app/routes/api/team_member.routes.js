const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();

const teamController = require('webservice/team_member.controller');

/**
 * @api {post} /team/storeTeamMember/ Store Team Members
 * @apiVersion 1.0.0
 * @apiGroup Team
 * @apiParam firstName First name
 * @apiParam lastName Last Name
 * @apiParam email Email Address
 * @apiParam password Password
 * @apiParam token token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "facebook": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "facebook_id": "",
            "page_id": "",
            "page_name": "",
            "page_access_token": ""
        },
        "twitter": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "twitter_id": ""
        },
        "pinterest": {
            "first_name": "",
            "last_name": "",
            "pinterest_id": "",
            "access_token": "",
            "expiry_time": "",
            "board_name": "",
            "board_url": "",
            "board_id": ""
        },
        "instagram": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "instagram_id": ""
        },
        "firstName": "Abc",
        "lastName": "xyz",
        "email": "architkumar.singh@webskitters.com",
        "password": "$2a$08$Nu2gfS1eFEwIo2v92L9oZ.SRl69Vhx71K/T8YGPcg1Kbax7Nz/upy",
        "contactNumber": "",
        "role": [],
        "regType": "normal",
        "social_id": "",
        "parent_id": "5d89fe1839da2f4d72ab455e",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "token": "071825f2bad34367cfe000388be8aafd3859dd4503fc356e225591ce23e8d8831014641c0c689da7",
        "stripe_subscription_id": "",
        "stripe_customer_id": "",
        "stripe_plan_id": "",
        "isSubscribed": false,
        "isDeleted": false,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d8dd15b490f6c30d7a10560",
        "createdAt": "2019-09-27T09:07:39.702Z",
        "updatedAt": "2019-09-27T09:07:39.702Z",
        "__v": 0
    },
    "message": "Invitation Link Sent."
}
*/
namedRouter.post("api.team.storeTeamMember", '/team/storeTeamMember/', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.storeTeamMember(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
	}
});

namedRouter.all('/team*', auth.authenticateAPI);

/**
 * @api {post} /team/sendInvitationLink/ Send Invitation Link
 * @apiVersion 1.0.0
 * @apiGroup Team
 * @apiHeader x-access-token User's Access Token
 * @apiParam email Email
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Invitation Link Sent."
}
*/
namedRouter.post("api.team.sendInvitationLink", '/team/sendInvitationLink/', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.sendInvitationLink(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
	}
});



module.exports = router;