const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["Active", "Inactive"];
const deletestatus = [false, true];

const TeamSchema = new Schema({
	firstName: {
		type: String,
		default: ''
	},
	lastName: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: 0
	},
	designation: {
		type: String,
		default: ''
	},
	instagram: {
		type: String,
		default: ''
	},
	facebook: {
		type: String,
		default: ''
	},
	image: {
		type: String,
		default: ''
	},
	status: {
		type: String,
		default: "Active",
		enum: status
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deletestatus
	},
}, {
	timestamps: true
});

// For pagination
TeamSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Team', TeamSchema);