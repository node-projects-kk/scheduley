const userReport = require('user/models/user.model');
const planRepo = require('plan/models/plan.model');
const promoRepo = require('promo_code/models/promocode.model');
const transactionRepo = require('user/models/transaction.model');
const perPage = config.PAGINATION_PERPAGE;


class UserReportRepository {
	constructor() { }

	async getAll(searchQuery) {
		try {
			const query = [{
				"isDeleted": false
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'title': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};

			return await PromoCode.aggregate([{
				"$lookup": {
					"from": "plans",
					"localField": "plan_id",
					"foreignField": "_id",
					"as": "plan_details"
				},
			},
			{
				$unwind: {
					path: "$plan_details",
					preserveNullAndEmptyArrays: true
				}
			},
			{
				"$lookup": {
					"from": "users",
					"localField": "user_id",
					"foreignField": "_id",
					"as": "user_details"
				},
			},
			{
				$unwind: {
					path: "$user_details",
					preserveNullAndEmptyArrays: true
				}
			},
			{
				$group: {
					"_id": "$_id",
					"title": {
						$first: "$title"
					},
					"status": {
						$first: "$status"
					},
					"slug": {
						$first: "$slug"
					},
					"price": {
						$first: "$price"
					},
					"promo_count": {
						$first: "$promo_count"
					},
					"expiry_date": {
						$first: "$expiry_date"
					},

					"createdAt": {
						$first: "$createdAt"
					},
					"isDeleted": {
						$first: "$isDeleted"
					},
					"promo_type": {
						$first: "$promo_type"
					},
					"plan": {
						$first: "$plan_details"
					},
					"user": {
						$first: "$user_details"
					},
				}
			},
			{
				$project: {
					_id: 1,
					title: 1,
					status: 1,
					slug: 1,
					price: 1,
					promo_count: 1,
					expiry_date: 1,
					createdAt: 1,
					isDeleted: 1,
					promo_type: 1,
					plan: 1,
					user: 1
				}
			},
			{
				$match: searchQuery
			},
			]).exec();

			// return await PromoCode.aggregatePaginate(aggregate, {
			// 	page: page,
			// 	limit: perPage
			// }); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await PromoCode.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await PromoCode.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			return await PromoCode.find(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Plan.findById(id).lean().exec();
			return await PromoCode.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await PromoCode.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getCmsCount() {
		try {
			return await PromoCode.count({
				'status': "Active"
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new PromoCode(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}


	async getAllNewUser(condition) {
		try {
			return await userReport.aggregate([{
				"$lookup": {
					"from": "roles",
					"localField": "role",
					"foreignField": "_id",
					"as": "user_role"
				},
			},
			{
				$unwind: {
					path: "$role_details",
					preserveNullAndEmptyArrays: true
				}
			},
			{
				"$match": {
					"isDeleted": false,
					"status": 'Active',
					'user_role.role': {
						$ne: 'admin'
					}
				}
			},
			{
				$project: {
					'month': {
						$dateToString: {
							format: "%m",
							date: "$createdAt"
						}
					},
					'year': {
						$dateToString: {
							format: "%Y",
							date: "$createdAt",
						}
					},
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$createdAt"
						}
					},
					'user_role.role': 1
				}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': {
						month: "$month",
						year: "$year",
						date: "$date"
					},
					'count': {
						$sum: 1
					}
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getTotalRevenue(condition) {
		try {
			return await transactionRepo.aggregate([
			{
				$project: {
					'month': {
						$dateToString: {
							format: "%m",
							date: "$createdAt"
						}
					},
					'year': {
						$dateToString: {
							format: "%Y",
							date: "$createdAt",
						}
					},
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$createdAt"
						}
					},
					'plan_title': 1,
					'plan_price': 1
				}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': {
						month: "$month",
						year: "$year"
					},
					'sum': {
						$sum: '$plan_price'
					}
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getAllSubscribeUser(condition) {
		try {
			return await planRepo.aggregate([{
				"$lookup": {
					"from": "users",
					"localField": "_id",
					"foreignField": "plan",
					"as": "user_details"
				},
			},
			{
				$unwind: {
					path: "$user_details",
					preserveNullAndEmptyArrays: true
				}
			},
			{
				"$match": {
					"isDeleted": false,
					"status": 'Active',
				}
			},
			{
				$project: {
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$user_details.createdAt"
						}
					},
					'user_details.plan': 1,
					'title': 1
				}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': {
						/* // month: "$month",
						month: {
							"$cond": {
								if: {
									$ne: ["$user_details", '$user_details.createdAt']
								},
								then: "$month",
								else: 0
							}
						},
						// year: "$year",
						year: {
							"$cond": {
								if: {
									$ne: ["$user_details", '$user_details.createdAt']
								},
								then: "$year",
								else: 0
							}
						}, */
						title: '$title',
						/* date: '$date' */
					},

					"count": {
						"$sum": {
							"$cond": {
								if: {
									$eq: ["$user_details", '$user_details.plan']
								},
								then: 0,
								else: 1
							}
						}
					},
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getPromoCodeUsageStatus(condition) {
		try {
			return await promoRepo.aggregate([{
				"$lookup": {
					"from": "users",
					"localField": "_id",
					"foreignField": "promo_code",
					"as": "user_info"
				},
			},
			{
				$unwind: "$user_info"
			},
			{
				"$match": {
					"isDeleted": false,
					"user_info.isDeleted": false
				}
			},
			{
				$project: {
					'title': '$title',
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$user_info.createdAt"
						}
					},
					'isDeleted': 1
				}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': {
						title: "$title",
						month: "$month",
						year: "$year"
					},
					'count': {
						$sum: 1
					}
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getTrialUserCount(condition) {
		try {
			return await userReport.aggregate([{
				"$lookup": {
					"from": "roles",
					"localField": "role",
					"foreignField": "_id",
					"as": "role_info"
				},
			},
			{
				$unwind: "$role_info"
			},
			{
				$project: {
					'paymentStatus': "$paymentStatus",
					'initialPaymentStatus': "$initialPaymentStatus",
					'role_info.role': 1,
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$createdAt"
						}
					},
				}
			},
			{ $match: { 'role_info.role': {$ne: 'admin'}}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': {
						paymentStatus: "$paymentStatus",
						initialPaymentStatus: "$initialPaymentStatus",
					},
					count: {
						$sum: 1
					}
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getPaidUserCount() {
		try {
			return await userReport.count({
				'paymentStatus': true,
				'isDeleted': false
			});
		} catch (error) {
			return error;
		}
	}

	async getActiveUsers() {
		try {
			return await userReport.aggregate([{
				"$lookup": {
					"from": "roles",
					"localField": "role",
					"foreignField": "_id",
					"as": "role_info"
				},
			},
			{
				$unwind: "$role_info"
			},
			{
				$match: {
					'isDeleted': false,
					'role_info.role': {
						$ne: 'admin'
					},
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getTransactionDetails(condition) {
		try {
			return await transactionRepo.aggregate([{
				"$lookup": {
					"from": "users",
					"localField": "user_id",
					"foreignField": "_id",
					"as": "user_info"
				},
			},
			{
				$unwind: "$user_info"
			},
			{
				$project: {
					'firstName': "$user_info.firstName",
					'lastName': "$user_info.lastName",
					'email': "$user_info.email",
					'plan_title': "$plan_title",
					'plan_price': "$plan_price",
					'user_id': "$user_id",
					'date': {
						$dateToString: {
							format: "%Y-%m-%d",
							date: "$createdAt"
						}
					},
				}
			},
			{
				$match: condition
			},
			{
				$group: {
					'_id': "$user_id",
					firstName: {
						$first: "$firstName"
					},
					lastName: {
						$first: "$lastName"
					},
					email: {
						$first: "$email"
					},
					plan_title: {
						$addToSet: "$plan_title"
					},
					plan_price: {
						$addToSet: "$plan_price"
					},
					count: {
						$sum: 1
					}
				}
			}
			]).exec();
		} catch (error) {
			return error;
		}
	}

}

module.exports = new UserReportRepository();