/* const pageRepo = require('page/repositories/page.repository'); */
const settingRepo = require('setting/repositories/setting.repository');
const companyRepo = require('company_management/repositories/company.repository');
/* exports.getPage = async req => {
	try {
		const rolling_benches = await pageRepo.getByField({ slug: "rolling-benches", status: 'Active' });
		const why_choosing_us = await pageRepo.getByField({ slug: "why-choosing-us", status: 'Active' });
		const advantages = await pageRepo.getByField({ slug: "advantages", status: 'Active' });
		const bottom_types = await pageRepo.getByField({ slug: "bottom-types", status: 'Active' });
		return { status: 200, data: {"rolling_bench":rolling_benches,"why_choosing_us":why_choosing_us,"advantage":advantages,"bottom_type":bottom_types}, message: 'Pages fetched Successfully' };
	}
	catch (error) {
		return { "success": false, "status": 500, data: [], "message": 'Something went wrong' }
	}
};
 */

exports.getLeaveMessage =  async (req, res) => {
    try {
       const settings = await settingRepo.getByField({'slug':"webmaster_mail"});
       let master_email = settings.setting_value;
       let email = req.body.email; 
       let message = req.body.message;  
       var html = 'Hello Admin,';
       html += "<p>A mail has come from  " + email + "</p>";
       html += "<p><b>Message:</b> "+ message + "</p>";
       
       await transporter.sendMail({
                from: email,
                to: master_email,
                //to: 'subhasish.ghosh@webskitters.com',
                subject: 'Leave Message',
                html: html
        });
       
        return { status: 200, data: [], message: 'Message send successfully' };
    }
    catch (error) {
        return { "success": false, "status": 500, data: [], "message": 'Something went wrong' }
    }
};

//to save companies. 
exports.save = async req => {
	try {
        req.body.user_id = req.user._id;
        let companySave = await companyRepo.save(req.body);
        return { status: 200, data: companySave, message: 'Company saved successfully' }
			}
	catch (error) {
		return { "success": false, "status": 500, data: [], "message": 'Something went wrong' }
	}
};

exports.getDetails = async req => {
	try {
        let company = await companyRepo.getAllByField({"user_id": req.user._id});
        return { status: 200, data: company, message: 'Company details fetched successfully' }
			}
	catch (error) {
		return { "success": false, "status": 500, data: [], "message": 'Something went wrong' }
	}
};