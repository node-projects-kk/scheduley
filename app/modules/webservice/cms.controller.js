const homeRepo = require('home/repositories/home.repository');
const caseRepo = require('case_studies/repositories/case_studies.repository');
const featureRepo = require('product/repositories/product.repository');
const aboutRepo = require('about_us/repositories/about_us.repository');
const teamRepo = require('team_management/repositories/team.repository');
const faqRepo = require('faq/repositories/faq.repository');


/* 
// @Method: homeList
// @Description: To get all the home page details from DB
*/
exports.homeList = async req => {
    try {
        var homeResult = await homeRepo.getAllHome();
        const caseResult = await caseRepo.getAllCaseStudy();
        // var homeObj = {};
        homeResult[0].casestudies = caseResult;

        return {
            "status": 200,
            data: homeResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* 
// @Method: featureList
// @Description: To get all the Feature page details from DB
*/
exports.featureList = async req => {
    try {
        var featureResult = await featureRepo.getAllProduct();
        return {
            "status": 200,
            data: featureResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* 
// @Method: aboutList
// @Description: To get all the About page details from DB
*/
exports.aboutList = async req => {
    try {
        var aboutResult = await aboutRepo.getAllAbout();
        const teamResult = await teamRepo.getAllTeam();
        // var homeObj = {};
        aboutResult[0].team_data = teamResult;

        return {
            "status": 200,
            data: aboutResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* 
// @Method: aboutList
// @Description: To get all the About page details from DB
*/
exports.faqList = async req => {
    try {
        var faqResult = await faqRepo.getAllFaq();
        return {
            "status": 200,
            data: faqResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};