const Product = require('product/models/product.model');
const perPage = config.PAGINATION_PERPAGE;

class ProductRepository {
    constructor() {}

    async getAll(searchQuery, sortOrder) {
        try {
            const query = [{
                "status": "active"
            }];
            // serach by keyword
            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    const search = searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                            'title': {
                                '$regex': search,
                                '$options': 'i'
                            }
                        }]
                    });
                }
            }
            searchQuery = {
                "$and": query
            };
            return await Product.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $match: searchQuery
                },
                {
                    $project: {
                        _id: "$_id",
                        banner_title: "$banner_block.title",
                        desc: "$desc",
                        status: "$status",
                        slug: "$slug"
                    }
                }
            ]);
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await Product.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await Product.findOne(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await Product.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Product.findById(id).lean().exec();
            return await Product.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Product.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    // async getCmsCount() {
    //     try {
    //         return await Product.countDocuments({
    //             'status': "active"
    //         });
    //     } catch (error) {
    //         return error;
    //     }
    // }

    async save(data) {
        try {
            return await Product.create(data).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllProduct() {
        try {
            return await Product.find().lean().exec();
        } catch (error) {
            return error;
        }
    }

}

module.exports = new ProductRepository();