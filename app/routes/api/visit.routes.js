const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const visitController = require('visit/controllers/visit.api.controller');


//const request_param = multer();
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
            cb(null, './public/uploads/user')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})
const request_param = multer({
    storage: storage
});


/**
 * @api {post} /visit/save Save visit count
 * @apiVersion 1.0.0
 * @apiGroup visit
 * @apiParam {string} country Country name
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "count": 2,
        "country": "usa",
        "isDeleted": false,
        "status": true,
        "_id": "5d234423c49ee96ba1d81739",
        "createdAt": "2019-07-08T13:24:51.736Z",
        "updatedAt": "2019-07-08T13:25:00.372Z",
        "__v": 0
    },
    "message": "Saved Successfully!!"
}
*/
namedRouter.post("api.visit.save", '/visit/save', request_param.any(), async (req, res) => {
    try {
        const success = await visitController.save(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


module.exports = router;