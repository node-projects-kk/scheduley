const about_usRepo = require('about_us/repositories/about_us.repository');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');

exports.list = async req => {
    try {
        const sortOrder = {
            '_id': -1
        };
        const data = await about_usRepo.getAll(req.query, sortOrder);
        return {
            "status": 200,
            "data": data,
            "message": "About us fetched successfully"
        };
    } catch (error) {
        throw error;
    }
};



/*
// @Method: edit
// @Description:  render about_us update page
*/
exports.edit = async req => {
    try {
        const data = await about_usRepo.getById(req.params.id);
        return {
            "status": 200,
            data,
            "message": "about_us Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};


exports.updatebackup = async req => {
    try {
        const olddata = await about_usRepo.getById(req.body.pro_id);
        console.log('olddata', olddata);

        if (req.files.length > 0) {
            // console.log('req.files', req.files)

            if (_.isEmpty(olddata)) {
                fs.unlink('public/uploads/about_us/' + olddata.image, function (err) {
                    // console.log('err 70', err);
                    if (err) throw new Error(err.message);
                });

                fs.unlink('public/uploads/about_us/thumb/' + olddata.image, function (err) {
                    // console.log('err 75', err);
                    if (err) throw new Error(err.message);
                });
            }
            gm('public/uploads/about_us/' + req.files[0].filename).resize(100).write('public/uploads/about_us/thumb/' + req.files[0].filename, function (err) {
                if (err) throw new Error(err.message);
            });

            req.body.image = req.files[0].filename;

            // console.log(JSON.stringify(req.body.banner_block));
        }

        // process.exit();

        const result = await about_usRepo.updateById(req.body, req.body.pro_id);
        console.log('result <> 226', result);
        return {
            "status": 200,
            data: result,
            "message": "About us updated Successfully"
        };

    } catch (error) {
        throw error;
    }
};


exports.update = async req => {
    try {
        const olddata = await about_usRepo.getById(req.body.pro_id);
       // console.log('olddata', olddata);


        if (_.has(req, 'files')) {
            if (req.files.length > 0) {
                req.files.forEach(function (file) {
                    if (file.fieldname == 'image') {
                        if (!_.isEmpty(olddata.image)) {
                            if (fs.existsSync('public/uploads/about_us/' + olddata.image)) {
                                const upl_img = fs.unlinkSync('public/uploads/about_us/' + olddata.image);
                            }
                            if (fs.existsSync('public/uploads/about_us/thumb/' + olddata.image)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/about_us/thumb/' + olddata.image);
                            }
                        }
                        gm('public/uploads/about_us/' + file.filename)
                            .resize(100)
                            .write('public/uploads/about_us/thumb/' + file.filename, function (err) {
                                if (err) throw new Error(err.message);
                            });
                        req.body.image = file.filename;
                    }

                    if (file.fieldname == 'about_image') {
                        if (!_.isEmpty(olddata.about_image)) {
                            if (fs.existsSync('public/uploads/about_us/' + olddata.about_image)) {
                                const upl_img = fs.unlinkSync('public/uploads/about_us/' + olddata.about_image);
                            }
                            if (fs.existsSync('public/uploads/about_us/thumb/' + olddata.about_image)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/about_us/thumb/' + olddata.about_image);
                            }
                        }
                        gm('public/uploads/about_us/' + file.filename)
                            .resize(100)
                            .write('public/uploads/about_us/thumb/' + file.filename, function (err) {
                                if (err) {
                                    req.flash('error', err.message);
                                    res.redirect(namedRouter.urlFor('meals.create'));
                                }
                            });
                        req.body.about_image = file.filename;
                    }
                });
            }
        }
        const result = await about_usRepo.updateById(req.body, req.body.pro_id);
        return {
            "status": 200,
            data: result,
            "message": "About us updated Successfully"
        };

    } catch (error) {
        throw error;
    }
};