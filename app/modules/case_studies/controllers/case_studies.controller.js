const caseRepo = require('case_studies/repositories/case_studies.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');

/* 
// @Method: list
// @Description: To get all the teams from DB
*/
exports.list = async req => {
	try {
		const result = await caseRepo.getAll(req.query, {
			'createdAt': -1
		});
		return {
			"status": 200,
			data: result,
			"message": "Case Study fetched successfully"
		};
	} catch (err) {
		throw err;
	}
};


exports.create = async (req, res) => {
	try {
		return {
			"status": 200,
			data: [],
			"message": "Case Study fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};

exports.store = async req => {
	try {
		if (req.files) {
			if (req.files[0].fieldname == 'logo_image') {
				gm('public/uploads/case/' + req.files[0].filename).resize(100).write('public/uploads/case/thumb/' + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				req.body.logo_image = req.files[0].filename;
			}
		}
		const result = await caseRepo.save(req.body);
		return {
			"status": 200,
			data: result,
			"message": 'Case Study stored successfully'
		};
	} catch (error) {
		throw error;
	}
};

/*
// @Method: statusChange
// @Description: Cms status change action
*/
exports.statusChange = async req => {
	try {
		const result = await caseRepo.getById(req.body.id);
		const updatedStatus = (_.has(result, 'status') && result.status == "inactive") ? "active" : "inactive";
		const result2 = await caseRepo.updateById({
			'status': updatedStatus
		}, req.body.id);
		return {
			"status": 200,
			data: result2,
			"message": "Case Study status has changed successfully"
		}
	} catch (error) {
		throw error;
	}
};

/*
// @Method: edit
// @Description:  render team update team
*/
exports.edit = async req => {
	try {
		const data = await caseRepo.getById(req.params.id);
		return {
			"status": 200,
			data,
			"message": "Case Study details fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};


/* 
// @Method: update
// @Description: Cms update action
*/
exports.update = async req => {
	try {
		let arr = [];
		const olddata = await caseRepo.getById(req.body.id);

		if (req.files.length > 0) {
			if (!_.isEmpty(olddata)) {
				if (!_.isEmpty(olddata) && olddata.logo_image != '') {
					if (fs.existsSync('public/uploads/case/' + olddata.logo_image)) {
						const upl_resume = fs.unlinkSync('public/uploads/case/' + olddata.logo_image);
					}
					if (fs.existsSync('public/uploads/case/thumb/' + olddata.logo_image)) {
						const upl_thumb_resume = fs.unlinkSync('public/uploads/case/thumb/' + olddata.logo_image);
					}
				}
			}
			else {
				req.body.logo_image = olddata.logo_image;
			}
			gm('public/uploads/case/' + req.files[0].filename).resize(100).write('public/uploads/case/thumb/' + req.files[0].filename, function (err) {
				if (err) throw new Error(err.message);
			});
			req.body.logo_image = req.files[0].filename;
		}
		const result = await caseRepo.updateById(req.body, req.body.id);
		return {"status": 200,data: result,"message": "Case Study updated Successfully"};
	}
	catch (error) {
		console.log("147>>",error.message);
		throw error;
	}
};

/* 
// @Method: delete
// @Description: Cms delete
*/
exports.destroy = async req => {
	try {
		const olddata = await caseRepo.getById(req.params.id);
		if (!_.isEmpty(olddata)) {

			fs.unlink('public/uploads/case/' + olddata.logo_image, function (err) {
				if (err) throw new Error(err.message);
			});

			fs.unlink('public/uploads/case/thumb/' + olddata.logo_image, function (err) {
				if (err) throw new Error(err.message);
			});
		}
		const result = await caseRepo.updateById({
			'isDeleted': true
		}, req.params.id);
		return {
			"status": 200,
			data: result,
			"message": "Case Study Removed Successfully"
		};
	} catch (error) {
		throw error;
	}
};