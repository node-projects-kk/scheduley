const visitRepo = require('visit/repositories/visit.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const moment = require('moment');

class visitController {
    constructor() {
        this.visit = [];
    }

    async save(req, res) {
        try {
           let visit = await visitRepo.getByField({"country": req.body.country})
           let visitAdd ='';
           if(visit)
           {
               let count=visit.count+1;
            visitAdd = await visitRepo.updateById({'count':count}, visit._id)
           }
           else
           {
            visitAdd = await visitRepo.save(req.body)
           }
            if (visitAdd) {
                return {
                    status: 200,
                    data: visitAdd,
                    message: "Saved Successfully!!"
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: "something went wrong!!"
                }
            }
        } catch (e) {
            console.log("41", e)
            return res.status(500).send({ message: e.message });
        }
    };

}

module.exports = new visitController();