const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const teamController = require('team_management/controllers/team.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/team/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});
const uploadFile = multer({
	storage: Storage,
	fileFilter: (req, file, cb) => {
		// accept image only
		if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
			return cb(new Error('Only image files are allowed!'), false);
		}
		cb(null, true);
	}
});
const request_param = multer();


namedRouter.all('/team*', auth.authenticate);

/*
// @Route: CMS List
*/
namedRouter.get("team.list", '/team/list', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('team_management/views/list.ejs', {
			page_name: 'team-management',
			page_title: 'team List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	}
});

/*
// @Route: Render Create CMS
*/
namedRouter.get("team.create", '/team/create', async (req, res) => {
	try {
		const success = await teamController.create(req);
		return res.render('team_management/views/add.ejs', {
			page_name: 'team-management',
			page_title: 'Create team member',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('team.list'));
	}
});

/*
// @Route: Create CMS Action
*/
namedRouter.post("team.store", '/team/store', uploadFile.any(), async (req, res) => {
	try {
		const success = await teamController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('team.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: CMS status change
*/
namedRouter.post('team.status.change', '/team/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	}
});

/*
// @Route: Render Edit CMS
*/
namedRouter.get("team.edit", '/team/edit/:id', async (req, res) => {
	try {
		const success = await teamController.edit(req);
		return res.render('team_management/views/edit.ejs', {
			page_name: 'team-management',
			page_title: 'Update team',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update CMS Action
*/
namedRouter.post("team.update", '/team/update', uploadFile.any(), async (req, res) => {
	const teamId = req.body.team_id;
	try {
		const success = await teamController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('team.edit', {
			id: teamId
		}));
	}
});

/*
// @Route: Delete CMS
*/
namedRouter.get('team.delete', '/team/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await teamController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('team.list'));
	}
});

// Export the express.Router() instance
module.exports = router;