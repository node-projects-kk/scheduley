const mongoose = require('mongoose');
const Visit = require('visit/models/visit.model');
const perPage = config.PAGINATION_PERPAGE;

const VisitRepository = {

    save: async (data) => {
        try {
            let Visit1 = await Visit.create(data);

            if (!Visit1) {
                return null;
            }
            return Visit1;
        } catch (e) {
            return e;
        }
    },

    getById: async (id) => {
        let Visit1 = await Visit.findById(id).lean().exec();
        try {
            if (!Visit1) {
                return null;
            }
            return Visit1;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let Visit1 = await Visit.findOne(params).lean().exec();
        try {
            if (!Visit1) {
                return null;
            }
            return Visit1;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let Visit1 = await Visit.find(params).exec();
        try {
            if (!Visit1) {
                return null;
            }
            return Visit1;

        } catch (e) {
            return e;
        }
    },

    getVisitCount: async (params) => {
        try {
            let VisitCount = await Visit.countDocuments(params);
            if (!VisitCount) {
                return null;
            }
            return VisitCount;
        } catch (e) {
            return e;
        }

    },

    delete: async (id) => {
        try {
            let Visit = await Visit.findById(id);
            if (Visit) {
                let VisitDelete = await Visit.findByIdAndUpdate(id, {
                    isDeleted: true
                }, {
                    new: true
                }).exec();
                if (!VisitDelete) {
                    return null;
                }
                return VisitDelete;
            }
        } catch (e) {
            throw e;
        }
    },


    updateById: async (data, id) => {
        try {
            let Visit1 = await Visit.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!Visit1) {
                return null;
            }
            return Visit1;
        } catch (e) {
            return e;
        }
    },

    async getAllVisit(condition) {
        try {
            return await Visit.aggregate([{
                    $project: {
                        _id: '$_id',
                        country: '$country',
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$createdAt"
                            }
                        }
                    }
                },
                {
                    $match: condition
                },
                {
                    $group: {
                        '_id': '$country',
                        'count': {
                            $sum: 1
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

};

module.exports = VisitRepository;