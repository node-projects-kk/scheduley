const planRepo = require('plan/repositories/plan.repository');
//const languageRepo = require('language/repositories/language.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');
const moment= require('moment');
const stripe = require("stripe")(config.stripe_secret_key);

exports.create = async (req, res) => {
	try {
		//const result = await languageRepo.getAllLanguage();
		// console.log("controller", result)
		return {
			"status": 200,
			data: [],
			"message": "Plan fetched successfully"
		};
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error
		};
	}
};

exports.store = async req => {
	try {
		
		if (req.files) {
			if (req.files[0].fieldname == 'image') {
				gm('public/uploads/plan/' + req.files[0].filename).resize(100).write('public/uploads/plan/thumb/' + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				req.body.image = req.files[0].filename;
			}
		}

		req.body.slug = slugify(req.body.title, {
			replacement: '-',
			lower: true
		});
		req.body.price = parseFloat(req.body.price);
		
		var plan = await stripe.plans.create({
			amount: req.body.price * 100,
			interval: "month",
			product: {name: req.body.title},
			currency: "usd",
		});
		req.body.stripe_plan_id = plan.id;
		const result = await planRepo.save(req.body);
		console.log(result);
		return {
			"status": 200,
			data: result,
			"message": 'Plan created successfully'
		};
	} catch (error) {
		throw error;
	}
};

/*
// @Method: edit
// @Description:  render plan update plan
*/
exports.edit = async req => {
	try {
		const data = await planRepo.getById(req.params.id);
		return { "status": 200, data, "message": "Plan fetched successfully" };
	} catch (error) {
		throw error;
	}
};

/* 
// @Method: update
// @Description: Cms update action
*/
exports.update = async req => {
	try {
		let arr = [];
		const olddata = await planRepo.getById(req.body.id);
		if (req.files.length > 0) {
			if (_.isEmpty(olddata)) {
				fs.unlink('public/uploads/plan/' + olddata.image, function (err) {
					if (err) throw new Error(err.message);
				});

				fs.unlink('public/uploads/plan/thumb/' + olddata.image, function (err) {
					if (err) throw new Error(err.message);
				});
			}
			gm('public/uploads/plan/' + req.files[0].filename).resize(100).write('public/uploads/plan/thumb/' + req.files[0].filename, function (err) {
				if (err) throw new Error(err.message);
			});
			req.body.image = req.files[0].filename;
		}
				
		const result = await planRepo.updateById(req.body, req.body.id);
		return { "status": 200, data: result, "message": "Plan updated Successfully" };

	} catch (error) {
		console.log(error.message);
		throw error;
	}
};


/* 
// @Method: list
// @Description: To get all the plans from DB
*/
exports.list = async req => {
	try {
		let from = moment().format('YYYY-MM-DD');
		let to = moment().format('YYYY-MM-DD');
		let condition={};
		if (req.params.from) {
			condition = {
				$and: [{"date": {$gte: req.params.from}},
					{"date": {$lte: req.params.to}}]
			}
		}
		else {
			condition = {
					"date": {$lte: to}
			}
		}
		let planRevenue = await planRepo.getALLTransaction(condition);
		const currentPage = req.query.page || 1;
		const {
			data,
			pageCount,
			totalCount
		} = await planRepo.getAll(req.query, {
			'plan.title': 1
		}, (req.query.page || 1));
		let newData=[];
		data.map(item =>{
			planRevenue.map(data =>{
				if(item.title==data._id.title)
				{
					item.sum=data.sum;
				}
			})
			newData.push(item);
		});

		// var stripePlans = await stripe.plans.list({limit: 3});
		// console.log('all plans',stripePlans);
		return {
			"status": 200,
			newData,
			current: currentPage,
			pages: pageCount,
			"message": "Plan list fetched successfully"
		};
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error
		};
	}
};

/*
// @Method: statusChange
// @Description: Cms status change action
*/
exports.statusChange = async req => {
	try {
		const result = await planRepo.getById(req.body.id);
		const updatedData = (_.has(result, 'status') && result.status == "Inactive") ? { 'status' : 'Active', 'inactiveDate':null } : { 'status' : 'Inactive','inactiveDate': new Date() };
		console.log(updatedData);
		const result2 = await planRepo.updateById(updatedData, req.body.id);
		return { "status": 200, data: result2, "message": "Plan status has changed successfully" }
	} catch (error) {
		return { "status": 500, data: [], "message": error };
	}
};

/* 
// @Method: delete
// @Description: Cms delete
*/
exports.destroy = async req => {
	try {
		const result = await planRepo.updateById({
			'isDeleted': true
		}, req.params.id);
		return {
			"status": 200,
			data: result,
			"message": "Plan Removed Successfully"
		};
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error
		};
	}
};