const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const userController = require('user/controllers/user.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {

		if (file.fieldname === 'company_logo') {
			callback(null, "./public/uploads/company")
		} else if (file.fieldname === 'profile_image') {
			callback(null, "./public/uploads/user");
		}
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({
	storage: Storage
});
const request_param = multer();

namedRouter.get('user.login', '/', userController.loginPage);

namedRouter.post("user.login.process", '/login', request_param.any(), async (req, res) => {
	try {
		const success = await userController.signin(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.dashboard'));

	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.post('user.forgotPass.process', '/user/forgotpassword', request_param.any(), async (req, res) => {
	try {
		const success = await userController.forgotPassword(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.get('user.logout', "/logout", userController.logout);
namedRouter.all('/*', auth.authenticate);

namedRouter.get("user.dashboard", '/dashboard/:from?/:to?', request_param.any(), async (req, res) => {
	try {
		var roleType = req.user.role[0].role;
		const success = await userController.dashboard(req);
		return res.render('user/views/dashboard.ejs', {
			page_name: 'user-dashboard',
			page_title: 'Dashboard',
			user: req.user,
			setting: req.session.setting,
			response: success,
			from: req.params.from,
			to: req.params.to
		})
	} catch (error) {
		console.log("70", error)
		return res.status(error.status).send(error);
	}
});

namedRouter.get('user.dashboard.charts', '/dashboardCharts/:from?/:to?/:status?', request_param.any(), async (req, res) => {
	try {

		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.DashboardCharts(req);
		const response = success;
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', error.message);
		// return res.redirect(namedRouter.urlFor('company.user.list'));
	}
});

namedRouter.get("user.list", '/user/list', request_param.any(), async (req, res) => {
	try {
		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.list(req);
		// For search string //
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";

		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			const serachQryStr = querystring.stringify(searchStr);
			success.search_param = "&" + serachQryStr;
		}

		return res.render('user/views/list.ejs', {
			page_name: 'user-management',
			page_title: 'Users List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success.data,
		});
	} catch (error) {
		req.flash('error', error.message);
		res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.get("user.edit", '/user/edit/:id', async (req, res) => {
	try {

		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.edit(req);

		return res.render('user/views/edit.ejs', {
			page_name: 'user-management',
			page_title: 'Update User',
			user: req.user,
			setting: req.session.setting,
			response: success,
		});
	} catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.post("user.update", '/user/update', uploadFile.any(), async (req, res) => {
	try {

		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.update(req);

		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.list'));
	} catch (error) {
		const userId = req.body.user_id;
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.edit', {
			id: userId
		}));
	}
});

namedRouter.get('user.delete', '/user/delete/:id', request_param.any(), async (req, res) => {
	try {

		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.list'));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.list'));
	}
});

namedRouter.get("user.create", '/user/create', (req, res) => {
	try {
		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		req.userrole = 'provider';
		userController.create(req).then(success => {
			res.render('user/views/add.ejs', {
				page_name: 'user-management',
				page_title: 'Create User',
				user: req.user,
				setting: req.session.setting,
				userrole: req.userrole,
				response: success
			});
		}, failure => {
			res.status(failure.status).send(failure);
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('user.list'));
	}
});

namedRouter.post("user.store", '/user/store', uploadFile.any(), (req, res) => {
	try {
		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		userController.store(req).then(success => {
			req.flash('success', success.message);
			res.redirect(namedRouter.urlFor('user.list'));
		}, failure => {
			req.flash('error', failure.message);
			res.redirect(namedRouter.urlFor('user.create'));
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('user.create'));
	}
});

namedRouter.get("user.view", '/user/view/:id', async (req, res) => {
	try {

		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.edit(req);

		return res.render('user/views/viewdetails.ejs', {
			page_name: 'user-management',
			page_title: 'View User Details',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.post('user.status.change', '/user/status-change', request_param.any(), async (req, res) => {
	try {
		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}
		const success = await userController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.list'));
	}
});

namedRouter.get("user.myprofile", '/user/myprofile/:id', async (req, res) => {
	try {
		const success = await userController.viewmyprofile(req);

		return res.render('user/views/myprofile.ejs', {
			page_name: 'user-profile',
			page_title: 'Edit My Profile',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.post("user.profileupdate", '/user/profileupdate', uploadFile.any(), async (req, res) => {
	try {
		const success = await userController.updateprofile(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.dashboard', {
			id: success.data
		}));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.get("user.changepassword", '/user/change-password', async (req, res) => {
	try {
		return res.render('user/views/change_password.ejs', {
			page_name: 'user-changepassword',
			page_title: 'Change Password',
			user: req.user,
			setting: req.session.setting
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.login'));
	}
});

namedRouter.post("user.updatepassword", '/user/update-password', request_param.any(), async (req, res) => {
	try {
		const success = await userController.changepassword(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('user.dashboard'));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('user.changepassword'));
	}
});


namedRouter.get("user.company", '/user/company/:id', request_param.any(), async (req, res) => {
	try {
		var role = req.user.role[0].role;
		if (role != 'admin') {
			req.flash('error', 'Sorry, unauthorized acccess');
			return res.redirect(namedRouter.urlFor('user.dashboard'));
		}

		const success = await userController.companyListing(req);
		// For search string //
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";

		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			const serachQryStr = querystring.stringify(searchStr);
			success.search_param = "&" + serachQryStr;
		}

		return res.render('company_management/views/list.ejs', {
			page_name: 'company-management',
			page_title: 'Company List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success.data,
		});
	} catch (error) {
		req.flash('error', error.message);
		res.redirect(namedRouter.urlFor('user.login'));
	}
});

module.exports = router;