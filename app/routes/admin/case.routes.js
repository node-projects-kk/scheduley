const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const caseController = require('case_studies/controllers/case_studies.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/case/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});
const uploadFile = multer({
	storage: Storage,
	fileFilter: (req, file, cb) => {
		// accept image only
		if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
			return cb(new Error('Only image files are allowed!'), false);
		}
		cb(null, true);
	}
});
const request_param = multer();


namedRouter.all('/case*', auth.authenticate);

/*
// @Route: Case List
*/
namedRouter.get("case.list", '/case/list', request_param.any(), async (req, res) => {
	try {
		const success = await caseController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('case_studies/views/list.ejs', {
			page_name: 'case-study-management',
			page_title: 'Case List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	}
});

/*
// @Route: Render Create Case
*/
namedRouter.get("case.create", '/case/create', async (req, res) => {
	try {
		const success = await caseController.create(req);
		return res.render('case_studies/views/add.ejs', {
			page_name: 'case-study-management',
			page_title: 'Create Case Study',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('case.list'));
	}
});

/*
// @Route: Create Case Action
*/
namedRouter.post("case.store", '/case/store', uploadFile.any(), async (req, res) => {
	try {
		const success = await caseController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('case.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: Case status change
*/
namedRouter.post('case.status.change', '/case/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await caseController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	}
});

/*
// @Route: Render Edit Case
*/
namedRouter.get("case.edit", '/case/edit/:id', async (req, res) => {
	try {
		const success = await caseController.edit(req);
		return res.render('case_studies/views/edit.ejs', {
			page_name: 'case-study-management',
			page_title: 'Update Case Study',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update Case Action
*/
namedRouter.post("case.update", '/case/update', uploadFile.any(), async (req, res) => {
	const caseId = req.body.id;
	try {
		const success = await caseController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('case.edit', {
			id: caseId
		}));
	}
});

/*
// @Route: Delete Case
*/
namedRouter.get('case.delete', '/case/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await caseController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('case.list'));
	}
});

// Export the express.Router() instance
module.exports = router;