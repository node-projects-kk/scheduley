const socialFeedRepo = require('socialfeed/repositories/socialfeed.repository');

/* 
// @Method: addSocialValue
// @Description: Save social media id/token etc in DB
*/
exports.addSocialValue = async req => {
	try {
		let user_id = req.user._id;
		req.body.user_id = user_id;
		console.log("11>>",req.body); process.exit();
		let insertResult = await socialFeedRepo.save(req.body);
		
		if (!_.isEmpty(insertResult)) {
			return {status: 200,	message: "Data Fetched Successfully",	data: insertResult};	
		}
		else{
			return {status: 201,	message: "Record insert failure",	data:[]};	
		}
	}
	catch (error) {
		return {	status: 500,	message: error.message};
	}
};