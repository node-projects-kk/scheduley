/* "use strict";

// Class definition
var KTMorrisChartsDemo = function () {

    var ajaxUrl = apiBaseURL + '/userreport/subscriptionChart';

    $.ajax({
        type: "GET",
        url: ajaxUrl,
        success: function (res) {
            console.log('morris 17', res);
            let labels_heading = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            demo3(res);
        }
    });

    // Private functions

    var demo1 = function () {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_1',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [{
                    y: '2006',
                    a: 100,
                    b: 90
                },
                {
                    y: '2007',
                    a: 75,
                    b: 65
                },
                {
                    y: '2008',
                    a: 50,
                    b: 40
                },
                {
                    y: '2009',
                    a: 75,
                    b: 65
                },
                {
                    y: '2010',
                    a: 50,
                    b: 40
                },
                {
                    y: '2011',
                    a: 75,
                    b: 65
                },
                {
                    y: '2012',
                    a: 100,
                    b: 90
                }
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a', 'b'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Values A', 'Values B']
        });
    }

    var demo2 = function () {
        // AREA CHART
        new Morris.Area({
            element: 'kt_morris_2',
            data: [{
                    y: '2006',
                    a: 100,
                    b: 90
                },
                {
                    y: '2007',
                    a: 75,
                    b: 65
                },
                {
                    y: '2008',
                    a: 50,
                    b: 40
                },
                {
                    y: '2009',
                    a: 75,
                    b: 65
                },
                {
                    y: '2010',
                    a: 50,
                    b: 40
                },
                {
                    y: '2011',
                    a: 75,
                    b: 65
                },
                {
                    y: '2012',
                    a: 100,
                    b: 90
                }
            ],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Series A', 'Series B']
        });
    }

    var demo3 = function (res) {
        let responseData = res.data.subscribe_user_count;
        let labels_heading = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var data1 = [];


        var planArr = [];
        res.data.subscribe_user_count.map(item => {
            if (planArr.indexOf(item._id.title) == -1) {
                planArr.push(item._id.title)
            }
        })

        for (let m = 1; m <= 12; m++) {
            let monthName = labels_heading[m - 1];
            var obj = {};
            obj['y'] = monthName
            for (let p = 0; p < planArr.length; p++) {
                let planName = planArr[p];
                let getMathchObj = responseData.find((o, i) => {
                    let monthVal = (m < 10) ? "0" + m : m;
                    if (o._id.month === monthVal && o._id.title == planName) {
                        return true;
                    }
                });

                if (typeof getMathchObj != 'undefined') {
                    obj[planName] = getMathchObj.count
                } else {
                    obj[planName] = 0
                }
            }

            data1.push(obj);
        }

        // BAR CHART
        new Morris.Bar({
            element: 'kt_morris_3',
            data: data1,
            xkey: 'y',
            ykeys: planArr,
            labels: planArr
        });
    }


    var demo4 = function () {
        // PIE CHART
        new Morris.Donut({
            element: 'kt_morris_4',
            data: [{
                    label: "Download Sales",
                    value: 12
                },
                {
                    label: "In-Store Sales",
                    value: 30
                },
                {
                    label: "Mail-Order Sales",
                    value: 20
                }
            ]
        });
    }

    return {
        // public functions
        init: function () {
            // demo1();
            // demo2();
            //demo3();
            // demo4();
        }
    };
}();

jQuery(document).ready(function () {
    KTMorrisChartsDemo.init();
});

// Class initialization on page load
jQuery(document).ready(function () {
    var path = window.location.pathname;
    // alert(path)
    // if (path == '/admin/userreport/subscription') {
    //     KTDashboard.init();
    // }
}); */