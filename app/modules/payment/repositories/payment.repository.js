const mongoose = require('mongoose');
const Payment = require('payment/models/payment.model');
const perPage = config.PAGINATION_PERPAGE;

const PaymentRepository = {

    save: async (data) => {
        try {
            let PaymentData = await Payment.create(data);
            if (!PaymentData) {
                return null;
            }
            return PaymentData;
        } catch (e) {
            return e;
        }
    },

    getById: async (id) => {
        let PaymentData = await Payment.findById(id).lean().exec();
        try {
            if (!PaymentData) {
                return null;
            }
            return PaymentData;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let PaymentData = await Payment.findOne(params).lean().exec();
        try {
            if (!PaymentData) {
                return null;
            }
            return PaymentData;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let PaymentData = await Payment.find(params).exec();
        try {
            if (!PaymentData) {
                return null;
            }
            return PaymentData;

        } catch (e) {
            return e;
        }
    },

    getVisitCount: async (params) => {
        try {
            let PaymentCount = await Payment.countDocuments(params);
            if (!PaymentCount) {
                return null;
            }
            return PaymentCount;
        } catch (e) {
            return e;
        }

    },

    delete: async (id) => {
        try {
            let PaymentData = await Payment.findById(id);
            if (PaymentData) {
                let PaymentDelete = await Payment.findByIdAndUpdate(id, {
                    isDeleted: true
                }, {
                    new: true
                }).exec();
                if (!PaymentDelete) {
                    return null;
                }
                return PaymentDelete;
            }
        } catch (e) {
            throw e;
        }
    },


    updateById: async (data, id) => {
        try {
            let PaymentData = await Payment.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!PaymentData) {
                return null;
            }
            return PaymentData;
        } catch (e) {
            return e;
        }
    },

    async getMonthlyRevenue(from, to) {
        try {
            return await Payment.aggregate([{
                    "$lookup": {
                        "from": "users",
                        "localField": "user_id",
                        "foreignField": "_id",
                        "as": "user_details"
                    },
                },
                {
                    $unwind: {
                        path: "$user_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                // {
                //     $project: {
                //         'price': '$price',
                //         'date': {
                //             $dateToString: {
                //                 format: "%Y-%m-%d",
                //                 date: "$createdAt"
                //             }
                //         },
                //     }
                // },
                {
                    $project: {
                        'month': {
                            $dateToString: {
                                format: "%m",
                                date: "$createdAt"
                            }
                        },
                        'year': {
                            $dateToString: {
                                format: "%Y",
                                date: "$createdAt",
                            }
                        },
                        'date': {
                            $dateToString: {
                                format: "%Y-%m-%d",
                                date: "$createdAt"
                            }
                        },
                        'price': '$price',
                        'isDeleted': 1,
                        'status': 1
                    }
                },
                {
                    "$match": {
                        "isDeleted": false,
                        "status": 'success',
                        "date": {
                            $gte: from
                        },
                        "date": {
                            $lte: to
                        }
                    }
                },
                {
                    $group: {
                        '_id': {
                            month: "$month",
                        },
                        'price': {
                            $sum: '$price'
                        }
                    }
                }
            ]).exec();
        } catch (error) {
            return error;
        }
    }

};

module.exports = PaymentRepository;