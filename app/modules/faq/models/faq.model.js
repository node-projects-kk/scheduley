var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var status = ["active", "inactive"];
var deleted = [true, false];

var FAQSchema = new Schema({
    question: {
        type: String,
        default: ''
    },
    answer: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: 'active',
        enum: status
    },
    is_deleted: {
        type: Boolean,
        default: false,
        enum: deleted
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

// For pagination
FAQSchema.plugin(mongooseAggregatePaginate);

module.exports = mongoose.model('faq', FAQSchema);