const planRepo = require('plan/repositories/plan.repository');
const transactionRepo = require('user/repositories/transaction.repository')
const userRepo = require('user/repositories/user.repository');
const stripe = require("stripe")(config.stripe_secret_key);
var moment = require('moment');

/* 
// @Method: list
// @Description: To get all the active plan details from DB
*/
exports.list = async req => {
    try {
        var planResult = await planRepo.getAllByField({"status": "Active"});
        return {
            "status": 200,
            data: planResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        return { "success": false, "status": 500, data: [], "message": error.message }
    }
};

/* 
// @Method: currentSubscription
// @Description: To get all current active subscription
*/
exports.currentSubscription = async req => {
    try {
        var transactionResult = await userRepo.getByIdWithPlan(req.user._id);
        let data = {
            name : transactionResult.firstName + " " + transactionResult.lastName,
            suscription_id : transactionResult.stripe_subscription_id,
            amount : transactionResult.plan.price
        }
        return { "status": 200, data: data, message: "Data Fetched Successfully" };
    } catch (error) {
        return { "success": false, "status": 500, data: [], "message": error.message }
    }
};

/* 
// @Method: upgradeSubscription
// @Description: To get all current active subscription which can we upgradable from current subcription
*/
exports.upgradeSubscription = async req => {
    try {
        var planResult = await userRepo.getByIdWithPlan(req.user._id);
        var transactionResult = await planRepo.upgradablePlanList(planResult.plan.price);
        return {
            "status": 200,
            data: transactionResult,
            message: "Data Fetched Successfully"
        };
    } catch (error) {
        return { "success": false, "status": 500, data: [], "message": error.message }
    }
};

/* 
// @Method: downgradeSubscription
// @Description: To get all current active subscription which can we downgradable from current subcription
*/
exports.downgradeSubscription = async req => {
    try {
        var planResult = await userRepo.getByIdWithPlan(req.user._id);
        var transactionResult = await planRepo.downgradablePlanList(planResult.plan.price);
        return { "status": 200, data: transactionResult, message: "Data Fetched Successfully" };
    } catch (error) {
        return { "success": false, "status": 500, data: [], "message": error.message }
    }
};

/* 
// @Method: updateSubscription
// @Description: To get all current active subscription which can we downgradable from current subcription
*/
exports.updateSubscription = async req => {
    try {
        var userResult = await userRepo.getById(req.user._id);
        var planDetails = await planRepo.getById(req.body.plan);
        
        var days = moment().diff(moment(userResult.createdAt), "days");
        
        if(userResult.paymentStatus == false && days <= 14)
        {
            var subscription = await stripe.subscriptions.create({
                customer: userResult.stripe_customer_id,
                items: [{plan: planDetails.stripe_plan_id}],
                trial_period_days : (14-days),
            });
            if(userResult.stripe_subscription_id!=''){
                var ret_subs = await stripe.subscriptions.retrieve(userResult.stripe_subscription_id);
                if(ret_subs.status!='canceled'){
                    var delSubs = await stripe.subscriptions.del(userResult.stripe_subscription_id);
                }
            }
            
            var updUserData = {
                stripe_subscription_id : subscription.id,
                stripe_plan_id : planDetails.stripe_plan_id,
                plan : req.body.plan
            }
            const updated = await userRepo.updateById(updUserData, userResult._id);
                        
            return { "status": 200, data: [], message: "Subscription updated successfully" };
        }
        else
        {
            if(userResult.stripe_subscription_id!=''){
                const old_subscription = await stripe.subscriptions.retrieve(userResult.stripe_subscription_id);
                if(old_subscription.status!='canceled'){
                    var subscription = await stripe.subscriptions.update(userResult.stripe_subscription_id, {
                        cancel_at_period_end: false,
                        items: [{
                            id: old_subscription.items.data[0].id,
                            plan: planDetails.stripe_plan_id,
                        }]
                    });

                    var updUserData = {
                        stripe_subscription_id : subscription.id,
                        stripe_plan_id : planDetails.stripe_plan_id,
                        plan : req.body.plan
                    }
                    const updated = await userRepo.updateById(updUserData, userResult._id);
                    
                    return { "status": 200, data: updated, message: "Subscription updated successfully" };
                }
                else
                {
                    var subscription = await stripe.subscriptions.create({
                        customer: userResult.stripe_customer_id,
                        items: [{plan: planDetails.stripe_plan_id}]
                    });

                    var updUserData = {
                        stripe_subscription_id : subscription.id,
                        stripe_plan_id : planDetails.stripe_plan_id,
                        plan : req.body.plan
                    }
                    const updated = await userRepo.updateById(updUserData, userResult._id);
                    
                    return { "status": 200, data: updated, message: "Subscription updated successfully" };
                }
            }
            else
            {
                var subscription = await stripe.subscriptions.create({
                    customer: userResult.stripe_customer_id,
                    items: [{plan: planDetails.stripe_plan_id}]
                });

                var updUserData = {
                    stripe_subscription_id : subscription.id,
                    stripe_plan_id : planDetails.stripe_plan_id,
                    plan : req.body.plan
                }
                const updated = await userRepo.updateById(updUserData, userResult._id);
                
                return { "status": 200, data: updated, message: "Subscription updated successfully" };
            }
        }        
        
    } catch (error) {
        console.log(error);
        //console.log("107", error.message);
        return { "success": false, "status": 500, data: [], "message": error.message }
    }
};
