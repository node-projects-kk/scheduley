const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const moment = require('moment');
const axios = require('axios');
const https = require('https');
var FormData = require('form-data');
var form = new FormData();
const userRepo = require('user/repositories/user.repository');
const social_postRepo = require('social_post/repositories/social_post.repository');
var FB = require('fb');
const testData = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});
var schedule = require('node-schedule');
const fs = require('fs');
class facebookController {
  constructor() { }

  /*
  // @Method: FacebookContentShare
  // @Purpose: Share Content in Facebook Page
  */
  async FacebookContentShare(req, res) {
    try {
      let scheduleDateTime = new Date(req.body.publish_time);
      let unix_ts = Math.round(scheduleDateTime.getTime() / 1000);

      let response = await axios.post(
        `https:	//graph.facebook.com/${req.body.page_id}/feed?published=false&message=${req.body.message}&scheduled_publish_time=${unix_ts}&access_token=${req.body.page_token}`, {}, {
          headers: {
            'Content-Type': 'application/json'
          }
        });

      if (response.status == 200) {
        return { status: 200, data: response.data, message: 'Thank You!! You have posted successfully.' }
      }
      else {
        return { status: 201, data: [], message: 'There are some problem, please try again.' }
      }
    }
    catch (e) {
      return { status: 500, data: [], "message": e.message + ', Something went wrong, please try again. ' }
    }
  }


  /*
  // @Method: FacebookSingleImageShare
  // @Purpose: Share single image in Facebook Page
  */
  async FacebookSingleImageShare(req, res) {
    try {

      if (req.files) {
        if (req.files[0].fieldname == 'fb_single_image') {
          req.body.fb_single_image = req.files[0].filename;
        }
        // }
        let fbImage = req.body.fb_single_image;
        let serverUrl = process.env.SERVER_LINK + '/uploads/social_post/facebook/image/' + fbImage;
        req.body.fb_single_image = serverUrl;

        let scheduleDateTime = new Date(req.body.publish_time);
        let unix_ts = Math.round(scheduleDateTime.getTime() / 1000);

        let response = await axios.post(
          'https://graph.facebook.com/' + req.body.page_id + '/photos', {
            url: req.body.fb_single_image,
            message: req.body.message,
            published: false,
            scheduled_publish_time: unix_ts,
            unpublished_content_type: 'SCHEDULED'
          }, {
            headers: {
              'Authorization': 'Bearer ' + req.body.page_token,
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            }
          }
        );

        if (response.status == 200) {
          fs.unlink('public/uploads/social_post/facebook/image/' + fbImage, function (err) {
            if (err) throw new Error(err.message);
          });
          return {
            status: 200,
            data: response.data,
            message: 'Thank You!! Your image posted successfully.'
          }
        } else {
          return {
            status: 201,
            data: [],
            message: 'There are some problem, please try again.'
          }
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'Please upload an image and then try again.'
        }
      }
    } catch (e) {
      return {
        status: 500,
        data: [],
        "message": e.message + ', Something went wrong, please try again. '
      }
    }
  }


  /*
  // @Method: FacebookMultipleImageShare
  // @Purpose: Share multiple image in Facebook Page
  */
  async FacebookMultipleImageShare(req, res) {
    try {
      if (req.files.length > 0) {
        let facebook_multiple_image = [];
        let server_image_names = [];
        for (let file in req.files) {
          let files = req.files[file];
          let serverUrl = process.env.SERVER_LINK + '/uploads/social_post/facebook/image/' + files.filename;
          let response = await axios.post('https://graph.facebook.com/v2.11/' + req.body.page_id + '/photos', {
            caption: 'Sample Image',
            url: serverUrl,
            published: false
          }, {
              headers: {
                'Authorization': 'Bearer ' + req.body.page_token,
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              }
            });
          facebook_multiple_image.push(response.data.id);
          server_image_names.push(files.filename);
        };

        let attachedImages = [];
        if (facebook_multiple_image.length > 1) {
          for (let i = 0; i < facebook_multiple_image.length; i++) {
            attachedImages[i] = {
              "media_fbid": facebook_multiple_image[i]
            }
          }

          let scheduleDateTime = new Date(req.body.publish_time);
          let unix_ts = Math.round(scheduleDateTime.getTime() / 1000);

          let FinalResponse = await axios.post(
            'https://graph.facebook.com/v2.11/' + req.body.page_id + '/feed', {
              message: req.body.message,
              attached_media: attachedImages,
              published: false,
              scheduled_publish_time: unix_ts,
              unpublished_content_type: 'SCHEDULED'
            }, {
              headers: {
                'Authorization': 'Bearer ' + req.body.page_token,
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              }
            }
          );

          if (FinalResponse.status == 200) {
            for (let j = 0; j < server_image_names.length; j++) {
              fs.unlink('public/uploads/social_post/facebook/image/' + server_image_names[j], function (err) {
                if (err) throw new Error(err.message);
              });
            }
            return {
              status: 200,
              data: FinalResponse.data,
              message: 'Thank You!! Multiple images posted successfully.'
            };
          } else {
            return {
              status: 201,
              data: [],
              message: 'There are some problem, please try again.'
            }
          }
        } else {
          return {
            status: 201,
            data: [],
            message: 'We are unable to upload the images on Facebook server, please try again.'
          }
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'There are no images for post, Please add images and try again.'
        }
      }
    } catch (e) {
      return {
        status: 500,
        data: [],
        "message": e.message + ' Something went wrong, please try again. '
      }
    }
  }

  async FacebookPostSchedule(req, res) {
    try {
      let userDetails = await userRepo.getById(req.user._id);

      let unix_ts = moment(req.body.publish_time).local().unix();
      let pageDetails = await FB.api(userDetails.facebook.page_id + '?fields=access_token', 'get', { access_token: userDetails.facebook.access_token });
      userDetails.facebook.page_access_token = pageDetails.access_token;
      let uploadingStep, uploadFinalStep = "", uploadInitailStep, upl_resume;
      req.body.user_id = req.user._id;
      req.body.post_status = "posted";
      if (req.files) {
        let start = async () => {
          await asyncForEach(req.files, async (file) => {
            if (file.mimetype.search('video') != -1) {
              req.body.ctype = "video";
              uploadInitailStep = await FB.api(userDetails.facebook.page_id + '/videos', 'post', { access_token: userDetails.facebook.page_access_token, upload_phase: 'start', file_size: file.size });
              do {
                //fs.strea
                uploadingStep = await FB.api(userDetails.facebook.page_id + '/videos', 'post', { access_token: userDetails.facebook.page_access_token, upload_phase: 'transfer', upload_session_id: uploadInitailStep.upload_session_id, start_offset: 0, video_file_chunk: fs.createReadStream('./public/uploads/social_post/facebook/' + file.filename) });
              }
              while (uploadingStep.start_offset != uploadingStep.end_offset);
              uploadFinalStep = await FB.api(userDetails.facebook.page_id + '/videos', 'post', { access_token: userDetails.facebook.page_access_token, upload_phase: 'finish', upload_session_id: uploadInitailStep.upload_session_id, message: req.body.post_content, published: false, scheduled_publish_time: unix_ts });
              req.body.social_id = uploadInitailStep.video_id;
              req.body.upload_session_id = uploadInitailStep.upload_session_id;
              await social_postRepo.save(req.body);
              upl_resume = await fs.unlinkSync('./public/uploads/social_post/facebook/' + file.filename);

            } else {
              req.body.ctype = "image";
              let url = process.env.SERVER_LINK + '/uploads/social_post/facebook/' + file.filename;
              uploadFinalStep = await FB.api(userDetails.facebook.page_id + '/photos', 'post', { access_token: userDetails.facebook.page_access_token, caption: req.body.post_content, published: false, scheduled_publish_time: unix_ts, url: url });
              req.body.social_id = uploadFinalStep.id;
              await social_postRepo.save(req.body);

              upl_resume = await fs.unlinkSync('./public/uploads/social_post/facebook/' + file.filename);
            }
          });
        }
        await start();
        async function asyncForEach(array, callback) {
          for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
          }
        }
        return {
          status: 200,
          data: uploadFinalStep,
          "message": 'Media schedulled successfully.'
        }
      }
      //console.log("238", pageDetails)

    }
    catch (e) {
      console.log("268", e);
      return {
        status: 500,
        data: e.message,
        "message": ' Something went wrong, please try again. '
      }
    }
  }

}

module.exports = new facebookController();