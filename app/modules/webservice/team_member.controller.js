const userRepo = require('user/repositories/user.repository');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const settingRepo = require('setting/repositories/setting.repository');
const userModel = require('user/models/user.model');
const User = new userModel();
const roleRepo = require('role/repositories/role.repository');
/* 
// @Method: addSocialValue
// @Description: Save social media id/token etc in DB
*/
exports.sendInvitationLink = async req => {
	try {
        const user = await userRepo.getById(req.user._id);
        var ciphertext = cryptr.encrypt(req.user._id);
		const link = config.frontend_url + "/#/auth/register"+"?token="+ciphertext;
        const setting_data = await settingRepo.getAllSetting();
        var settingObj = {};
        if (!_.isEmpty(setting_data)) {
            setting_data.forEach(function (element) {
                settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
            });
        }
        let userlocals = {
            firstName: user.firstName,
            lastName: user.lastName,
            link: link
        };
        let isMailSendForUser = await mailer.sendMail(settingObj.Site_Title + '<support@scheduley-iit.com>', req.body.email, settingObj.Site_Title + ' ' + 'Invitation Link', 'invitation-link', userlocals);
        return {
            status: 200,
            data: [],
            message: 'Invitation Link Sent.'
        };
	}
	catch (error) {
		return {	status: 500,	message: error.message};
	}
};

exports.storeTeamMember = async req => {
	try {
        const user = await userRepo.getByField({"email": req.body.email});
        const roleDetails = await roleRepo.getByField({'role': 'team-member'});
        if(user)
        {
            return {
                status: 200,
                data: [],
                message: 'Email Already Exists.'
            };
        }
        else
        {
           
            if (!_.isEmpty(roleDetails)) {
                req.body.role = roleDetails._id;
            }
        req.body.password = User.generateHash(req.body.password);
        req.body.parent_id = cryptr.decrypt(req.body.token);
        const saveUser = await userRepo.save(req.body);

        const setting_data = await settingRepo.getAllSetting();
		var settingObj = {};
		if (!_.isEmpty(setting_data)) {
			setting_data.forEach(function (element) {
				settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
			});
                }
                
		let userlocals = {
			firstName: saveUser.firstName,
			lastName: saveUser.lastName
		};

		let isMailSendForUser = await mailer.sendMail(settingObj.Site_Title + '<support@scheduley-iit.com>', saveUser.email, settingObj.Site_Title + ' ' + 'Registration successfully', 'new-user', userlocals);
        
        
         return {
            status: 200,
            data: saveUser,
            message: 'Invitation Link Sent.'
        };
    }
	}
	catch (error) {
		return {	status: 500,	message: error.message};
	}
};