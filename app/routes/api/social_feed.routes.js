const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();

const socialFeedController = require('webservice/socialfeed.controller');
namedRouter.all('/social_feeds*', auth.authenticateAPI);

/**
 * @api {post} /social_feeds/addsocialvalue Add Social media id,token
 * @apiVersion 1.0.0
 * @apiGroup Facebook Page
  * @apiHeader x-access-token User's Access token
  * @apiParam social 	social[0][provider]:twitter
					social[0][token]:5151165165151
					social[0][social_id]:456123
					social[1][provider]:facebook
					social[1][token]:fb686868
					social[1][social_id]:cxsVxcsh556567vxax56
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "message": "Data Stored Successfully",
    "data": {
        "user_id": "5d836bb03432c03bff26fa82",
        "status": "Active",
        "isDeleted": false,
        "_id": "5d84a88e6f66026db88adca2",
        "social": [
            {
                "provider": "twitter",
                "token": "5151165165151",
                "social_id": "456123",
                "_id": "5d84a88e6f66026db88adca4"
            },
            {
                "provider": "facebook",
                "token": "fb686868",
                "social_id": "cxsVxcsh556567vxax56",
                "_id": "5d84a88e6f66026db88adca3"
            }
        ],
        "createdAt": "2019-09-20T10:23:10.024Z",
        "updatedAt": "2019-09-20T10:23:10.024Z",
        "__v": 0
    }
}
*/
namedRouter.post("api.social_feed.addsocialvalue", '/social_feeds/addsocialvalue', request_param.any(), async (req, res) => {
	try {
		const success = await socialFeedController.addSocialValue(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

module.exports = router;