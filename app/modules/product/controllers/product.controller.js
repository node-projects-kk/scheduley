const productRepo = require('product/repositories/product.repository');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');

/*
// @Method: edit
// @Description:  render product update page
*/
exports.edit = async req => {
	try {
		const data = await productRepo.getById(req.params.id);
		return {
			"status": 200,
			data,
			"message": "Product Fetched Successfully"
		};
	} catch (error) {
		throw error;
	}
};

/* 
// @Method: update
// @Description: product update action
*/
exports.update = async req => {
	try {
		const olddata = await productRepo.getById(req.body.pro_id);

		let bannerBlockObj = {};
		let middleContentBlockObj = {};
		let sliderBlockObj = {};
		let powerToolBlockObj = {};
		let tryBlockObj = {};
		let sliderImgArr = [];

		bannerBlockObj.title = req.body.title;

		middleContentBlockObj.post_title = req.body.post_title;
		middleContentBlockObj.post_content = req.body.post_content;
		middleContentBlockObj.calendar_title = req.body.calendar_title;
		middleContentBlockObj.calendar_content = req.body.calendar_content;
		middleContentBlockObj.phone_title = req.body.phone_title;
		middleContentBlockObj.phone_content = req.body.phone_content;
		middleContentBlockObj.draftpost_title = req.body.draftpost_title;
		middleContentBlockObj.draftpost_content = req.body.draftpost_content;

		sliderBlockObj.left_title_1 = req.body.left_title_1;
		sliderBlockObj.left_content_1 = req.body.left_content_1;
		sliderBlockObj.left_title_2 = req.body.left_title_2;
		sliderBlockObj.left_content_2 = req.body.left_content_2;
		sliderBlockObj.left_title_3 = req.body.left_title_3;
		sliderBlockObj.left_content_3 = req.body.left_content_3;
		sliderBlockObj.left_title_4 = req.body.left_title_4;
		sliderBlockObj.left_content_4 = req.body.left_content_4;

		powerToolBlockObj.tool_heading = req.body.tool_heading;
		powerToolBlockObj.tool_content = req.body.tool_content;
		powerToolBlockObj.sub_title_1 = req.body.sub_title_1;
		powerToolBlockObj.sub_content_1 = req.body.sub_content_1;
		powerToolBlockObj.sub_title_2 = req.body.sub_title_2;
		powerToolBlockObj.sub_content_2 = req.body.sub_content_2;
		powerToolBlockObj.sub_title_3 = req.body.sub_title_3;
		powerToolBlockObj.sub_content_3 = req.body.sub_content_3;

		tryBlockObj.try_title = req.body.try_title;
		tryBlockObj.try_content = req.body.try_content;
		tryBlockObj.try_button_text = req.body.try_button_text;

		bannerBlockObj.image = olddata.banner_block.image;

		middleContentBlockObj.post_left_image = olddata.middle_content_block.post_left_image;
		middleContentBlockObj.calendar_right_image = olddata.middle_content_block.calendar_right_image;
		middleContentBlockObj.phone_left_image = olddata.middle_content_block.phone_left_image;
		middleContentBlockObj.draftpost_right_image = olddata.middle_content_block.draftpost_right_image;

		powerToolBlockObj.sub_file_1 = olddata.power_tool_block.sub_file_1;
		powerToolBlockObj.sub_file_2 = olddata.power_tool_block.sub_file_2;
		powerToolBlockObj.sub_file_3 = olddata.power_tool_block.sub_file_3;

		sliderBlockObj.slider_files_one = olddata.slider_block.slider_files_one;
		sliderBlockObj.slider_files_two = olddata.slider_block.slider_files_two;
		sliderBlockObj.slider_files_three = olddata.slider_block.slider_files_three;
		sliderBlockObj.slider_files_four = olddata.slider_block.slider_files_four;

		let uploadPath = 'public/uploads/product/';
		let uploadThumbPath = 'public/uploads/product/thumb/';

		if (_.has(req, 'files')) {
			if (req.files.length > 0) {
				req.files.forEach(function (file) {
					if (file.fieldname.toLowerCase() == "banner_image") {
						if (!_.isEmpty(olddata.banner_block)) {
							if (fs.existsSync(uploadPath + olddata.banner_block.image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.banner_block.image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.banner_block.image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.banner_block.image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						bannerBlockObj.image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "post_left_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.post_left_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.post_left_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.post_left_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.post_left_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.post_left_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "calendar_right_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.calendar_right_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.calendar_right_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.calendar_right_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.calendar_right_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.calendar_right_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "phone_left_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.phone_left_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.phone_left_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.phone_left_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.phone_left_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.phone_left_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "draftpost_right_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.draftpost_right_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.draftpost_right_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.draftpost_right_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.draftpost_right_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.draftpost_right_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "sub_file_1") {
						if (!_.isEmpty(olddata.power_tool_block)) {
							if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_1)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_1);
							}
							if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_1)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_1);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						powerToolBlockObj.sub_file_1 = file.filename;
					}

					if (file.fieldname.toLowerCase() == "sub_file_2") {
						if (!_.isEmpty(olddata.power_tool_block)) {
							if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_2)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_2);
							}
							if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_2)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_2);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						powerToolBlockObj.sub_file_2 = file.filename;
					}

					if (file.fieldname.toLowerCase() == "sub_file_3") {
						if (!_.isEmpty(olddata.power_tool_block)) {
							if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_3)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_3);
							}
							if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_3)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_3);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						powerToolBlockObj.sub_file_3 = file.filename;
					}

					if (file.fieldname.toLowerCase() == "slider_files_one") {
						if (!_.isEmpty(olddata.slider_block)) {
							if (fs.existsSync(uploadPath + olddata.slider_block.slider_files_one)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.slider_block.slider_files_one);
							}
							if (fs.existsSync(uploadThumbPath + olddata.slider_block.slider_files_one)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.slider_block.slider_files_one);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						sliderBlockObj.slider_files_one = file.filename;
					}

					if (file.fieldname.toLowerCase() == "slider_files_two") {
						if (!_.isEmpty(olddata.slider_block)) {
							if (fs.existsSync(uploadPath + olddata.slider_block.slider_files_two)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.slider_block.slider_files_two);
							}
							if (fs.existsSync(uploadThumbPath + olddata.slider_block.slider_files_two)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.slider_block.slider_files_two);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						sliderBlockObj.slider_files_two = file.filename;
					}

					if (file.fieldname.toLowerCase() == "slider_files_three") {
						if (!_.isEmpty(olddata.slider_block)) {
							if (fs.existsSync(uploadPath + olddata.slider_block.slider_files_three)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.slider_block.slider_files_three);
							}
							if (fs.existsSync(uploadThumbPath + olddata.slider_block.slider_files_three)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.slider_block.slider_files_three);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						sliderBlockObj.slider_files_three = file.filename;
					}

					if (file.fieldname.toLowerCase() == "slider_files_four") {
						if (!_.isEmpty(olddata.slider_block)) {
							if (fs.existsSync(uploadPath + olddata.slider_block.slider_files_four)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.slider_block.slider_files_four);
							}
							if (fs.existsSync(uploadThumbPath + olddata.slider_block.slider_files_four)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.slider_block.slider_files_four);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						sliderBlockObj.slider_files_four = file.filename;
					}
				});
			}
		}
		req.body.banner_block = bannerBlockObj;
		req.body.power_tool_block = powerToolBlockObj;
		req.body.middle_content_block = middleContentBlockObj;
		req.body.slider_block = sliderBlockObj;
		req.body.try_block = tryBlockObj;

		const result = await productRepo.updateById(req.body, req.body.pro_id);
		return {
			"status": 200,
			data: result,
			"message": "Product Page updated Successfully"
		};
	} catch (error) {
		throw error;
	}
};






/* 
// @Method: list
// @Description: To get all the Product from DB
*/
exports.list = async req => {
	try {
		const currentPage = req.query.page || 1;
		const data = await productRepo.getAll(req.query, {
			'_id': 1
		});
		return {
			"status": 200,
			data,
			"message": "Product List Fetched Successfully"
		};
	} catch (error) {
		throw error;
	}
};

/*
// @Method: statusChange
// @Description: Product status change action
*/
exports.statusChange = async req => {
	try {
		const result = await productRepo.getById(req.body.id);
		const updatedStatus = (_.has(result, 'status') && result.status == "inactive") ? "active" : "inactive";
		const result2 = await cmsRepo.updateById({
			'status': updatedStatus
		}, req.body.id);
		return {
			"status": 200,
			data: result2,
			"message": "Product status has changed successfully"
		}
	} catch (error) {
		throw error;
	}
};

/* 
// @Method: delete
// @Description: Product delete
*/
exports.destroy = async req => {
	try {
		const result = await productRepo.delete(req.params.id);
		return {
			"status": 200,
			data: result,
			"message": "Product Removed Successfully"
		};
	} catch (error) {
		throw error;
	}
};