/*=========================================================================================
    File Name: dashboard1.js
    Description: Dashboard 1
    ----------------------------------------------------------------------------------------
    Item Name: Apex - Responsive Admin Theme
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// chartist chart
// ------------------------------
// $(window).on("load", function () {

//     var path = window.location.pathname;
//     var page = path.split("/").pop();
//     if (page == 'dashboard') {
//         showCharts();
//     }
// });

/*
function showCharts() {
    // var showCharts = function () {
    let activeUserSeries = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let new_company_signup = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let new_user_signup = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let money_investment = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let financial_status_count = [];
    let financial_status = [];


    var ajaxUrl = apiBaseURL + '/dashboard/charts';
    $.ajax({
        type: "GET",
        url: ajaxUrl,
        success: function (res) {
            let labels_heading = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            res.data.active_user_count.map(item => {
                activeUserSeries[Number(item._id.month) - 1] = item.count;
                var data = {
                    labels: labels_heading,
                    series: [activeUserSeries],
                };
                lineArea1.update(data);
            })

            res.data.new_company_signup.map(item => {
                new_company_signup[Number(item._id.month) - 1] = item.count;
                var data = {
                    labels: labels_heading,
                    series: [new_company_signup],
                };
                //update and refresh chart
                lineArea5.update(data);
            })

            res.data.new_user_signup.map(item => {
                new_user_signup[Number(item._id.month) - 1] = item.count;
                var data = {
                    labels: labels_heading,
                    series: [new_user_signup],
                };
                //update and refresh chart
                lineArea4.update(data);
            })

            res.data.money_investment_detail.map(item => {
                money_investment[Number(item._id.month) - 1] = item.count;
                var data = {
                    labels: labels_heading,
                    series: [money_investment],
                };
                //update and refresh chart
                lineArea2.update(data);
                lineArea3.update(data);
            })

            res.data.status_count.map(item => {
                financial_status_count.push(item.count);
                let company_status = item._id.status;
                let updated_status = company_status.replace("_", " ");
                financial_status.push(updated_status);
            })


            // Companies through compliance officers Starts
            var ctx = document.getElementById('myChart');
            var myDoughnutChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: financial_status_count,
                        backgroundColor: [
                            'rgba(255, 0, 0, 1)',
                            'rgba(0,0,255,1)',
                            'rgba(0,128,0,1)'
                        ],
                    }],

                    labels: financial_status
                },
                options: {
                    cutoutPercentage: 50
                }
            });
            // Companies through compliance officers Ends  

        }
    });

    // Active users Chart
    var lineArea1 = new Chartist.Line('#line-area1', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            series: [
                activeUserSeries,
                // [80, 95, 87, 155, 140, 147, 130, 180, 160, 175, 165, 200]
            ]
        }, {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ]);

    lineArea1.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient2',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(0, 201, 255, 1)'
        });

        defs.elem('linearGradient', {
            id: 'gradient3',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0.3,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(132, 60, 247, 1)'
        });
    });
    lineArea1.on('draw', function (data) {
        var circleRadius = 4;
        if (data.type === 'point') {

            var circle = new Chartist.Svg('circle', {
                cx: data.x,
                cy: data.y,
                r: circleRadius,
                class: 'ct-point-circle'
            });
            data.element.replace(circle);
        } else if (data.type === 'label') {
            // adjust label position for rotation
            const dX = data.width / 2 + (30 - data.width)
            data.element.attr({
                x: data.element.attr('x') - dX
            })
        }
    });
    // Active users Chart Ends    


    // Total Revenue Starts
    var lineArea2 = new Chartist.Line('#line-area2', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            series: [
                money_investment,
                // [80, 95, 87, 155, 140, 147, 130, 180, 160, 175, 165, 200]
            ]
        }, {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ]);

    lineArea2.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient2',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(0, 201, 255, 1)'
        });

        defs.elem('linearGradient', {
            id: 'gradient3',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0.3,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(132, 60, 247, 1)'
        });
    });
    lineArea2.on('draw', function (data) {
        var circleRadius = 4;
        if (data.type === 'point') {

            var circle = new Chartist.Svg('circle', {
                cx: data.x,
                cy: data.y,
                r: circleRadius,
                class: 'ct-point-circle'
            });
            data.element.replace(circle);
        } else if (data.type === 'label') {
            // adjust label position for rotation
            const dX = data.width / 2 + (30 - data.width)
            data.element.attr({
                x: data.element.attr('x') - dX
            })
        }
    });
    // Total Revenue Ends    


    // Money Invested Starts
    var lineArea3 = new Chartist.Line('#line-area3', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            series: [
                money_investment,
                // [80, 95, 87, 155, 140, 147, 130, 180, 160, 175, 165, 200]
            ]
        }, {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ]);

    lineArea3.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient2',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(0, 201, 255, 1)'
        });

        defs.elem('linearGradient', {
            id: 'gradient3',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0.3,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(132, 60, 247, 1)'
        });
    });
    lineArea3.on('draw', function (data) {
        var circleRadius = 4;
        if (data.type === 'point') {

            var circle = new Chartist.Svg('circle', {
                cx: data.x,
                cy: data.y,
                r: circleRadius,
                class: 'ct-point-circle'
            });
            data.element.replace(circle);
        } else if (data.type === 'label') {
            // adjust label position for rotation
            const dX = data.width / 2 + (30 - data.width)
            data.element.attr({
                x: data.element.attr('x') - dX
            })
        }
    });
    // Money Invested Ends    


    // New user sign-ups Starts
    var lineArea4 = new Chartist.Line('#line-area4', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            series: [
                new_user_signup,
                // [80, 95, 87, 155, 140, 147, 130, 180, 160, 175, 165, 200]
            ]
        }, {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ]);

    lineArea4.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient2',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(0, 201, 255, 1)'
        });

        defs.elem('linearGradient', {
            id: 'gradient3',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0.3,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(132, 60, 247, 1)'
        });
    });
    lineArea4.on('draw', function (data) {
        var circleRadius = 4;
        if (data.type === 'point') {

            var circle = new Chartist.Svg('circle', {
                cx: data.x,
                cy: data.y,
                r: circleRadius,
                class: 'ct-point-circle'
            });
            data.element.replace(circle);
        } else if (data.type === 'label') {
            // adjust label position for rotation
            const dX = data.width / 2 + (30 - data.width)
            data.element.attr({
                x: data.element.attr('x') - dX
            })
        }
    });
    // New user sign-ups Ends    


    // New company sign ups Starts
    var lineArea5 = new Chartist.Line('#line-area5', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            series: [
                new_company_signup,
                // [80, 95, 87, 155, 140, 147, 130, 180, 160, 175, 165, 200]
            ]
        }, {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ]);

    lineArea5.on('created', function (data) {
        var defs = data.svg.elem('defs');
        defs.elem('linearGradient', {
            id: 'gradient2',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(0, 201, 255, 1)'
        });

        defs.elem('linearGradient', {
            id: 'gradient3',
            x1: 0,
            y1: 1,
            x2: 0,
            y2: 0
        }).elem('stop', {
            offset: 0.3,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(255, 255, 255, 1)'
        }).parent().elem('stop', {
            offset: 1,
            'stop-opacity': '0.2',
            'stop-color': 'rgba(132, 60, 247, 1)'
        });
    });
    lineArea5.on('draw', function (data) {
        var circleRadius = 4;
        if (data.type === 'point') {

            var circle = new Chartist.Svg('circle', {
                cx: data.x,
                cy: data.y,
                r: circleRadius,
                class: 'ct-point-circle'
            });
            data.element.replace(circle);
        } else if (data.type === 'label') {
            // adjust label position for rotation
            const dX = data.width / 2 + (30 - data.width)
            data.element.attr({
                x: data.element.attr('x') - dX
            })
        }
    });
    // New company sign ups Ends    

    // Bar Chart Starts
    if ($('#viewSalesChart').length > 0) {
        $('#viewSalesChart').click(function () {
            var salesStartDate = $('#salesStartDate').val();
            var salesEndDate = $('#salesEndDate').val();
            var ajaxUrl = apiBaseURL + '/sales_report/chartdata';
            $.ajax({
                type: "POST",
                url: ajaxUrl,
                data: {
                    'startDate': salesStartDate,
                    'endDate': salesEndDate
                },
                success: function (data) {
                    var dataLabels = [];
                    var dataSeries = [];
                    $.each(data, function (index, value) {
                        dataLabels.push(value.day);
                        dataSeries.push(value.total);
                        //alert( value.day + ": " + value.total );
                    });
                    drawBarChart(dataLabels, dataSeries);
                }
            });
        });
        $('#viewSalesChart').trigger('click');
    }

    if ($('#bar-chart-revenue').length > 0) {
        renderRevenueChart();
    }

    if ($('#bar-chart-call').length > 0) {
        renderCallChart();
    }

    function renderRevenueChart() {
        var revenueStartDate = $('#revenueStartDate').val();
        var revenueEndDate = $('#revenueEndDate').val();
        var ajaxUrl = apiBaseURL + '/revenue_report/chartdata';
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: {
                'startDate': revenueStartDate,
                'endDate': revenueEndDate
            },
            success: function (data) {
                var dataLabels = [];
                var dataSeries = [];
                $.each(data, function (index, value) {
                    dataLabels.push(value.day);
                    dataSeries.push(value.total);
                    //alert( value.day + ": " + value.total );
                });
                drawBarChartRevenue(dataLabels, dataSeries);
            }
        });
    }

    function renderCallChart() {
        var ajaxUrl = apiBaseURL + '/call_report/chartdata';
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: {
                'startDate': '',
                'endDate': ''
            },
            success: function (data) {
                var dataLabels = [];
                var dataSeries = [];
                $.each(data, function (index, value) {
                    dataLabels.push(value.day);
                    dataSeries.push(value.total);
                    //alert( value.day + ": " + value.total );
                });
                drawBarChartCall(dataLabels, dataSeries);
            }
        });
    }

    function drawBarChart(dataLabels, dataSeries) {
        var barChart = new Chartist.Bar('#bar-chart', {
                labels: dataLabels,
                series: [dataSeries]

            }, {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0
                },
                low: 0,
                high: 60
            },
            [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ]);

        barChart.on('created', function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient4',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(238, 9, 121,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 106, 0, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient5',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(0, 75, 145,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(120, 204, 55, 1)'
            });

            defs.elem('linearGradient', {
                id: 'gradient6',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(132, 60, 247,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(56, 184, 242, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient7',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(155, 60, 183,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 57, 111, 1)'
            });
        });
        barChart.on('draw', function (data) {
            var barHorizontalCenter, barVerticalCenter, label, value;
            if (data.type === 'bar') {

                data.element.attr({
                    y1: 195,
                    x1: data.x1 + 0.001
                });

            }
        });
    }

    function drawBarChartRevenue(dataLabels, dataSeries) {
        var barChart = new Chartist.Bar('#bar-chart-revenue', {
                labels: dataLabels,
                series: [dataSeries]

            }, {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0
                },
                low: 0,
                high: 60
            },
            [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ]);

        barChart.on('created', function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient4',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(238, 9, 121,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 106, 0, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient5',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(0, 75, 145,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(120, 204, 55, 1)'
            });

            defs.elem('linearGradient', {
                id: 'gradient6',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(132, 60, 247,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(56, 184, 242, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient7',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(155, 60, 183,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 57, 111, 1)'
            });
        });
        barChart.on('draw', function (data) {
            var barHorizontalCenter, barVerticalCenter, label, value;
            if (data.type === 'bar') {

                data.element.attr({
                    y1: 195,
                    x1: data.x1 + 0.001
                });

            }
        });
    }

    function drawBarChartCall(dataLabels, dataSeries) {
        var barChart = new Chartist.Bar('#bar-chart-call', {
                labels: dataLabels,
                series: [dataSeries]

            }, {
                axisX: {
                    showGrid: false,
                },
                axisY: {
                    showGrid: false,
                    showLabel: false,
                    offset: 0
                },
                low: 0,
                high: 60
            },
            [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
            ]);

        barChart.on('created', function (data) {
            var defs = data.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient4',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(238, 9, 121,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 106, 0, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient5',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(0, 75, 145,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(120, 204, 55, 1)'
            });

            defs.elem('linearGradient', {
                id: 'gradient6',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(132, 60, 247,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(56, 184, 242, 1)'
            });
            defs.elem('linearGradient', {
                id: 'gradient7',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(155, 60, 183,1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(255, 57, 111, 1)'
            });
        });
        barChart.on('draw', function (data) {
            var barHorizontalCenter, barVerticalCenter, label, value;
            if (data.type === 'bar') {

                data.element.attr({
                    y1: 195,
                    x1: data.x1 + 0.001
                });

            }
        });
    }
}
*/