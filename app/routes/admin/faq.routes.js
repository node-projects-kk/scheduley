var express = require('express');
var routeLabel = require('route-label');
var router = express.Router();
var namedRouter = routeLabel(router);
var querystring = require('querystring');


var multer = require('multer');
var faqController = require('faq/controllers/faq.controller');

// var Storage = multer.diskStorage({
// 	destination: function (req, file, callback) {
// 		callback(null, "./public/uploads/customer");
// 	}
// 	, filename: function (req, file, callback) {
// 		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
// 	}
// });

// var uploadFile = multer({ storage: Storage });
var request_param = multer();


namedRouter.all('/faq*', auth.authenticate);

/*
// @Route: FAQ List
*/
namedRouter.get("faq.list", '/faq/list', request_param.any(), async (req, res) => {
  try {
    const success = await faqController.list(req);
    let searchStr = {
      'keyword': '',
      'role': ''
    };
    success.search_param = "";
    if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
      searchStr = {
        'keyword': req.query.keyword,
        'role': req.query.role
      };
      success.search_param = "&" + querystring.stringify(searchStr);
    }
    // For search string
    return res.render('faq/views/list.ejs', {
      page_name: 'faq-management',
      page_title: 'FAQ List',
      user: req.user,
      setting: req.session.setting,
      postdata: searchStr,
      response: success
    });
  } catch (error) {
    // console.log('error', error);
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  }
});


/*
// @Route: Render Create Faq
*/
namedRouter.get("faq.create", '/faq/create', async (req, res) => {
  try {
    const success = await faqController.create(req);
    return res.render('faq/views/add.ejs', {
      page_name: 'faq-management',
      page_title: 'Create FAQ',
      user: req.user,
      setting: req.session.setting,
      response: success
    });
  } catch (error) {
    return res.redirect(namedRouter.urlFor('faq.list'));
  }
});

/*
// @Route: Store Faq Action
*/
namedRouter.post("faq.store", '/faq/store', request_param.any(), async (req, res) => {
  try {
    const success = await faqController.store(req);
    req.flash('success', success.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('faq.create'));
  }
});

/*
// @Route: Render Edit Faq
*/
namedRouter.get("faq.edit", '/faq/edit/:id', async (req, res) => {
  try {
    const success = await faqController.edit(req);
    return res.render('faq/views/edit.ejs', {
      page_name: 'faq-management',
      page_title: 'Update FAQ',
      setting: req.session.setting,
      user: req.user,
      response: success
    });
  } catch (error) {
    return res.status(error.status).send(error);
  }
});

/*
// @Route: Update Faq Action
*/
namedRouter.post("faq.update", '/faq/update', request_param.any(), async (req, res) => {
  const FAQId = req.body.id;
  try {
    const success = await faqController.update(req);
    req.flash('success', success.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('faq.edit', {
      id: FAQId
    }));
  }
});


/*
// @Route: Faq status change
*/
namedRouter.post('faq.status.change', '/faq/status-change', request_param.any(), async (req, res) => {
  try {
    const success = await faqController.statusChange(req);
    req.flash('success', success.message);
    return res.send(success);
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  }
});

/*
// @Route: Delete Faq
*/
namedRouter.get('faq.delete', '/faq/delete/:id', request_param.any(), async (req, res) => {
  try {
    const success = await faqController.delete(req);
    req.flash('success', success.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('faq.list'));
  }
});

// Export the express.Router() instance
module.exports = router;