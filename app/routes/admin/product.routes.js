const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const productController = require('product/controllers/product.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/product/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({
	storage: Storage
});

const request_param = multer();

namedRouter.all('/product*', auth.authenticate);

/*
// @Route: Product List
*/
namedRouter.get("product.list", '/product/list', request_param.any(), async (req, res) => {
	try {
		const success = await productController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('product/views/list.ejs', {
			page_name: 'product-management',
			page_title: 'Product List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	}
});

/*
// @Route: Render Create Product
*/
namedRouter.get("product.create", '/product/create', async (req, res) => {
	try {
		const success = await productController.create(req);
		return res.render('product/views/add.ejs', {
			page_name: 'product-management',
			page_title: 'Create Product',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('product.list'));
	}
});

/*
// @Route: Create Product Action
*/
namedRouter.post("product.store", '/product/store', request_param.any(), async (req, res) => {
	try {
		const success = await productController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('product.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: Render Edit Product
*/
namedRouter.get("product.edit", '/product/edit/:id', async (req, res) => {
	try {
		const success = await productController.edit(req);
		return res.render('product/views/edit.ejs', {
			page_name: 'product-management',
			page_title: 'Update Product',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update Product Action
*/
namedRouter.post("product.update", '/product/update', uploadFile.any(), async (req, res) => {
	const productId = req.body.pro_id;
	try {
		const success = await productController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	} catch (error) {
		// console.log('error', error);
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('product.edit', {
			id: productId
		}));
	}
});

/*
// @Route: Product status change
*/
namedRouter.post('product.status.change', '/product/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await productController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	}
});

/*
// @Route: Delete Product
*/
namedRouter.get('product.delete', '/product/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await productController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('product.list'));
	}
});

// Export the express.Router() instance
module.exports = router;