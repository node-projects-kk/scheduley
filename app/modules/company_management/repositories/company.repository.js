const CompanyManagement = require('company_management/models/company.model');
const mongoose = require('mongoose');
const perPage = config.PAGINATION_PERPAGE;

class CompanyManagementRepository {
	constructor() {}

	async getAll(searchQuery, sortOrder = {
		'_id': -1
	}) {
		try {
			const query = [{
				"isDeleted": false
			}];
			// serach by keyword
			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
								'company_title': {
									'$regex': search,
									'$options': 'i'
								}
							},
							{
								'company_email': {
									'$regex': search,
									'$options': 'i'
								}
							},
							{
								'company_phone': {
									'$regex': search,
									'$options': 'i'
								}
							}
						]
					});
				}
			}
			searchQuery = {
				"$and": query
			};
			return await CompanyManagement.aggregate([{
					"$sort": sortOrder
				},
				{
					$match: searchQuery
				},
				{
					$project: {
						_id: "$_id",
						company_title: '$company_title',
						company_address: '$company_address',
						company_website: '$company_website',
						company_email: '$company_email',
						company_phone: '$company_phone',
						status: '$status'
					}
				}
			]).exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await CompanyManagement.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await CompanyManagement.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			return await CompanyManagement.find(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			// await CompanyManagement.findById(id).lean().exec();
			return await CompanyManagement.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await CompanyManagement.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).exec();
		} catch (error) {
			return error;
		}
	}

	async getCmsCount() {
		try {
			return await CompanyManagement.count({
				'status': "active"
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new CompanyManagement(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}

	async getAllDashboard() {
		try {
			const query = [{
				"isDeleted": false
			}];
			let searchQuery = {
				"$and": query
			};
			return await CompanyManagement.aggregate([{
					"$lookup": {
						"from": "users",
						"localField": "user_id",
						"foreignField": "_id",
						"as": "user"
					},
				},
				{
					$unwind: "$user"
				},
				{
					$match: searchQuery
				},
				{
					$project: {
						_id: "$_id",
						company_title: '$company_title',
						firstName: '$user.firstName',
						lastName: '$user.lastName',
						image: '$user.profile_image',
						email: '$user.email',
						phone: '$user.contactNumber',
					}
				}
			]).sort({
				_id: -1
			}).exec();
		} catch (error) {
			return error;
		}
	}

	async ViewCompanyPostInfo(id) {
		try {
			return await CompanyManagement.aggregate([{
					"$match": {
						'_id': mongoose.Types.ObjectId(id)
					}
				},
				{
					$unwind: '$social_account_info'
				},
				{
					"$lookup": {
						"from": "socialposts",
						"localField": "social_account_info.social_id",
						"foreignField": "social_id",
						"as": "social_post_info"
					},
				},
				{
					$unwind: '$social_post_info'
				},
				{
					$group: {
						'_id': {
							'social_id': "$social_account_info.social_id",
							'social_type': "$social_account_info.social_type",
							'username': "$social_account_info.username"
						},
						"social_post_details": {
							$addToSet: {
								// 'social_id': "$social_account_info.social_id",
								// 'social_type': "$social_account_info.social_type",
								// 'username': "$social_account_info.username",
								'social_post_info': '$social_post_info'
							}
						},
					}
				}
			]).exec();
		} catch (error) {
			return error;
		}
	}

}

module.exports = new CompanyManagementRepository();