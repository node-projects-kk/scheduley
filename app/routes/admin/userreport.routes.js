const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');


const multer = require('multer');
const userReportController = require('user_report/controllers/userreport.controller');

const request_param = multer();

namedRouter.all('/userreport*', auth.authenticate);

/*
// @Route: Render User Report Page
*/
namedRouter.get("userreport.list", '/userreport/list/:from?/:to?', request_param.any(), async (req, res) => {
  try {
    return res.render('user_report/views/user.ejs', {
      page_name: 'user-report-management',
      page_title: 'User Report',
      user: req.user,
      from: req.params.from,
      to: req.params.to,
      setting: req.session.setting,
    });
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('userreport.list'));
  }
});


/*
// @Route: Render Subscription Report page
*/
namedRouter.get("userreport.subscription", '/userreport/subscription/:from?/:to?', request_param.any(), async (req, res) => {
  try {
    return res.render('user_report/views/subscription.ejs', {
      page_name: 'subscription-report-management',
      page_title: 'Subscription Report',
      user: req.user,
      from: req.params.from,
      to: req.params.to,
      setting: req.session.setting,
    });
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('userreport.subscription'));
  }
});


/*
// @Route: Render User Promo Report page
*/
namedRouter.get("userreport.promo", '/userreport/promo/:from?/:to?', request_param.any(), async (req, res) => {
  try {

    return res.render('user_report/views/user-promo.ejs', {
      page_name: 'user-promocode-management',
      page_title: 'PromoCode Report',
      user: req.user,
      setting: req.session.setting,
      from: req.params.from,
      to: req.params.to
    });
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('userreport.promo'));
  }
});


/*
// @Route: Render User Promo Report page
*/
namedRouter.get("userreport.trial", '/userreport/trial/:from?/:to?', request_param.any(), async (req, res) => {
  try {

    return res.render('user_report/views/user-type.ejs', {
      page_name: 'user-type-management',
      page_title: 'User Type Report',
      user: req.user,
      setting: req.session.setting,
      from: req.params.from,
      to: req.params.to
    });
  } catch (error) {
    req.flash('error', error.message);
    return res.redirect(namedRouter.urlFor('userreport.trial'));
  }
});



/*
// @Route: Render User Report Charts for ajax calling
*/
namedRouter.get('userreport.charts', '/userreport/charts/:from?/:to?/:status?', request_param.any(), async (req, res) => {
  try {
    const success = await userReportController.userChart(req);
    const response = success;
    req.flash('success', success.message);
    return res.send(success);
  } catch (error) {
    req.flash('error', error.message);
    // return res.redirect(namedRouter.urlFor('company.user.list'));
  }
});

/*
// @Route: Render User Subscription Charts for ajax calling
*/
namedRouter.get('userreport.subscriptionChart', '/userreport/subscriptionChart/:subscriptionYear?', request_param.any(), async (req, res) => {
  try {
    const success = await userReportController.subscriptionChart(req);
    const response = success;
    req.flash('success', success.message);
    return res.send(success);
  } catch (error) {
    req.flash('error', error.message);
    // return res.redirect(namedRouter.urlFor('company.user.list'));
  }
});



// Export the express.Router() instance
module.exports = router;