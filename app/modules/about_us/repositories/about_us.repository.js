const about_us = require('about_us/models/about_us.model');
const perPage = config.PAGINATION_PERPAGE;

class about_usRepository {
    constructor() {}

    async getAll(searchQuery, sortOrder = {
        '_id': -1
    }) {
        try {
            const query = [{
                "status": "active"
            }];
            // serach by keyword
            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    const search = searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                            'title': {
                                '$regex': search,
                                '$options': 'i'
                            }
                        }]
                    });
                }
            }
            searchQuery = {
                "$and": query
            };
            return await about_us.aggregate([{
                    "$sort": sortOrder
                },
                //{
                //    $match: searchQuery
                //},
                //{
                //    $project: {
                //        _id: "$_id",
                //        banner_title: "$banner_block.banner_title",
                //        desc: "$desc",
                //        status: "$status",
                //        slug: "$slug"
                //    }
                //}
            ]);
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await about_us.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await about_us.findOne(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await about_us.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await about_us.findById(id).lean().exec();
            return await about_us.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await about_us.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            return await about_us.create(data).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllAbout() {
        try {
            return await about_us.find().lean().exec();
        } catch (error) {
            return error;
        }
    }

}

module.exports = new about_usRepository();