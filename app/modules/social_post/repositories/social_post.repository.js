const social_post = require('social_post/models/social_post.model');
const perPage = config.PAGINATION_PERPAGE;

class social_postRepository {
	constructor() {}

	async getById(id) {
		try {
			return await social_post.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await social_post.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			return await social_post.find(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			return await social_post.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await social_post.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			});
		} catch (error) {
			return error;
		}
    }
    
	async save(data) {
		try {
			return await social_post.create(data);
		} catch (error) {
			return error;
		}
	}
}

module.exports = new social_postRepository();