const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
 const multer = require('multer');
 const request_param = multer();

const planController = require('plan/controllers/plan.api.controller');

/**
 * @api {get} /plan Plan List
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d88728f9a0bc348890b908e",
            "stripe_plan_id": "plan_FrUrUFM12CziKC",
            "title": "Silver",
            "slug": "silver-plus",
            "price": 19,
            "content": "<ul class=\"plan-card__features__list\"><li class=\"plan-card__features__list__item\">Automated post scheduling</li><li class=\"plan-card__features__list__item\">Key performance metrics</li><li class=\"plan-card__features__list__item\">Ad spend limit of <span class=\"_boostSpend\">19</span> per month to boost posts        </li></ul>",
            "image": "image_1569223310822_f1.jpg",
            "status": "Active",
            "inactiveDate": null,
            "isDeleted": false,
            "createdAt": "2019-09-23T07:21:51.761Z",
            "updatedAt": "2019-09-23T07:28:19.449Z"
        },
        {
            "_id": "5d8873349a0bc348890b908f",
            "stripe_plan_id": "plan_FrUunNhrIqxRmb",
            "title": "Gold",
            "slug": "gold-plus",
            "price": 49,
            "content": "<ul class=\"plan-card__features__list\"><li class=\"plan-card__features__list__item plan-card__features__list__item--included-features\">Everything included in Professional plan</li><li class=\"plan-card__features__list__item\">Team message assignments</li><li class=\"plan-card__features__list__item\">Custom analytics</li><li class=\"plan-card__features__list__item\">Exportable reports</li><li class=\"plan-card__features__list__item\">Ad spend limit of 49 per month to boost posts            </li></ul>",
            "image": "image_1569223475403_refresh_update.png",
            "status": "Active",
            "inactiveDate": null,
            "isDeleted": false,
            "createdAt": "2019-09-23T07:24:36.387Z",
            "updatedAt": "2019-09-23T07:28:28.247Z"
        },
        {
            "_id": "5d8873999a0bc348890b9090",
            "stripe_plan_id": "plan_FrUvO0Pj2knh2A",
            "title": "Platinum",
            "slug": "platinum-plus",
            "price": 99,
            "content": "<ul class=\"plan-card__features__list\"><li class=\"plan-card__features__list__item plan-card__features__list__item--included-features\">Everything included in Team plan</li><li class=\"plan-card__features__list__item\">Flexible approval workflows</li><li class=\"plan-card__features__list__item\">1 hour of 1-on-1 onboarding</li><li class=\"plan-card__features__list__item\">5 social media certifications</li><li class=\"plan-card__features__list__item\">24/7 support</li><li class=\"plan-card__features__list__item\">Ad spend limit of 99 per month to boost posts</li></ul>",
            "image": "image_1569223577086_test3.jpg",
            "status": "Active",
            "inactiveDate": null,
            "isDeleted": false,
            "createdAt": "2019-09-23T07:26:17.844Z",
            "updatedAt": "2019-09-23T07:28:36.970Z"
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.plan", '/plan', function (req, res) {
    planController.list(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

namedRouter.all('/plan*', auth.authenticateAPI);

/**
 * @api {get} /plan/currentSubscription Current Subscription
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "name": "Archit Singh",
        "suscription_id": "sub_Fq060yU9FOb2Bn",
        "amount": 19
    },
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.plan.currentSubscription", '/plan/currentSubscription', function (req, res) {
    planController.currentSubscription(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {get} /plan/upgradeSubscription Upgrade Subscription List
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cd2ac2f43798264eaaf87ac",
            "title": "Gold",
            "slug": "gold",
            "price": 49,
            "content": "<div class=\"plan-card__features\"><ul class=\"plan-card__features__list\"><li class=\"plan-card__features__list__item plan-card__features__list__item--included-features\">Everything included in Professional plan\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Team message assignments\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Custom analytics\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Exportable reports\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Ad spend limit of 49&nbsp;per month to boost posts&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li></ul></div>",
            "image": "image_1558104681678_dummy.png",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-05-08T10:15:11.508Z",
            "updatedAt": "2019-09-17T11:11:54.112Z",
            "__v": 0
        },
        {
            "_id": "5cd2b7f37c1b52730dc308af",
            "title": "Platinum",
            "slug": "platinum",
            "price": 99,
            "content": "<div class=\"plan-card__features\"><ul class=\"plan-card__features__list\"><li class=\"plan-card__features__list__item plan-card__features__list__item--included-features\">Everything included in Team plan\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Flexible approval workflows\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    1 hour of 1-on-1 onboarding\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    5 social media certifications\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    24/7 support\r\n                                                </li><li class=\"plan-card__features__list__item\">\r\n                                                    Ad spend limit of 99&nbsp;per month to boost posts&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li></ul></div>",
            "image": "image_1557313523218_f4.jpg",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-05-08T11:05:23.240Z",
            "updatedAt": "2019-09-17T11:12:56.868Z",
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.plan.upgradeSubscription", '/plan/upgradeSubscription', function (req, res) {
    planController.upgradeSubscription(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {get} /plan/downgradeSubscription Downgrade Subscription List
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.plan.downgradeSubscription", '/plan/downgradeSubscription', function (req, res) {
    planController.downgradeSubscription(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {post} /plan/updateSubscription Update Subscription
 * @apiVersion 1.0.0
 * @apiGroup Plan
 * @apiHeader x-access-token User's Access token
 * @apiParam plan Plan ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.post("api.plan.updateSubscription", '/plan/updateSubscription', request_param.any(), function (req, res) {
    planController.updateSubscription(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});



module.exports = router;