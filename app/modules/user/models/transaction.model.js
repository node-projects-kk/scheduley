var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["Active", "Inactive"];

var TransactionSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    plan_title : {
        type: String,
        default: "",
    },
    plan_slug : {
        type: String,
        default: "",
    },
    plan_price : {
        type: String,
        default: "",
    },
    plan_content : {
        type: String,
        default: "",
    },
    promo_code: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PromoCode'
    },
    payment_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment'
    },
    status: {
        type: String,
        default: "Active",
        enum: status
    },
}, {
        timestamps: true
    });


// For pagination
TransactionSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Transaction', TransactionSchema);