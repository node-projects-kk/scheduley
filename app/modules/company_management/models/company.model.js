const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["active", "inactive"];
const deletestatus = [false, true];


const CompanySchema = new Schema({
	company_title: {
		type: String,
		default: ''
	},
	social_account_info: [{
		social_id: {
			type: String,
			default: ''
		},
		social_type: {
			type: String,
			default: ''
		},
		username: {
			type: String,
			default: ''
		},
		access_token: {
			type: String,
			default: ''
		},
		expiry_time: {
			type: String,
			default: ''
		},
	}],
	user_id: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		default: null
	},
	status: {
		type: String,
		default: "active",
		enum: status
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deletestatus
	},
	createdAt: {
		type: Date,
		default: Date.now()
	}
});

// For pagination
CompanySchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('CompanyMangement', CompanySchema);