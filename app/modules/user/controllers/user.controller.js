const mongoose = require('mongoose');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const userModel = require('user/models/user.model');
const moment = require('moment');
const User = new userModel();
const visitRepo = require('visit/repositories/visit.repository');
const PaymentRepo = require('payment/repositories/payment.repository');

var gm = require('gm').subClass({
    imageMagick: true
});
var fs = require('fs');
const jwt = require('jsonwebtoken');

/* @Method: login
// @Description: user Login Render
*/
exports.login = (req, res) => res.render('user/views/login.ejs');


/* @Method: loginPage
// @Description: user Login Page
*/
exports.loginPage = async (req, res) => {
    try {
        const setting_data = await settingRepo.getAllSetting();

        var setting = {};
        if (!_.isEmpty(setting_data)) {

            setting_data.forEach(function (element) {
                setting[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
            });

            req.session.setting = setting;
        }

        //res.render('user/views/login.ejs');
        return res.render('user/views/login.ejs', {
            setting: req.session.setting
        })

    } catch (error) {
        console.log('erererererer', error);
        throw error;
    }
};
/* @Method: signin
// @Description: user Login
*/
exports.signin = async req => {
    try {
        // find the user
        const user = await userRepo.fineOneWithRole(req.body);
        if (!_.isEmpty(user.role)) {
            const payload = {
                id: user._id
            };
            const token = jwt.sign(payload, config.jwtSecret, {
                expiresIn: 86400 // expires in 24 hours
            });
            req.session.token = token;
            req.user = user;
            // return the information including token as JSON
            return {
                "status": 200,
                token: token,
                data: [user],
                "message": "You have successfully logged in"
            };
        } else {
            throw new Error('Authentication failed. Wrong credentials.')
        }
    } catch (error) {
        console.log("77", error);
        throw error;
    }
};

/* @Method: Create
// @Description: user create Render
*/
exports.create = async req => {
    try {
        const roles = await roleRepo.getAllByField({
            'roleType': {
                $nin: ["frontend"]
            },
            'role': {
                $nin: ["admin"]
            }
        });
        return {
            "status": 200,
            data: roles,
            "message": req.flash('info')
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: store
// @Description: user create action
*/
exports.store = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false
        });
        if (_.isEmpty(result)) {
            /*const role = await roleRepo.getByField({
                "role": req.body.role
            });
            req.body.role = role._id;*/
            const userPassword = req.body.password;
            req.body.password = req.user.generateHash(req.body.password);

            const result = await userRepo.save(req.body);

            const setting_data = await settingRepo.getAllSetting();
            var settingObj = {};
            if (!_.isEmpty(setting_data)) {
                setting_data.forEach(function (element) {
                    settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                });
                //req.session.setting = settingObj;
            }

            var mailOptions = {
                from: settingObj.Site_Title + '<support@beashark.com>',
                to: result.email,
                subject: "Welcome to " + settingObj.Site_Title,
                html: 'Hello ' + result.firstName + ' ' + result.lastName + ',<br><br>Your profile has been created. Please login with this details below.<br>Email : ' + result.email + '<br>Password : ' + userPassword + '<br><br>Thank you.'
            };

            await transporter.sendMail(mailOptions);

            return {
                "status": 200,
                data: result,
                "message": "User Created Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: edit
// @Description:  render user update page
*/
exports.edit = async req => {
    try {
        const result = await userRepo.getById(req.params.id);


        var routeNmPrefx = '';
        var routeLnkPrefx = '';
        if (req.roleType == 'frontend' && req.roleName == 'users') {
            routeNmPrefx = 'frontend.';
            routeLnkPrefx = 'frontend-';
        } else if (req.roleType == 'frontend' && req.roleName == 'companies') {
            routeNmPrefx = 'company.';
            routeLnkPrefx = 'company-';
        }
        return {
            "status": 200,
            data: result,
            routeNmPrefx: routeNmPrefx,
            routeLnkPrefx: routeLnkPrefx,
            "message": "User Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: update
// @Description: user update action
*/
exports.update = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false,
            '_id': {
                $ne: req.body.user_id
            }
        });
        if (_.isEmpty(result)) {
            await userRepo.getUser(req.body.user_id);
            const result2 = await userRepo.updateById(req.body, req.body.user_id);
            return {
                "status": 200,
                data: result2,
                "message": "User Updated Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/* @Method: updateCompany
// @Description: user update action
*/
exports.updateCompany = async req => {
    try {
        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false,
            '_id': {
                $ne: req.body.user_id
            }
        });
        if (_.isEmpty(result)) {
            const data = await userRepo.getCompanyById(req.body.company_id);

            if (req.files.length > 0) {

                // if (!_.isEmpty(data)) {
                //     fs.unlink('public/uploads/company/' + data.company_details[0].logo, function (err) {
                //         if (err) throw new Error(err.message);
                //     });

                //     fs.unlink('public/uploads/company/thumb/' + data.company_details[0].logo, function (err) {
                //         if (err) throw new Error(err.message);
                //     });
                // }

                gm('public/uploads/company/' + req.files[0].filename)
                    .resize(100)
                    .write('public/uploads/company/thumb/' + req.files[0].filename, function (err) {
                        if (err)
                            throw new Error(err.message);
                    });

                req.body.logo = req.files[0].filename;
            } else {
                req.body.logo = data.company_details[0].logo;
            }

            const result2 = await userRepo.updateById(req.body, req.body.user_id);

            var companyDetObj = {
                'name': req.body.company_name,
                'industry': req.body.industry_type,
                'logo': req.body.logo
            }
            var companyObj = {
                'company_details': []
            }
            companyObj.company_details.push(companyDetObj);

            const resultCompany = await userRepo.updateCompanyById(companyObj, req.body.company_id);

            return {
                "status": 200,
                data: result2,
                "message": "Company Updated Successfully"
            };
        } else {
            throw new Error("This email address is already exist!");
        }
    } catch (error) {
        throw error;
    }
};

/* @Method: resetPassword
// @Description: user update action
*/
exports.resetPassword = async req => {
    try {

        const data = await userRepo.getById(req.params.id);

        const newPassword = Math.random().toString(36).substring(7);
        const new_password = req.user.generateHash(newPassword);

        const setting_data = await settingRepo.getAllSetting();
        var settingObj = {};

        if (!_.isEmpty(setting_data)) {
            setting_data.forEach(function (element) {
                settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
            });
        }

        var mailOptions = {
            from: settingObj.Site_Title + '<support@beashark.com>',
            to: data.email,
            subject: "Reset Password",
            html: `Your password has been updated.<br>New password: <b>` + newPassword + `</b><br>Thank you.`
        };

        await transporter.sendMail(mailOptions);

        await userRepo.updateById({
            "password": new_password
        }, req.params.id);
        return {
            "status": 200,
            data: [],
            "message": "Password has been changed successfully"
        }

    } catch (error) {
        throw error;
    }
};

/* @Method: list
// @Description: To get all the users from DB
*/
exports.list = async req => {
    try {
        const data = await userRepo.getAllUsersByRolename(req.query, req.user, {
            '_id': -1
        });
        return {
            "status": 200,
            data: data,
            "message": "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: companyListing
// @Description: To get all the company from DB
*/
exports.companyListing = async req => {
    try {
        const data = await userRepo.getAllCompanybyUserId(req.params.id);
        return {
            "status": 200,
            data: data,
            "message": "Data Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: Dashboard
// @Description: User Dashboard
*/
exports.dashboard = async req => {
    try {
        var today = moment().format('YYYY-MM-DD')
        var from = moment().format('YYYY-MM-DD');
        var to = moment().format('YYYY-MM-DD');
        let condition = {};
        if (req.params.from) {
            from = req.params.from;
            to = req.params.to;
            condition = {
                $and: [{
                        "date": {
                            $gte: from
                        }
                    },
                    {
                        "date": {
                            $lte: to
                        }
                    }
                ]
            }
        } else {
            condition = {
                "date": {
                    $lte: to
                }
            }
        }
        const responseObj = {};
        const totalSignupUser = await userRepo.getTotalSignUpCount(condition);
        responseObj.totalSignupUser = totalSignupUser;

        const activeUserCount = await userRepo.getActiveUserCount(condition);
        responseObj.activeUserCount = activeUserCount[0].count;

        const salesToday = await userRepo.getSalesToday(today);
        responseObj.salesToday = 0;
        if (salesToday.length > 0) {
            responseObj.salesToday = salesToday[0].sum;
        }

        return {
            "status": 200,
            data: responseObj,
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: allCharts
// @Description: To get All Charts Data from db
*/
exports.DashboardCharts = async req => {
    try {
        const responseObj = {};
        var from = moment().format('YYYY-MM-DD');
        var to = moment().format('YYYY-MM-DD');
        let condition = {};
        if (req.params.status == "filter") {
            from = req.params.from;
            to = req.params.to;
            condition = {
                $and: [{
                        "date": {
                            $gte: from
                        }
                    },
                    {
                        "date": {
                            $lte: to
                        }
                    }
                ]
            }
        } else {
            condition = {
                "date": {
                    $lte: to
                }
            }
        }
        const plansUserCount = await userRepo.getAllPlanUserCount(condition);
        responseObj.plansUserCount = plansUserCount;

        let visit = await visitRepo.getAllVisit(condition);

        let visitCount = 0;
        for (let j = 0; j < visit.length; j++) {
            visitCount += visit[j].count
        }

        let checkingActiveUser = await userRepo.getActiveUserCount(condition);
        let userCount = checkingActiveUser[0].count;

        if (userCount && visitCount) {
            responseObj.getConversionRate = ((userCount / visitCount) * 100).toFixed(2);
        } else {
            responseObj.getConversionRate = 0;
        }

        let total = 0;
        for (var s = 0; s < visit.length; s++) {
            total = total + visit[s].count;
        }

        for (s = 0; s < visit.length; s++) {
            visit[s].percentage = ((visit[s].count / total) * 100).toFixed(2);
        }
        responseObj.visit = visit;
        responseObj.total_visit = total;

        const monthlyRevenue = await PaymentRepo.getMonthlyRevenue(from, to);
        responseObj.monthlyRevenue = monthlyRevenue;
        return {
            "status": 200,
            data: responseObj,
        };
    } catch (error) {
        console.log('ererer', error);
        throw error;
    }
};

/* @Method: Logout
// @Description: User Logout
*/
exports.logout = async (req, res) => req.session.destroy(() => res.redirect('/' + process.env.ADMIN_FOLDER_NAME));

/* @Method: viewmyprofile
// @Description: To get Profile Info from db
*/
exports.viewmyprofile = async req => {
    try {
        const id = req.params.id;
        const user = await userRepo.getById(id);
        return {
            "status": 200,
            data: user,
            "message": "Profile Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: updateprofile
// @Description: Update My Profile 
*/
exports.updateprofile = async req => {
    try {
        const id = req.body.id;

        const user = await userRepo.getById(id);

        if (req.files.length > 0) {

            if (!_.isEmpty(user) && user.profile_image != '') {
                if (fs.existsSync('public/uploads/user/' + user.profile_image)) {
                    const upl = await fs.unlinkSync('./public/uploads/user/' + user.profile_image);
                }
                if (fs.existsSync('./public/uploads/user/thumb/' + user.profile_image)) {
                    const upl_thumb = await fs.unlinkSync('./public/uploads/user/thumb/' + user.profile_image);
                }
            }

            gm('public/uploads/user/' + req.files[0].filename)
                .resize(100)
                .write('public/uploads/user/thumb/' + req.files[0].filename, function (err) {
                    if (err)
                        deferred.reject({
                            "success": false,
                            "status": 500,
                            data: [],
                            "message": err.message
                        });
                });

            req.body.profile_image = req.files[0].filename;
        }

        const result = await userRepo.updateById(req.body, id);
        return {
            "status": 200,
            data: result,
            "message": "Profile updated successfully"
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: statusChange
// @Description: User status change action
*/
exports.statusChange = async req => {
    try {
        const result = await userRepo.getById(req.body.id);
        const updatedStatus = (_.has(result, 'status') && result.status == "Inactive") ? "Active" : "Inactive";
        await userRepo.updateById({
            'status': updatedStatus
        }, req.body.id);
        return {
            "status": 200,
            data: result,
            "message": "Status has changed successfully"
        };
    } catch (error) {
        throw error;
    }
};

/*
// @Method: changepassword
// @Description: User password change
*/
exports.changepassword = async req => {
    try {
        const user = await userRepo.getById(req.body.id);
        if (!user) {
            throw new Error('Authentication failed. User not found.');
        } else {
            // check if password matches
            if (!User.validPassword(req.body.old_password, user.password)) {
                throw new Error('Authentication failed. Wrong password.');
            } else {

                // if user is found and password is right, check if he is an admin
                if (user.role[0].roleType == 'admin') {
                    const new_password = req.user.generateHash(req.body.password);
                    await userRepo.updateById({
                        "password": new_password
                    }, req.body.id);
                    return {
                        "status": 200,
                        data: [user],
                        "message": "Your password has been changed successfully"
                    }
                } else {
                    throw new Error('Authentication failed. Wrong credentials.');
                }
            }
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: forgotPassword
// @Description: User forgotPassword
*/
exports.forgotPassword = async req => {
    try {
        const result = await userRepo.forgotPassword(req.body);
        await transporter.sendMail({
            from: 'Admin<smith.williams0910@gmail.com>',
            to: req.body.email_p_c,
            subject: 'Admin New Password',
            html: `Your password has been updated.<br>New password: <b>` + result + `</b><br>Thank you.`
        });
        return {
            "status": 200,
            data: [result],
            "message": "Check Email For New Password"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: delete
// @Description: User delete
*/
exports.destroy = async req => {
    try {
        await userRepo.delete(req.params.id);
        return {
            "status": 200,
            data: [],
            "message": "Data Removed Successfully"
        };
    } catch (error) {
        throw error;
    }
};

exports.userListByRole = async req => {
    try {
        var role = req.user.role;
        const resultSet = await userRepo.getUsersByRole(role[0].id, req.user.id);

        return {
            "status": 200,
            data: resultSet,
            "message": "Answer Updated Successfully"
        };

    } catch (error) {
        throw error;
    }
};