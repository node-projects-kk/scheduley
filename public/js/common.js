$(".noKeyboardInput").keydown(false);

$('.ckeditor').summernote({

});

//image file validation
function validImage() {
    //Get reference of FileUpload.
    var fileUpload = document.getElementById("img_file_input");

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
    if (!regex.test(fileUpload.value.toLowerCase())) {
        $("#img_file_input").val('');
        swal.fire({
            title: "Error",
            text: "Invalid Image file.",
            type: "warning"
        });
        return false;
    } else {
        return true;
    }
}

/* Imput type file validation */
$(".only_image").on("change", function (event) {
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'x-ms-bmp']) == -1) {
        $(this).next('.imgErr').css('display', 'block');
        $(this).val('');
        return false;
    } else {
        $(this).next('.imgErr').css('display', 'none');
    }
});


// For status change //
function statusChangeFunction(type, element) {
    swal.fire({
        title: "Are you sure?",
        text: "But you will still be able to retrieve this data.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, change it!",
        cancelButtonText: "No, cancel please!",
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            statusModifier(type, element);

        } else if (result.dismiss === 'cancel') {
            swal.fire(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }, function (dismiss) {

        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal.fire(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);
}

function statusModifier(type, element) {
    var id = $(element).attr('data-team');
    var url = apiBaseURL + "/" + type + "/status-change";
    //var redirect_url = apiBaseURL + "/" + type + "/list";
    var redirect_url = $(location).attr('href');
    $.ajax({
        url: url,
        type: "POST",
        data: {
            "id": id
        },
        success: function (msg) {
            if (msg) {
                location.href = redirect_url;
            }
        }
    });
}
// For status change //


// For delete data //
$(document).on("click", ".delete", function (event) {
    var redirect_url = $(this).attr('href');
    event.preventDefault(); // prevent form submit


    /*************/
    swal.fire({
        title: "Are you sure?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",

    }).then(function (result) {
        if (result.value) {
            location.href = redirect_url;
        } else if (result.dismiss === 'cancel') {
            swal.fire(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }

    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal.fire(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);

})

// Allow only decimal number //
$('.allownumericwithdecimal').on('input', function () {
    this.value = this.value
        .replace(/[^\d.]/g, '') // numbers and decimals only
        //.replace(/(^[\d]{2})[\d]/g, '$1')   // not more than 2 digits at the beginning
        .replace(/(\..*)\./g, '$1') // decimal can't exist more than once
        .replace(/(\.[\d]{2})./g, '$1'); // not more than 4 digits after decimal
});

/*$(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }

});*/

//ONLY FOR USER_PROFILE PAGE
$('.attribute').on('ifChecked ifUnchecked', function (event) {
    $('.attribute').valid();
    var getTextBoxId = "#attr_val_" + $(this).attr('data-id');
    if (event.type == 'ifChecked') {
        $(getTextBoxId).show();
    } else {
        $(getTextBoxId).parent()
            .find("em.invalid")
            .remove();
        $(getTextBoxId).val('');
        $(getTextBoxId).hide();

    }
});

/////  FOR Event Image Prevention ///////
$("#event_image_image").on("change", function (event) {
    var size = $(this).attr('data-team');
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($("#event_image_image")[0].files.length > size) {
        $(this).val('');
        $(".msg").text('Please Select Upto ' + size + ' Images');
    } else if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'x-ms-bmp']) == -1) {
        $(this).val('');
        $(".msg").text('Invalid Image Type');
    } else {
        $(".msg").text('');
    }
});

//for prventing first character to be space 
$("input").on("keypress", function (e) {
    var startPos = e.currentTarget.selectionStart;
    if (e.which === 32 && startPos == 0)
        e.preventDefault();
});

$("textarea").on("keypress", function (e) {
    var startPos = e.currentTarget.selectionStart;
    if (e.which === 32 && startPos == 0)
        e.preventDefault();
});

//Delete Image
$(document).on("click", ".img_delete", function (event) {
    event.preventDefault();
    var url = $(this).attr('href');
    var redirect_url = window.location.href;
    var id = redirect_url.substring(redirect_url.lastIndexOf('/') + 1);
    var img = $(this).attr('data-team');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",

    }).then(function () {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                "id": id,
                img_name: img
            },
            success: function (msg) {
                if (msg) {
                    location.href = redirect_url;
                }
            }
        });

    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);

});

//for banner Image Selection
$('input[name="banner_image"]').on('click', function (event) {
    if ($(this).attr('id') == 'existing_image') {

        $(".ex_img_0").prop("checked", true);
        $("#existing_banner_img").show();
        $('#event_img').hide();
        $('#event_video').hide();
        $('#type').val('exist_image');
    } else if ($(this).attr('id') == 'banner_image') {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').show();
        $('#event_video').hide();
        $('#type').val('image');

    } else if ($(this).attr('id') == 'banner_video') {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').hide();
        $('#event_video').show();
        $('#type').val('video');
    } else {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').hide();
        $('#event_video').hide();
    }
});

$(document).on("change", "#event_image_image", function (event) {
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'x-ms-bmp']) == -1) {
        $(this).val('');
        $(".img_msg").text('Invalid Image Type');

    } else {
        $(".img_msg").text('');

    }
});

$(document).on("change", "#event_image_video", function (event) {
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['x-flv', 'mp4', 'x-mpegURL', 'MP2T', '3gpp', 'quicktime', 'x-msvideo', 'x-ms-wmv']) == -1) {
        $(this).val('');
        $(".video_msg").text('Invalid Video Type.');

    } else {
        $(".video_msg").text('');

    }
});

//Date picker
/*$('#start_date').pickadate({
    today: '',
    format: 'yyyy/mm/dd',
    close: 'Close Picker',
    clear: ''
});
$('#end_date').pickadate({
    today: '',
    format: 'yyyy/mm/dd',
    close: 'Close Picker',
    clear: ''
});
// Basic time
$('#start_time').pickatime();
$('#end_time').pickatime();

$('#date_of_birth').pickadate({
    today: '',
    format: 'yyyy/mm/dd',
    close: 'Close Picker',
    clear: '',
    onSet: function() {
        console.log('set new date')
    }
});*/

/*** Promocode admin add page expiry date */
$(function () {
    $("#kt_datepicker_1").datepicker({
        format: 'yyyy-mm-dd'
    });
});


/* Page: Student Create */
function showSubscriptionDetail(v, subs) {
    if (v != '') {
        var url = apiBaseURL + "/subscription/detail/" + v;
        $.ajax({
            url: url,
            type: "GET",
            success: function (res) {
                //$('#allowedClassSelectionNum').val(res.data.number_of_class);
                $("#no_of_class_dsp" + subs).html(res.data.number_of_class);
                $("#duration_dsp" + subs).html(res.data.duration + ' month(s)');
                $("#price_dsp" + subs).html(res.data.price);
                //$("#split_payment_dsp").html(res.data.split_payment.toUpperCase());
                if (res.data.split_payment_number == 1) {
                    $("#split_payment_dsp" + subs).html('No');
                } else {
                    $("#split_payment_dsp" + subs).html('Yes');
                }

                $('#studentClassSelection' + subs).css("display", "block");
                $("#subscriptionBlock" + subs).css("display", "block");
            },
            error: function () {
                //$('#notification-bar').text('An error occurred');
            }
        });
    } else {

        $("#studentClassSelection" + subs).css("display", "none");
        $("#subscriptionBlock" + subs).css("display", "none");
    }
}

// Ride locations

function initialize() {
    var acInputs = document.getElementsByClassName("autocomplete");
    for (var i = 0; i < acInputs.length; i++) {

        var options = {
            componentRestrictions: {
                country: ['CA', 'US', 'IN']
            }
        };

        var autocomplete = new google.maps.places.Autocomplete(acInputs[i], options);
        autocomplete.inputId = acInputs[i].id;
        // var place = autocomplete.getPlace();
        // console.log('73',autocomplete);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            GetLatlong(this.inputId);

            // document.getElementById("log").innerHTML = 'You used input with id ' + this.inputId;
        });
    }
}
initialize();

function GetLatlong(element) {

    var geocoder = new google.maps.Geocoder();
    var address = document.getElementById(element).value;

    var addressComponent = address.split(', ');
    var addressComponentCount = addressComponent.length;

    var latInput = element + '_lat';
    var longInput = element + '_long';
    var countryInput = element + '_country';
    var cityInput = element + '_city';
    var stateInput = element + '_state';
    geocoder.geocode({
        'address': address
    }, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            document.getElementById(latInput).value = latitude;
            document.getElementById(longInput).value = longitude;

            document.getElementById(countryInput).value = addressComponent[addressComponentCount - 1];
            document.getElementById(stateInput).value = addressComponent[addressComponentCount - 2];
            document.getElementById(cityInput).value = addressComponent[addressComponentCount - 3];
        }
    });
}

// Ride locations End
/** Fare Suggestion */
$(document).ready(function () {
    /*    $('#fare-suggestion-add').attr("disabled", true);
        const source_location = document.getElementById('source_location');
        const destnation_location = document.getElementById('destnation_location');

        new google.maps.places.Autocomplete(source_location);
        new google.maps.places.Autocomplete(destnation_location);*/

    $('#getDistance').click((e) => {
        const source_location_value = $('#source_location').val();
        const destnation_location_value = $('#destnation_location').val();

        if (destnation_location_value === '' || source_location_value === '') {
            alert('Please fill-up source/destination location');
        } else {
            const service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [$('#source_location').val()],
                destinations: [$('#destnation_location').val()],
                travelMode: 'DRIVING',
            }, (response, status) => {

                if (status === "OK") {
                    const element = response.rows[0].elements[0];
                    $('#fare_suggestion_distance_metadata').val(element);
                    $('#distance_amount').val(element.distance.value);
                    $('#fare_suggestion_add').attr("disabled", false);
                    $('.fareBox').show();
                } else {
                    alert("Unable to fetch distance. Please try again");
                    $('.fareBox').hide();
                }

                console.log({
                    response,
                    status
                });
            });
        }

        e.preventDefault();
    });
});



//transaction rider and  driver filteration

$("#userType").change(function () {
    console.log(this.value)
    if (this.value === 'driver' && this.value != 'rider') {
        $("#driverDrp").removeClass('hide')
        $("#riderDrp").addClass('hide')

    } else if (this.value === 'rider' && this.value != 'driver') {
        $("#riderDrp").removeClass('hide')
        $("#driverDrp").addClass('hide')

    }

})


// Allow only decimal number //    

$(".allownumericwithdecimals").on("keypress keyup blur", function (event) {
    //this.value = this.value.replace(/[^0-9\.]/g,'');
    var ignoredKeys = [8, 9, 37, 38, 39, 40];
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));

    if (ignoredKeys.indexOf(event.which) >= 0) {
        return true;
    } else if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

function validateFloatKeyPress(el) {
    var v = parseFloat(el.value);
    el.value = (isNaN(v)) ? '' : v.toFixed(2);
}

// blog post category/tags multi select
jQuery('#category_id').multiselect({
    // buttonWidth: '160px',
    includeSelectAllOption: true,
    nonSelectedText: 'Select an Option'
});


// product page add more slider area
$("#addMoreUrl").click(function () {
    var urlLength = parseInt($('#url_length').val()) + 1;

    var html = '<div class="row urlDivLess" id="urlDivLess_' + urlLength + '"><div class="form-group col-md-9 mb-2"><label for="content" class="control-label">Slider Image </label><div class="controls"><input type="file" id="image" name="slider_files[]" class="only_image" accept="image/*"><div id="upload_error" class="imgErr" style="display:none">Invalid image type</div></div></div><div class="form-group col-md-3 mb-2"><label for="content" class="control-label">Action </label><div class="controls"><button class="btn-success LessUrl" id="LessUrl_' + urlLength + '">Less</button></div></div></div>';

    $('#urlDiv').append(html);
    $('#url_length').val(urlLength);
    return false;
});

$(document).on("click", ".LessUrl", function (event) {
    var btnId = $(this).attr('id');
    var idNo = btnId.replace('LessUrl_', ' ');
    var urlLength = parseInt($('#url_length').val()) - 1;
    var divId = 'urlDivLess_' + parseInt(idNo);
    $("#" + divId).remove();
    $('#url_length').val(urlLength);
});

$(function() {
    $('.dateRangePicker').daterangepicker({
      opens: 'left',
      maxDate: new Date()
    }, function(start, end, label) {
        $('#daterange1').val(start.format('YYYY-MM-DD'));
        $('#daterange2').val(end.format('YYYY-MM-DD'));
        window.location.href = apiBaseURL + $('#name').val() + start.format('YYYY-MM-DD')+ '/'+end.format('YYYY-MM-DD');
      console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD')+apiBaseURL);
    });
  });