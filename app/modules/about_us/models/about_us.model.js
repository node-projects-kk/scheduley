const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["active", "inactive"];

const AboutUsSchema = new Schema({
    title: {
        type: String,
        default: ''
    },
    short_description: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    image: {
        type: String,
        default: ''
    },
    sub_heading: {
        title: {
            type: String,
            default: ''
        },
        description: {
            type: String,
            default: ''
        },
        value: [{
            value: {
                type: Number,
                default: 0
            },
            value_title: {
                type: String,
                default: ''
            }
        }],
    },
    about_title: {
        type: String,
        default: ''
    },
    about_content: {
        type: String,
        default: ''
    },
    about_image: {
        type: String,
        default: ''
    },
    how_it_works: {
        title: {
            type: String,
            default: ''
        },
        description: {
            type: String,
            default: ''
        },
        step1: {
            title: {
                type: String,
                default: ''
            },
            description: {
                type: String,
                default: ''
            }
        },
        step2: {
            title: {
                type: String,
                default: ''
            },
            description: {
                type: String,
                default: ''
            }
        },
        step3: {
            title: {
                type: String,
                default: ''
            },
            description: {
                type: String,
                default: ''
            }
        },
        step4: {
            title: {
                type: String,
                default: ''
            },
            description: {
                type: String,
                default: ''
            }
        },
    },
    try_block: {
        try_title: {
            type: String,
            default: ''
        },
        try_content: {
            type: String,
            default: ''
        },
    },
    slug: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: "active",
        enum: status
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    }
});

// For pagination
AboutUsSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('About_us', AboutUsSchema);