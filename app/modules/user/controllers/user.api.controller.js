const { join } = require('path');
const { promisify } = require('util');
const { readFile } = require('fs');
const gm = require('gm').subClass({ imageMagick: true });
const settingRepo = require('setting/repositories/setting.repository');
const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const userModel = require('user/models/user.model');
const planRepo = require('plan/repositories/plan.repository');
const User = new userModel();
const Q = require('q');
const jwt = require('jsonwebtoken');
const ejs = require('ejs');
var stripe = require("stripe")(config.stripe_secret_key);
var CryptoJS = require("crypto-js");
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

const readFileAsync = promisify(readFile);

exports.create = async req => {
    try {
        const roleDetails = await roleRepo.getByField({ 'role': 'user' });
        const planDetails = await planRepo.getById(req.body.plan);
        //console.log("28>>",planDetails);

        if (req.body.regType && (req.body.regType == "google" || req.body.regType == "facebook" || req.body.regType == "instagram" || req.body.regType == "twitter" || req.body.regType == "pinterest")) {
            let checkSocial = await userRepo.getByField({ "social_id": req.body.social_id, "isDeleted": false });
            if (!_.isEmpty(checkSocial)) {
                return { status: 201, data: [], "message": 'User already exists.' };
            }
        }
        else {
            const userDetails = await userRepo.getByField({ 'email': req.body.email, 'isDeleted': false });

            if (!_.isEmpty(userDetails) || !_.isNull(userDetails)) {
                return { "status": 201, data: [], "message": "Email already Exists" }
            }
        }

        if (!_.isEmpty(roleDetails)) {
            req.body.role = roleDetails._id;
        }

        if (req.body.regType && (req.body.regType == "google" || req.body.regType == "facebook" || req.body.regType == "instagram" || req.body.regType == "twitter" || req.body.regType == "pinterest")) {
            req.body.social_id = req.body.social_id;
        }
        else {
            req.body.social_id = "";
        }

        req.body.password = User.generateHash(req.body.password);

        /* For Subscription Start*/

        var stripeCardResponse = await stripe.tokens.create({
            card: {
                number: req.body.card_number,
                exp_month: parseInt(req.body.exp_month),
                exp_year: parseInt(req.body.exp_year),
                cvc: req.body.cvc
            }
        });

        //console.log("68>>",stripeCardResponse);

        var stripe_token = stripeCardResponse.id;
        var customer = await stripe.customers.create({
            email: req.body.email,
        });

        //console.log("75>>",customer);

        var payment = await stripe.subscriptions.create({
            customer: customer.id,
            items: [{ plan: planDetails.stripe_plan_id },],
            trial_period_days: 14,
        });
        //console.log("90>>",payment);

        /* Subscription End */

        req.body.stripe_subscription_id = payment.id;
        req.body.stripe_plan_id = planDetails.stripe_plan_id;
        req.body.stripe_customer_id = customer.id;
        req.body.isSubscribed = true;
        const saveUser = await userRepo.save(req.body);
        const setting_data = await settingRepo.getAllSetting();
        var settingObj = {};
        if (!_.isEmpty(setting_data)) {
            setting_data.forEach(function (element) {
                settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
            });
        }

        let userlocals = {
            firstName: saveUser.firstName,
            lastName: saveUser.lastName
        };

        let isMailSendForUser = await mailer.sendMail(settingObj.Site_Title + '<support@scheduley-iit.com>', saveUser.email, settingObj.Site_Title + ' ' + 'Registration successfully', 'new-user', userlocals);

        const user_details = {};
        user_details._id = saveUser._id;
        user_details.firstName = saveUser.firstName;
        user_details.lastName = saveUser.lastName;
        user_details.email = saveUser.email;
        user_details.stripe_subscription_id = saveUser.stripe_subscription_id;
        user_details.stripe_plan_id = saveUser.stripe_plan_id;
        user_details.stripe_customer_id = saveUser.stripe_customer_id;

        //const payload = { id: saveUser._id };
        //const token = jwt.sign(payload, config.jwtSecret, {expiresIn: 86400});

        return { status: 200, data: user_details,/*token:token,*/message: "Profile Created Suceessfully" };
    }
    catch (error) {
        return { status: 500, message: error.message };
    }
}

exports.signin = async req => {
    try {
        if (Boolean(req.body.social_id)) {
            console.log("1");
            if (req.body.regType === 'facebook' || req.body.regType === 'google' || req.body.regType === 'instagram' || req.body.regType === 'pinterest' || req.body.regType === 'twitter') {
                let checkUserData = await userRepo.getByField({ "social_id": req.body.social_id, "isDeleted": false });
                if (!_.isEmpty(checkUserData)) {
                    if (checkUserData.status && checkUserData.status === "Active") {
                        const payload = { id: checkUserData._id };
                        const token = jwt.sign(payload, config.jwtSecret, { expiresIn: 86400 });
                        return { status: 200, data: checkUserData, isLoggedIn: true, token: token, "message": 'Login successfull.' };
                    }
                    else {
                        return { status: 201, data: [], isLoggedIn: false, "message": 'User status is inactive.' };
                    }
                }
                else {
                    return { status: 201, data: [], isLoggedIn: false, "message": 'User not found.' };
                }
            }
        }
        else if (!Boolean(req.body.social_id) && req.body.email != '') {
            console.log("2");
            const user = await userRepo.fineOneWithRole(req.body);
            if (user.isDeleted == true) {
                return { "status": 201, isLoggedIn: false, data: [], "message": 'Authentication failed.' }
            }
            else {
                if (!_.isEmpty(user.role)) {
                    if (user.status != 'Active') {
                        return { "status": 201, data: [], isLoggedIn: false, "message": "Your Account has been Blocked or Inactive, please contact admin to Unblock or Active it." }
                    }
                    const payload = { id: user._id };
                    const token = jwt.sign(payload, config.jwtSecret, { expiresIn: 86400 });
                    req.session.token = token;
                    req.user = user;

                    if (_.has(req.body, 'deviceType') && !_.isEmpty(req.body.deviceType)) {
                        let userData = { 'deviceType': req.body.deviceType, 'deviceToken': req.body.deviceToken };
                        const updated = await userRepo.updateById(userData, user._id);
                    }

                    return { "status": 200, isLoggedIn: true, token: token, data: user, "message": "You have successfully logged in" };
                }
                else {
                    return { "status": 201, isLoggedIn: false, data: [], "message": 'Authentication failed. Wrong credentials.' }
                }
            }
        }
    }
    catch (error) {
        console.log('error', error.message);
        return { "status": 500, data: [], "message": error.message }
    }
};


exports.getMyProfile = async req => {
    try {
        const user = await userRepo.getById(req.user._id);
        let data = user;
        return {
            status: 200,
            data,
            message: 'Profile Info fetched Successfully'
        };
    } catch (error) {
        return {
            "success": false,
            "status": 500,
            data: [],
            "message": 'Something went wrong'
        }
    }
}

exports.updateProfile = async req => {
    try {

        const result = await userRepo.getByField({
            'email': req.body.email,
            'isDeleted': false,
            '_id': {
                $ne: req.user._id
            }
        });
        if (_.isEmpty(result)) {
            const result2 = await userRepo.updateById(req.body, req.user._id);
            return {
                "status": 200,
                data: result2,
                "message": "User updated Successfully"
            };
        } else {
            return {
                "status": 500,
                data: [],
                "message": "This email address is already exist!"
            };
        }

    } catch (error) {
        // console.log(error);
        return {
            status: 500,
            data: [],
            message: 'Something went wrong'
        };
    }
}

exports.forgotPassword = async (req, res) => {
    try {
        const user = await userRepo.getByField({
            email: req.body.email
        });
        if (!_.isEmpty(user)) {
            var ciphertext = cryptr.encrypt(user._id);
            const link = config.frontend_url + "/#/auth/reset-password" + "?token=" + ciphertext;

            const setting_data = await settingRepo.getAllSetting();
            var settingObj = {};
            if (!_.isEmpty(setting_data)) {
                setting_data.forEach(function (element) {
                    settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                });
            }
            let userlocals = {
                firstName: user.firstName,
                lastName: user.lastName,
                link: link
            };
            await userRepo.updateById({
                token: ciphertext
            }, user._id);

            let isMailSendForUser = await mailer.sendMail(settingObj.Site_Title + '<support@scheduley-iit.com>', user.email, settingObj.Site_Title + ' ' + 'Reset Password', 'forgot-password', userlocals);
            return {
                status: 200,
                data: [],
                message: 'Password Reset Link Sent to your mail.'
            };
        } else {
            return {
                status: 500,
                data: [],
                message: 'No matching user found'
            };
        }
    } catch (error) {
        console.log("257", error);
        return {
            status: 500,
            data: error,
            message: 'Something went wrong'
        };
    }
}

exports.checkExpiryOfToken = async (req, res) => {
    try {
        const user = await userRepo.getById(cryptr.decrypt(req.body.token));
        if (user.token != "") {
            await userRepo.updateById({
                token: ""
            }, cryptr.decrypt(req.body.token));
            return {
                status: 200,
                message: 'Link active.'
            };
        }
        else {
            return {
                status: 500,
                message: 'Link Expired'
            };
        }
    } catch (error) {
        return {
            status: 500,
            data: error.message,
            message: 'Something went wrong'
        };
    }
}

exports.resetPassword = async (req, res) => {
    try {
        if (req.body.token) {
            var password = User.generateHash(req.body.password);
            await userRepo.updateById({
                password: password
            }, cryptr.decrypt(req.body.token));
            return {
                status: 200,
                message: 'Password changed successfully.'
            };
        }
        else {
            return {
                status: 201,
                message: 'Link Expired'
            };
        }
    } catch (error) {
        return {
            status: 500,
            data: error.message,
            message: 'Something went wrong'
        };
    }
}

exports.changePassword = async (req, res) => {
    try {
        const user = await userRepo.getById(req.user._id);
        if ((!User.validPassword(req.body.currentPassword, user.password))) {
            return {
                status: 201,
                data: [],
                "message": 'Wrong current password'
            };
        } else {
            const newPassword = User.generateHash(req.body.newPassword);
            await userRepo.updateById({
                password: newPassword
            }, req.user._id);
            return {
                status: 200,
                data: [],
                "message": 'Password updated successfully'
            };
        }
    } catch (error) {
        return {
            status: 500,
            data: error,
            message: 'Something went wrong'
        };
    }
}

exports.changeNameOrEmail = async (req, res) => {
    try {
        let user;
        if (_.has(req.body, "firstName")) {
            user = await userRepo.updateById({
                firstName: req.body.firstName,
                lastName: req.body.lastName
            }, req.user._id);
            return {
                status: 200,
                data: user,
                "message": 'Name updated successfully'
            };
        }
        else if (_.has(req.body, "email")) {
            let emailCheck = await userRepo.getByField({ "email": req.body.email });
            if (emailCheck) {
                return {
                    status: 201,
                    data: [],
                    "message": 'Email already exists'
                };
            }
            else {
                user = await userRepo.updateById({
                    email: req.body.email
                }, req.user._id);
                return {
                    status: 200,
                    data: user,
                    "message": 'Email updated successfully'
                };
            }
        }
    } catch (error) {
        return {
            status: 500,
            data: error,
            message: 'Something went wrong'
        };
    }
}

exports.addImage = async req => {

    try {
        if (!_.isEmpty(req.files)) {
            req.files.forEach(function (file) {
                req.body.profile_image = file.filename;

                gm('public/uploads/user/' + file.filename)
                    .resize(225)
                    .write('public/uploads/user/thumb/' + file.filename, function (err) {
                        if (err) return {
                            status: 200,
                            data: [],
                            message: err
                        };
                        // thumb = file.filename;
                    });
            });

            const success = await userRepo.updateById(req.body, req.user._id);
            return {
                status: 200,
                data: success,
                message: 'Image Saved Successfully'
            };
        } else {
            return { status: 500, data: [], message: 'Image is required' };
        }

    } catch (error) {
        return { status: 500, data: [], message: error };
    }
}

exports.unsubscribe = async req => {
    try {
        let user_id = req.user._id;
        let checkUserData = await userRepo.getByField({ "_id": user_id, "isDeleted": false });

        if (!_.isEmpty(checkUserData)) {
            let stripeSubscriptionId = checkUserData.stripe_subscription_id;
            if (stripeSubscriptionId) {
                let removeResult = await stripe.subscriptions.del(stripeSubscriptionId);
                if (removeResult) {
                    let updateVal = { "stripe_subscription_id": "", "isSubscribed": false };
                    let updateResult = await userRepo.updateById(updateVal, user_id);
                    if (updateResult) {
                        return { status: 200, data: updateResult, message: 'User unsubscribed' };
                    }
                }
                else {
                    return { status: 201, data: removeResult, message: 'Unsubscibe failure!' };
                }
            }
        }
        else {
            return { status: 201, data: [], message: 'User not found' };
        }
    }
    catch (error) {
        return { status: 500, data: [], message: error.message };
    }
}

exports.deleteUserAccount = async req => {
    try {
        let user_id = req.user._id;
        let checkUserData = await userRepo.getByField({ "_id": user_id, "isDeleted": false });
        if (!_.isEmpty(checkUserData)) {
            let updateVal = { "isDeleted": true };
            let updateResult = await userRepo.updateById(updateVal, user_id);
            if (updateResult) {
                return { status: 200, data: updateResult, message: 'Your account is deleted' };
            }
        }
        else {
            return { status: 201, data: [], message: 'User not found' };
        }
    }
    catch (error) {
        return { status: 500, data: [], message: error.message };
    }
}

/* 
exports.mobileVerify = async req => {
    try {
        const userdetails = await userRepo.getById(req);
        userdetails.mobileVerified = true;
        const success = await userRepo.updateById(userdetails, userdetails._id);
        return {
            status: 200,
            data: success,
            message: 'Mobile verified successfully'
        };
    } catch (error) {
        throw error;
    }
}

exports.emailverify = async req => {
    try {
        var checkObj = {
            'email': req.body.email
        }
        const userdetails = await userRepo.getByField(checkObj);

        if (!_.isEmpty(userdetails)) {
            if (parseInt(userdetails.otp_number) == parseInt(req.body.otp_number)) {
                if (userdetails.emailVerified) {
                    return {
                        status: 500,
                        data: [],
                        message: "Email already verified"
                    };
                } else {
                    userdetails.profileVerified = true;
                    userdetails.emailVerified = true;
                    const success = await userRepo.updateById(userdetails, userdetails._id);

                    const user_details = {};
                    user_details._id = success._id;
                    user_details.firstName = success.firstName;
                    user_details.lastName = success.lastName;
                    user_details.email = success.email;
                    user_details.profileVerified = success.profileVerified;
                    user_details.emailVerified = success.emailVerified;
                    user_details.mobileVerified = success.mobileVerified;

                    const setting_data = await settingRepo.getAllSetting();
                    var settingObj = {};
                    if (!_.isEmpty(setting_data)) {
                        setting_data.forEach(function (element) {
                            settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                        });
                    }

                    var mailOptions = {
                        from: settingObj.Site_Title + '<support@beashark.com>',
                        to: userdetails.email,
                        subject: settingObj.Site_Title + " Email Verification",
                        html: 'Hello ' + userdetails.firstName + ' ' + userdetails.lastName + ',<br><br>Your email has been verified successfully.<br><br>Thank you.'
                    };
                    await transporter.sendMail(mailOptions);

                    return {
                        status: 200,
                        data: user_details,
                        message: 'Email verified successfully'
                    };
                }
            } else {
                return {
                    status: 500,
                    data: [],
                    message: "OTP not valid"
                };
            }
        } else {
            return {
                status: 500,
                data: [],
                message: "Email not valid!"
            };
        }
    } catch (error) {
        throw error;
    }
} */

exports.getBrowseList = async req => {
    try {
        const currentPage = req.params.page || 1;
        const {
            data,
            pageCount
        } = await userRepo.getBrowseList({
            '_id': -1
        }, currentPage);

        return {
            "status": 200,
            data,
            current: currentPage,
            pages: pageCount,
            "message": "Data Fetched Successfully"
        };
    } catch (error) {
        return {
            "success": false,
            "status": 500,
            data: [],
            "message": 'Something went wrong'
        }
    }
};

exports.updateProfileUser = async req => {
    try {
        const result2 = await userRepo.updateById(req.body, req.user._id);
        return {
            "status": 200,
            data: result2,
            "message": "User updated Successfully"
        };
    } catch (error) {
        return {
            "success": false,
            "status": 500,
            data: [],
            "message": 'Something went wrong'
        }
    }
};


