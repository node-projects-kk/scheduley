const Home = require('home/models/home.model');
const perPage = config.PAGINATION_PERPAGE;

class HomeRepository {
    constructor() {}

    async getAll(searchQuery, sortOrder = {
        '_id': -1
    }) {
        try {
            const query = [{}];
            // serach by keyword
            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    const search = searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                            'title': {
                                '$regex': search,
                                '$options': 'i'
                            }
                        }]
                    });
                }
            }
            searchQuery = {
                "$and": query
            };
            return await Home.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $match: searchQuery
                },
                {
                    $project: {
                        _id: "$_id",
                        banner_title: "$banner_block.banner_title",
                        status: "$status"
                    }
                }
            ]);
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await Home.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Home.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllHome() {
        try {
            return await Home.find().lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new HomeRepository();