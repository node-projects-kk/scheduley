const promocodeRepo = require('promo_code/repositories/promocode.repository');
const userRepo = require('user/repositories/user.repository');
const planRepo = require('plan/repositories/plan.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');
var moment = require('moment');

exports.create = async (req, res) => {
	try {
		const uresult = await userRepo.getAllUsers(_searchQuery = {});
		const presult = await planRepo.getAllByField({
			"isDeleted": false
		});
		return {
			"status": 200,
			data: {
				"user": uresult,
				"plan": presult
			},
			"message": ""
		};
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error
		};
	}
};

exports.store = async req => {
	try {
		if (req.body.radiotype == "User") {
			req.body.user_id = req.body.userid;
			req.body.plan_id = null;
			req.body.promo_type = "User";
		} else if (req.body.radiotype == "Subscription") {
			req.body.plan_id = req.body.planid;
			req.body.user_id = null;
			req.body.promo_type = "Subscription";
		} else {
			req.body.user_id = null;
			req.body.plan_id = null;
			req.body.promo_type = "General";
		}

		const result_exist = await promocodeRepo.getByField({
			'title': req.body.title
		});

		if (_.isEmpty(result_exist)) {
			req.body.slug = slugify(req.body.title, {
				replacement: '-',
				lower: true
			});
			req.body.price = parseFloat(req.body.price).toFixed(2);
			const result = await promocodeRepo.save(req.body);
			return {
				"status": 200,
				data: result,
				"message": 'Promo Code stored successfully'
			};
		} else {
			return {
				"status": 200,
				data: [],
				"message": 'Promo Code already exist with this title!'
			};
			//throw new Error("Product already exist with this title!");
		}
	} catch (error) {
		// console.log("73>>", error);
		throw error;
	}
};

exports.edit = async req => {
	try {
		const data = await promocodeRepo.getById(req.params.id);
		const uresult = await userRepo.getAllUsers(_searchQuery = {});
		const presult = await planRepo.getAllByField({
			"isDeleted": false
		});
		return {
			"status": 200,
			data: {
				data,
				"user": uresult,
				"plan": presult
			},
			"message": "Page data fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};

exports.update = async req => {
	try {
		let arr = [];

		if (req.body.radiotype == "User") {
			req.body.user_id = req.body.userid;
			req.body.plan_id = null;
			req.body.promo_type = "User";
		} else if (req.body.radiotype == "Subscription") {
			req.body.plan_id = req.body.planid;
			req.body.user_id = null;
			req.body.promo_type = "Subscription";
		} else {
			req.body.user_id = null;
			req.body.plan_id = null;
			req.body.promo_type = "General";
		}

		const result = await promocodeRepo.getByField({
			'title': req.body.title,
			'_id': {
				$ne: req.body.id
			}
		});

		if (_.isEmpty(result)) {
			req.body.slug = slugify(req.body.title, {
				replacement: '-',
				lower: true
			});
			req.body.price = parseFloat(req.body.price).toFixed(2);
			const result = await promocodeRepo.updateById(req.body, req.body.id);
			return {
				"status": 200,
				data: result,
				"message": "Promo Code updated Successfully"
			};
		} else {
			return {
				"status": 500,
				data: [],
				"message": "This title is already exist!"
			};
		}
	} catch (error) {
		throw error;
	}
};

exports.list = async req => {
	try {
		var from = moment().format('YYYY-MM-DD');
		var to = moment().format('YYYY-MM-DD');
		if (req.params.from) {
			from = req.params.from;
			to = req.params.to;
		}
		const data = await promocodeRepo.getAll(req.query, {
			'expiry_date': 1
		}, from, to);
		return {
			"status": 200,
			"data": data,
			"message": "Promo list fetched successfully"
		};
	} catch (error) {
		throw error;
	}
};

exports.statusChange = async req => {
	try {
		const result = await promocodeRepo.getById(req.body.id);
		const updatedStatus = (_.has(result, 'status') && result.status == "Inactive") ? "Active" : "Inactive";
		const result2 = await promocodeRepo.updateById({
			'status': updatedStatus
		}, req.body.id);
		return {
			"status": 200,
			data: result2,
			"message": "Promo code status has changed successfully"
		}
	} catch (error) {
		throw error;
	}
};

exports.destroy = async req => {
	try {
		const result = await promocodeRepo.delete(req.params.id);
		return {
			"status": 200,
			data: result,
			"message": "Promo code Removed Successfully"
		};
	} catch (error) {
		throw error;
	}
};