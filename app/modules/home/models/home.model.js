const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["active", "inactive"];

const HomeSchema = new Schema({
  banner_block: {
    banner_title: {type: String, default: ''},
    banner_image: { type: String, default: '' }
  },
  middle_content_block: { 
    small_content: { type: String, default: '' },
    middle_content_logo: [{ 
      logo: {type: String, default: ''},
      _id: false
   }],
    post_content_header: { type: String, default: '' },
    post_content_text: { type: String, default: '' },
    post_content_right_image: { type: String, default: '' },
    post_content_middle_header: { type: String, default: '' },
    post_content_middle_text: { type: String, default: '' },
    post_content_middle_image: { type: String, default: '' },
    post_content_lower_header: { type: String, default: '' },
    post_content_lower_text: { type: String, default: '' },

    post_content_lower_feature: [
      {
        image : { type: String, default: '' },
        header : { type: String, default: '' },
        description : { type: String, default: '' },
        _id : false
      }
    ]
},
company_content_block: {
    header_title: {type: String, default: ''},
    header_description: {type: String, default: ''},
    post_content_middle_header: { type: String, default: '' },
    post_content_middle_text: { type: String, default: '' },
    post_content_middle_image: { type: String, default: '' }
},
pricing_content_block: {
    header_title: {type: String, default: ''},
    header_text: {type: String, default: ''},
    header_pricing_business_image: {type: String, default: ''},
    post_content_middle_header: { type: String, default: '' },
    post_content_middle_text: { type: String, default: '' }
},
status: { type: String, default: "active", enum: status},
createdAt: { type: Date, default: Date.now(),}    
});

// For pagination
HomeSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Home', HomeSchema);