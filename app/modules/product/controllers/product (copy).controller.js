const productRepo = require('product/repositories/product.repository');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');

/*
// @Method: edit
// @Description:  render product update page
*/
exports.edit = async req => {
    try {
        const data = await productRepo.getById(req.params.id);
        return {
            "status": 200,
            data,
            "message": "Product Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* 
// @Method: update
// @Description: product update action
*/
// exports.update = async req => {
//     try {
//         const result = await productRepo.getByField({
//             'title': req.body.title,
//             '_id': {
//                 $ne: req.body.cms_id
//             }
//         });
//         if (_.isEmpty(result)) {
//             const result2 = await productRepo.updateById(req.body, req.body.cms_id);
//             return {
//                 "status": 200,
//                 data: result2,
//                 "message": "Product Updated Successfully"
//             };
//         } else {
//             return new Error({
//                 "status": 500,
//                 data: [],
//                 "message": "This title is already exist!"
//             });
//         }
//     } catch (error) {
//         throw error;
//     }
// };






exports.update = async req => {
    try {
        const olddata = await productRepo.getById(req.body.pro_id);
	
	let bannerBlockObj = {};
	let middleContentBlockObj = {};
	let sliderBlockObj = {};
	let powerToolBlockObj = {};
	let tryBlockObj = {};
	let sliderImgArr =[];
	
	bannerBlockObj.title = req.body.title;
	
	middleContentBlockObj.post_title = req.body.post_title;
	middleContentBlockObj.post_content = req.body.post_content;
	middleContentBlockObj.calendar_title = req.body.calendar_title;
	middleContentBlockObj.calendar_content = req.body.calendar_content;
	middleContentBlockObj.phone_title = req.body.phone_title;
	middleContentBlockObj.phone_content = req.body.phone_content;
	middleContentBlockObj.draftpost_title = req.body.draftpost_title;
	middleContentBlockObj.draftpost_content = req.body.draftpost_content;
	
	sliderBlockObj.left_title_1 = req.body.left_title_1;
	sliderBlockObj.left_content_1 = req.body.left_content_1;
	sliderBlockObj.left_title_2 = req.body.left_title_2;
	sliderBlockObj.left_content_2 = req.body.left_content_2;
	sliderBlockObj.left_title_3 = req.body.left_title_3;
	sliderBlockObj.left_content_3 = req.body.left_content_3;
	sliderBlockObj.left_title_4 = req.body.left_title_4;
	sliderBlockObj.left_content_4 = req.body.left_content_4;
	
	powerToolBlockObj.tool_heading = req.body.tool_heading;
	powerToolBlockObj.tool_content = req.body.tool_content;
	powerToolBlockObj.sub_title_1 = req.body.sub_title_1;
	powerToolBlockObj.sub_content_1 = req.body.sub_content_1;
	powerToolBlockObj.sub_title_2 = req.body.sub_title_2;
	powerToolBlockObj.sub_content_2 = req.body.sub_content_2;
	powerToolBlockObj.sub_title_3 = req.body.sub_title_3;
	powerToolBlockObj.sub_content_3 = req.body.sub_content_3;
	
	tryBlockObj.try_title = req.body.try_title;
	tryBlockObj.try_content = req.body.try_content;
	tryBlockObj.try_button_text = req.body.try_button_text;
	
	let uploadPath = 'public/uploads/product/';
	let uploadThumbPath = 'public/uploads/product/thumb/';
	
	console.log("68>>",req.files);
	
	if (_.has(req, 'files')) {
		if (req.files.length > 0) {
			if (req.files[0].fieldname == "banner_image") {
				if (!_.isEmpty(olddata.banner_block)) {
					if (fs.existsSync(uploadPath + olddata.banner_block.image)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.banner_block.image);
					}
					if (fs.existsSync(uploadThumbPath + olddata.banner_block.image)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.banner_block.image);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				bannerBlockObj.image = req.files[0].filename;
				req.body.banner_block = bannerImgObj;
			}
			else if (req.files[0].fieldname == "post_left_image") {
				if (!_.isEmpty(olddata.middle_content_block)) {
					if (fs.existsSync(uploadPath + olddata.middle_content_block.post_left_image)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.middle_content_block.post_left_image);
					}
					if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.post_left_image)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.post_left_image);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				middleContentBlockObj.post_left_image = req.files[0].filename;
				req.body.middle_content_block = middleContentBlockObj;
			}
			else if (req.files[0].fieldname == "calendar_right_image") {
				if (!_.isEmpty(olddata.middle_content_block)) {
					if (fs.existsSync(uploadPath + olddata.middle_content_block.calendar_right_image)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.middle_content_block.calendar_right_image);
					}
					if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.calendar_right_image)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.calendar_right_image);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				middleContentBlockObj.calendar_right_image = req.files[0].filename;
				req.body.middle_content_block = middleContentBlockObj;
			}
			else if (req.files[0].fieldname == "phone_left_image") {
				if (!_.isEmpty(olddata.middle_content_block)) {
					if (fs.existsSync(uploadPath + olddata.middle_content_block.phone_left_image)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.middle_content_block.phone_left_image);
					}
					if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.phone_left_image)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.phone_left_image);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				middleContentBlockObj.phone_left_image = req.files[0].filename;
				req.body.middle_content_block = middleContentBlockObj;
			}
			else if (req.files[0].fieldname == "draftpost_right_image") {
				if (!_.isEmpty(olddata.middle_content_block)) {
					if (fs.existsSync(uploadPath + olddata.middle_content_block.draftpost_right_image)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.middle_content_block.draftpost_right_image);
					}
					if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.draftpost_right_image)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.draftpost_right_image);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				middleContentBlockObj.draftpost_right_image = req.files[0].filename;
				req.body.middle_content_block = middleContentBlockObj;
			}
			else if (req.files[0].fieldname == "slider_files") {
				req.files.forEach(function (item) {
					if (fs.existsSync(uploadPath + item.filename)) {
						const upl_resume = await fs.unlinkSync(uploadPath + item.filename);
					}
					if (fs.existsSync(uploadThumbPath + item.filename)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + item.filename);
					}	
					sliderImgArr.push(item.filename);
					gm(uploadPath + item.filename).resize(100).write(uploadThumbPath + item.filename, function (err) {
						if (err) throw new Error(err.message);
					});
				});
			        sliderBlockObj.slider_files = sliderImgArr;
				req.body.slider_block = sliderBlockObj;
			}
			else if (req.files[0].fieldname == "sub_file_1") {
				if (!_.isEmpty(olddata.power_tool_block)) {
					if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_1)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_1);
					}
					if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_1)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_1);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				powerToolBlockObj.sub_file_1 = req.files[0].filename;
				req.body.power_tool_block = powerToolBlockObj;
			}
			else if (req.files[0].fieldname == "sub_file_2") {
				if (!_.isEmpty(olddata.power_tool_block)) {
					if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_2)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_2);
					}
					if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_2)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_2);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				powerToolBlockObj.sub_file_2 = req.files[0].filename;
				req.body.power_tool_block = powerToolBlockObj;
			}
			else if (req.files[0].fieldname == "sub_file_3") {
				if (!_.isEmpty(olddata.power_tool_block)) {
					if (fs.existsSync(uploadPath + olddata.power_tool_block.sub_file_3)) {
						const upl_resume = await fs.unlinkSync(uploadPath + olddata.power_tool_block.sub_file_3);
					}
					if (fs.existsSync(uploadThumbPath + olddata.power_tool_block.sub_file_3)) {
						const upl_thumb_resume = await fs.unlinkSync(uploadThumbPath + olddata.power_tool_block.sub_file_3);
					}	
				}
				gm(uploadPath + req.files[0].filename).resize(100).write(uploadThumbPath + req.files[0].filename, function (err) {
					if (err) throw new Error(err.message);
				});
				powerToolBlockObj.sub_file_3 = req.files[0].filename;
				req.body.power_tool_block = powerToolBlockObj;
			}
		}
	}
			
			//if (_.isEmpty(olddata)) {
				//if (fs.existsSync('public/uploads/profile/' + data.profile_image)) {
				//					const upl_resume = await fs.unlinkSync('public/uploads/profile/' + data.profile_image);
				//				}
				//				if (fs.existsSync('public/uploads/profile/thumb/' + data.profile_image)) {
				//					const upl_thumb_resume = await fs.unlinkSync('public/uploads/profile/thumb/' + data.profile_image);
				//				}
			//	fs.unlink('public/uploads/product/' + olddata.banner_block.banner_image, function (err) {
			//		console.log('err 70', err);
			//		if (err) throw new Error(err.message);
			//	});
			//
			//	fs.unlink('public/uploads/product/thumb/' + olddata.banner_block.banner_image, function (err) {
			//		console.log('err 75', err);
			//		if (err) throw new Error(err.message);
			//	});
			//}
		
        }
	}
 
	
	
        console.log("100>>",req.body)

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.middle_content_block.post_left_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.middle_content_block.post_left_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.middle_content_block.post_left_image = req.files[0].filename;
        //}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.middle_content_block.calendar_right_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.middle_content_block.calendar_right_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.middle_content_block.calendar_right_image = req.files[0].filename;
        //}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.middle_content_block.phone_left_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.middle_content_block.phone_left_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.middle_content_block.phone_left_image = req.files[0].filename;
        //}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.middle_content_block.draftpost_right_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.middle_content_block.draftpost_right_image, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.middle_content_block.draftpost_right_image = req.files[0].filename;
        //}

        ////if (req.files.length > 0) {
        ////    if (_.isEmpty(olddata)) {
        ////        req.files.forEach(function (item) {
        ////            if (item.fieldname == 'slider_files') {
        ////                result.images.push(item.filename);
        ////                if (fs.existsSync("./public/uploads/product/" + item.filename)) {
        ////                    fs.unlink('public/uploads/product/' + olddata.slider_block.slider_files, function (err) {
        ////                        if (err) throw new Error(err.message);
        ////                    });
        ////                }
        ////                if (fs.existsSync("./public/uploads/product/thumb/" + item.filename)) {
        ////                    fs.unlink('public/uploads/product/thumb/' + olddata.slider_block.slider_files, function (err) {
        ////                        if (err) throw new Error(err.message);
        ////                    });
        ////                }
        ////
        ////            }
        ////        });
        ////        gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        ////            if (err) throw new Error(err.message);
        ////        });
        ////        req.body.slider_block.slider_files = olddata.slider_block.slider_files;
        ////    }
        ////}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.power_tool_block.sub_file_1, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.power_tool_block.sub_file_1, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.power_tool_block.sub_file_1 = req.files[0].filename;
        //}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.power_tool_block.sub_file_2, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.power_tool_block.sub_file_2, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.power_tool_block.sub_file_2 = req.files[0].filename;
        //}

        //if (req.files.length > 0) {
        //    if (_.isEmpty(olddata)) {
        //        fs.unlink('public/uploads/product/' + olddata.power_tool_block.sub_file_3, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //
        //        fs.unlink('public/uploads/product/thumb/' + olddata.power_tool_block.sub_file_3, function (err) {
        //            if (err) throw new Error(err.message);
        //        });
        //    }
        //    gm('public/uploads/product/' + req.files[0].filename).resize(100).write('public/uploads/product/thumb/' + req.files[0].filename, function (err) {
        //        if (err) throw new Error(err.message);
        //    });
        //
        //    req.body.power_tool_block.sub_file_3 = req.files[0].filename;
        //}

      //  const result = await productRepo.updateById(req.body, req.body.pro_id);
       // console.log('result <> 226', result);
       // return {"status": 200, data: result, "message": "Product updated Successfully"  };

    } catch (error) {
        throw error;
    }
};






/* 
// @Method: list
// @Description: To get all the Product from DB
*/
exports.list = async req => {
    try {
        const currentPage = req.query.page || 1;
        const data = await productRepo.getAll(req.query, {
            'cms.title': 1
        });
        return {
            "status": 200,
            data,
            "message": "Product List Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/*
// @Method: statusChange
// @Description: Product status change action
*/
exports.statusChange = async req => {
    try {
        const result = await productRepo.getById(req.body.id);
        const updatedStatus = (_.has(result, 'status') && result.status == "inactive") ? "active" : "inactive";
        const result2 = await cmsRepo.updateById({
            'status': updatedStatus
        }, req.body.id);
        return {
            "status": 200,
            data: result2,
            "message": "Product status has changed successfully"
        }
    } catch (error) {
        throw error;
    }
};

/* 
// @Method: delete
// @Description: Product delete
*/
exports.destroy = async req => {
    try {
        const result = await productRepo.delete(req.params.id);
        return {
            "status": 200,
            data: result,
            "message": "Product Removed Successfully"
        };
    } catch (error) {
        throw error;
    }
};