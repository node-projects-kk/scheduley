define({ "api": [
  {
    "type": "get",
    "url": "/about",
    "title": "About Page Data",
    "version": "1.0.0",
    "group": "CMS",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5ced058cbffe823e6bb252ea\",\n            \"title\": \"scheduley-iit about us\",\n            \"short_description\": \"short descripton\",\n            \"description\": \"long description\",\n            \"image\": \"image_1568717373015_autumn.jpg\",\n            \"sub_heading\": {\n                \"title\": \"sub heading title\",\n                \"description\": \"sub heading desc\",\n                \"value\": [\n                    {\n                        \"value\": 1,\n                        \"value_title\": \"value 1 title\"\n                    },\n                    {\n                        \"value\": 2,\n                        \"value_title\": \"value 2 title\"\n                    },\n                    {\n                        \"value\": 3,\n                        \"value_title\": \"value 3 title\"\n                    }\n                ]\n            },\n            \"about_title\": \"About Us312\",\n            \"about_content\": \"Lorem Ipsum 123\",\n            \"about_image\": \"about_image_1568717373087_nature-benefits.jpg\",\n            \"how_it_works\": {\n                \"title\": \"how it works\",\n                \"description\": \"how it works desc\",\n                \"step1\": {\n                    \"title\": \"step 1\",\n                    \"description\": \"step 1 desc\"\n                },\n                \"step2\": {\n                    \"title\": \"step 2\",\n                    \"description\": \"step 2 desc\"\n                },\n                \"step3\": {\n                    \"title\": \"Step 3\",\n                    \"description\": \"Step 3 desc\"\n                },\n                \"step4\": {\n                    \"title\": \"step 4\",\n                    \"description\": \"step 4 desc123\"\n                }\n            },\n            \"try_block\": {\n                \"try_title\": \"Try Scheduley Free for 14 Days\",\n                \"try_content\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas orci tortor, tincidunt et nisi at laoreet maximus nulla. Cras ac ligula vulputate, pretium metus sed, bibendum diam.\"\n            },\n            \"slug\": \"about-cms\",\n            \"status\": \"active\",\n            \"createdAt\": \"2019-05-27T11:40:57.467Z\",\n            \"team_data\": [\n                {\n                    \"_id\": \"5cecf9cabb7b742baaec2c87\",\n                    \"firstName\": \"Test\",\n                    \"lastName\": \"Demo\",\n                    \"email\": \"test@demo.com\",\n                    \"designation\": \"CEO\",\n                    \"instagram\": \"Test Demo\",\n                    \"facebook\": \"www\",\n                    \"image\": \"image_1559034314203_featured-small-circular.jpg\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-05-28T09:05:14.221Z\",\n                    \"updatedAt\": \"2019-05-29T06:41:00.466Z\",\n                    \"__v\": 0\n                },\n                {\n                    \"_id\": \"5cee5dc1b285d71855e01b25\",\n                    \"firstName\": \"Archit1\",\n                    \"lastName\": \"Singh1\",\n                    \"email\": \"test@gmail.com1\",\n                    \"designation\": \"Software Developer1\",\n                    \"instagram\": \"insta1\",\n                    \"facebook\": \"wwww1\",\n                    \"image\": \"image_1559125673619_download.jpeg\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-05-29T10:24:01.388Z\",\n                    \"updatedAt\": \"2019-05-29T10:27:53.640Z\",\n                    \"__v\": 0\n                }\n            ]\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/cms.routes.js",
    "groupTitle": "CMS",
    "name": "GetAbout"
  },
  {
    "type": "get",
    "url": "/faq",
    "title": "Faq Page Data",
    "version": "1.0.0",
    "group": "CMS",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5ce7b5febffe823e6b814ae4\",\n            \"question\": \"I am new to the business.\",\n            \"answer\": \"It saves you a lot of work, as we're running the adsswqwqw\",\n            \"status\": \"active\",\n            \"is_deleted\": false,\n            \"created_at\": \"2019-05-20T12:01:35.151Z\",\n            \"__v\": 0\n        },\n        {\n            \"_id\": \"5ce7c787bffe823e6b8224d8\",\n            \"question\": \"Sample Question?\",\n            \"answer\": \"Sample Answer....!\",\n            \"status\": \"active\",\n            \"is_deleted\": false,\n            \"created_at\": \"2019-05-20T12:01:35.151Z\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/cms.routes.js",
    "groupTitle": "CMS",
    "name": "GetFaq"
  },
  {
    "type": "get",
    "url": "/feature",
    "title": "Featured Page Data",
    "version": "1.0.0",
    "group": "CMS",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5cebc9bbbffe823e6ba54917\",\n            \"banner_block\": {\n                \"title\": \"Every feature you need to master social media marketing.\",\n                \"image\": \"banner_image_1559049576675_f5.jpg\"\n            },\n            \"middle_content_block\": {\n                \"post_title\": \"Posts Queue11\",\n                \"post_content\": \"All your scheduled posts in one place. Drag and drop to reorder.\",\n                \"calendar_title\": \"Calendar Planner\",\n                \"calendar_content\": \"Easily view and plan your posts by week or month. Drag and drop to reorder.\",\n                \"phone_title\": \"Phone Preview\",\n                \"phone_content\": \"See how your posts and feed will look to your followers on a mobile.\",\n                \"draftpost_title\": \"Draft Posts\",\n                \"draftpost_content\": \"Half way through creating your posts and need to walk away? Save them as draft.\",\n                \"post_left_image\": \"post_left_image_1559055468496_black-silver_business_card__large.jpg\",\n                \"calendar_right_image\": \"calendar_right_image_1559048978752_1.jpg\",\n                \"phone_left_image\": \"phone_left_image_1559049041941_man.png\",\n                \"draftpost_right_image\": \"draftpost_right_image_1559049041943_hotstar-app.jpg\"\n            },\n            \"slider_block\": {\n                \"left_title_1\": \"Upload Sources\",\n                \"left_content_1\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\n                \"left_title_2\": \"Image editor\",\n                \"left_content_2\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\n                \"left_title_3\": \"Post creator\",\n                \"left_content_3\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\n                \"left_title_4\": \"Automated posting\",\n                \"left_content_4\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\n                \"slider_files_one\": \"slider_files_one_1559055033627_Image_from_Skype1.jpg\",\n                \"slider_files_two\": \"slider_files_1559052308734_img5.jpg\",\n                \"slider_files_three\": \"slider_files_three_1559055868459_e325011770365ff5593d556755274ff4.jpg\",\n                \"slider_files_four\": \"slider_files_four_1559055868460_26750a3797416f6f1245b67c46d971c7.jpg\"\n            },\n            \"power_tool_block\": {\n                \"tool_heading\": \"Power Tools\",\n                \"tool_content\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\n                \"sub_title_1\": \"Multiple Accounts\",\n                \"sub_content_1\": \"Manage multiple Instagram accounts from a single scheduley-iit login.\",\n                \"sub_title_2\": \"Bulk Upload\",\n                \"sub_content_2\": \"Upload upto 50 posts in one go. Auto set times, save all to your queue or drafts.\",\n                \"sub_title_3\": \"Teams\",\n                \"sub_content_3\": \"Collaborate with your team members and set permission for each account.\",\n                \"sub_file_1\": \"sub_file_1_1559046405673_test6.png\",\n                \"sub_file_2\": \"sub_file_2_1559055868462_642d9a82a03f88453e3ee6433a05e5ec.jpg\",\n                \"sub_file_3\": \"sub_file_3_1559046405676_test3.jpg\"\n            },\n            \"try_block\": {\n                \"try_title\": \"Try Scheduley Free for 14 Days11\",\n                \"try_content\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas orci tortor, tincidunt et nisi at laoreet maximus nulla. Cras ac ligula vulputate, pretium metus sed, bibendum diam.11\",\n                \"try_button_text\": \"Get Started11\"\n            },\n            \"slug\": \"product-cms\",\n            \"status\": \"active\",\n            \"createdAt\": \"2019-05-27T11:40:57.467Z\"\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/cms.routes.js",
    "groupTitle": "CMS",
    "name": "GetFeature"
  },
  {
    "type": "get",
    "url": "/home",
    "title": "Home Page Data",
    "version": "1.0.0",
    "group": "CMS",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5ced4d27bffe823e6bb69da3\",\n            \"banner_block\": {\n                \"banner_title\": \"Banner Title\",\n                \"banner_image\": \"banner_image_1559134478822_banner.png\"\n            },\n            \"middle_content_block\": {\n                \"small_content\": \"Middle\",\n                \"post_content_header\": \"Post Content Header\",\n                \"post_content_text\": \"Post Content Text\",\n                \"post_content_right_image\": \"post_content_right_image_1559123809006_top-ten-logos.jpg\",\n                \"post_content_middle_header\": \"Post Content Middle Header\",\n                \"post_content_middle_text\": \"Post Content Middle Text\",\n                \"post_content_middle_image\": \"post_content_middle_image_1559134478827_video.jpg\",\n                \"post_content_lower_header\": \"Post Content Lower Heade\",\n                \"post_content_lower_text\": \"Post Content Lower Text\",\n                \"post_content_lower_feature\": [\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"post_content_lower_feature[2][image]_1568802763603_1200px-NASA_logo.svg.png\",\n                        \"header\": \"11111111111\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    },\n                    {\n                        \"image\": \"\",\n                        \"header\": \"11\",\n                        \"description\": \"dd\"\n                    }\n                ],\n                \"middle_content_logo\": [\n                    {\n                        \"logo\": \"middle_content_logo1_1559123808994_images_(1).jpeg\"\n                    },\n                    {\n                        \"logo\": \"middle_content_logo2_1559123808994_images_(1).png\"\n                    },\n                    {\n                        \"logo\": \"middle_content_logo3_1559123808995_images_(2).jpeg\"\n                    },\n                    {\n                        \"logo\": \"middle_content_logo4_1559123808995_images_(4).jpeg\"\n                    },\n                    {\n                        \"logo\": \"middle_content_logo5_1559123808996_images_(3).jpeg\"\n                    },\n                    {\n                        \"logo\": \"middle_content_logo6_1559123808996_logo.png\"\n                    }\n                ]\n            },\n            \"company_content_block\": {\n                \"header_title\": \"Company\",\n                \"header_description\": \"Company Des\",\n                \"post_content_middle_header\": \"middle com\",\n                \"post_content_middle_text\": \"middle com des\",\n                \"post_content_middle_image\": \"comapany_post_content_middle_image_1559134478836_phone.png\"\n            },\n            \"pricing_content_block\": {\n                \"header_title\": \"prcinind\",\n                \"header_text\": \"prcininddes\",\n                \"header_pricing_business_image\": \"pricing_header_image_1559134478839_month.png\",\n                \"post_content_middle_header\": \"Last Header\",\n                \"post_content_middle_text\": \"P H Text\"\n            },\n            \"status\": \"active\",\n            \"createdAt\": \"2019-05-28T11:40:57.467Z\",\n            \"casestudies\": [\n                {\n                    \"logo_image\": \"logo_image_1559132150617_dymmy2.jpg\",\n                    \"case_content\": \"Demo111\",\n                    \"button_text\": \"test11111111\",\n                    \"button_link\": \"https://www.google.co.in\",\n                    \"status\": \"active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-05-28T09:05:14.221Z\",\n                    \"_id\": \"5cee588b7c540c03da619320\",\n                    \"__v\": 0\n                },\n                {\n                    \"logo_image\": \"logo_image_1559140279790_avatar7.png\",\n                    \"case_content\": \"sample content\",\n                    \"button_text\": \"new button\",\n                    \"button_link\": \"https://www.google.com/\",\n                    \"status\": \"active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-05-29T14:30:14.742Z\",\n                    \"_id\": \"5cee97b7890f4014598cac3f\",\n                    \"__v\": 0\n                }\n            ]\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/cms.routes.js",
    "groupTitle": "CMS",
    "name": "GetHome"
  },
  {
    "type": "get",
    "url": "/company/getDetails",
    "title": "Company Get Details",
    "version": "1.0.0",
    "group": "Company",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"_id\": \"5d8b49b7a98d1f4c492d51f6\",\n        \"company_title\": \"Archit\",\n        \"user_id\": \"5d89fe1839da2f4d72ab455e\",\n        \"status\": \"active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-09-25T11:04:21.654Z\",\n        \"social_account_info\": [\n            {\n                \"social_id\": \"233\",\n                \"social_type\": \"facebook\",\n                \"username\": \"Archit\",\n                \"access_token\": \"1232\",\n                \"expiry_time\": \"1232\",\n                \"_id\": \"5d8b49b7a98d1f4c492d51f8\"\n            },\n            {\n                \"social_id\": \"233\",\n                \"social_type\": \"twitter\",\n                \"username\": \"Archit\",\n                \"access_token\": \"1232\",\n                \"expiry_time\": \"1232\",\n                \"_id\": \"5d8b49b7a98d1f4c492d51f7\"\n            }\n        ],\n        \"__v\": 0\n    },\n    \"message\": \"Company details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/company.routes.js",
    "groupTitle": "Company",
    "name": "GetCompanyGetdetails"
  },
  {
    "type": "post",
    "url": "/company/store",
    "title": "Company Save",
    "version": "1.0.0",
    "group": "Company",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "company_title",
            "description": "<p>Company Title</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "social_account_info[social_id]",
            "description": "<p>Social Account ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "social_account_info[social_type]",
            "description": "<p>Social Account Type</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "social_account_info[username]",
            "description": "<p>Social Account Username</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "social_account_info[access_token]",
            "description": "<p>Social Account Access Token</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "social_account_info[expiry_time]",
            "description": "<p>Access Token Expiry Time</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "           {\n\t\"company_title\": \"Archit\",\n\t\"social_account_info\": [\n\t\t{\"social_id\": \"233\", \"social_type\": \"facebook\", \"username\": \"Archit\", \"access_token\": \"1232\", \"expiry_time\":\"1232\"},\n\t\t{\"social_id\": \"233\", \"social_type\": \"twitter\", \"username\": \"Archit\", \"access_token\": \"1232\", \"expiry_time\":\"1232\"}\n\t]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"company_title\": \"Archit\",\n        \"user_id\": \"5d89fe1839da2f4d72ab455e\",\n        \"status\": \"active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-09-25T11:04:21.654Z\",\n        \"_id\": \"5d8b49b7a98d1f4c492d51f6\",\n        \"social_account_info\": [\n            {\n                \"social_id\": \"233\",\n                \"social_type\": \"facebook\",\n                \"username\": \"Archit\",\n                \"access_token\": \"1232\",\n                \"expiry_time\": \"1232\",\n                \"_id\": \"5d8b49b7a98d1f4c492d51f8\"\n            },\n            {\n                \"social_id\": \"233\",\n                \"social_type\": \"twitter\",\n                \"username\": \"Archit\",\n                \"access_token\": \"1232\",\n                \"expiry_time\": \"1232\",\n                \"_id\": \"5d8b49b7a98d1f4c492d51f7\"\n            }\n        ],\n        \"__v\": 0\n    },\n    \"message\": \"Company saved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/company.routes.js",
    "groupTitle": "Company",
    "name": "PostCompanyStore"
  },
  {
    "type": "post",
    "url": "/facebook/content",
    "title": "Share Content in Facebook",
    "version": "1.0.0",
    "group": "Facebook_Page",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_token",
            "description": "<p>User Facebook Page Access Token</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_id",
            "description": "<p>User Facebook Page Id</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "publish_time",
            "description": "<p>Post Schedule Time (format: 2019-08-13 21:00)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "message",
            "description": "<p>User's Content or Message</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"id\": \"101088261235363_112855926725263\"\n    },\n    \"message\": \"Thank You!! You have posted successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/facebook.routes.js",
    "groupTitle": "Facebook_Page",
    "name": "PostFacebookContent"
  },
  {
    "type": "post",
    "url": "/facebook/multipleImage",
    "title": "Share Multiple Image in Facebook",
    "version": "1.0.0",
    "group": "Facebook_Page",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "facebook_multiple_image",
            "description": "<p>Upload multiple images (supported type: jpeg/png/gif/tif/bmp)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_token",
            "description": "<p>User Facebook Page Access Token</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_id",
            "description": "<p>User's Facebook Page Id</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "publish_time",
            "description": "<p>Post Schedule Time (format: 2019-08-13 21:00)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "message",
            "description": "<p>User's Content or Message or caption</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"id\": \"101088261235363_116493396361516\",\n        \"post_supports_client_mutation_id\": true\n    },\n    \"message\": \"Thank You!! Multiple images posted successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/facebook.routes.js",
    "groupTitle": "Facebook_Page",
    "name": "PostFacebookMultipleimage"
  },
  {
    "type": "post",
    "url": "/facebook/schedule",
    "title": "Schedule posts in Facebook",
    "version": "1.0.0",
    "group": "Facebook_Page",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "post",
            "description": "<p>Upload post</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "publish_time",
            "description": "<p>Post Schedule Time</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "post_content",
            "description": "<p>User's Content or Message or caption</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"success\": true\n    },\n    \"message\": \"Media schedulled successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/facebook.routes.js",
    "groupTitle": "Facebook_Page",
    "name": "PostFacebookSchedule"
  },
  {
    "type": "post",
    "url": "/facebook/singleImage",
    "title": "Share Single Image in Facebook",
    "version": "1.0.0",
    "group": "Facebook_Page",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "fb_single_image",
            "description": "<p>Upload an Image (supported type: jpeg/png/gif/tif/bmp)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_token",
            "description": "<p>User Facebook Page Access Token</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page_id",
            "description": "<p>User Facebook Page Id</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "publish_time",
            "description": "<p>Post Schedule Time (format: 2019-08-13 21:00)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "message",
            "description": "<p>User's Content or Message or caption</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"id\": \"101088261235363_112855926725263\"\n    },\n    \"message\": \"Thank You!! You have posted successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/facebook.routes.js",
    "groupTitle": "Facebook_Page",
    "name": "PostFacebookSingleimage"
  },
  {
    "type": "post",
    "url": "/social_feeds/addsocialvalue",
    "title": "Add Social media id,token",
    "version": "1.0.0",
    "group": "Facebook_Page",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "social",
            "description": "<p>social[0][provider]:twitter social[0][token]:5151165165151 social[0][social_id]:456123 social[1][provider]:facebook social[1][token]:fb686868 social[1][social_id]:cxsVxcsh556567vxax56</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"message\": \"Data Stored Successfully\",\n    \"data\": {\n        \"user_id\": \"5d836bb03432c03bff26fa82\",\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"_id\": \"5d84a88e6f66026db88adca2\",\n        \"social\": [\n            {\n                \"provider\": \"twitter\",\n                \"token\": \"5151165165151\",\n                \"social_id\": \"456123\",\n                \"_id\": \"5d84a88e6f66026db88adca4\"\n            },\n            {\n                \"provider\": \"facebook\",\n                \"token\": \"fb686868\",\n                \"social_id\": \"cxsVxcsh556567vxax56\",\n                \"_id\": \"5d84a88e6f66026db88adca3\"\n            }\n        ],\n        \"createdAt\": \"2019-09-20T10:23:10.024Z\",\n        \"updatedAt\": \"2019-09-20T10:23:10.024Z\",\n        \"__v\": 0\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/social_feed.routes.js",
    "groupTitle": "Facebook_Page",
    "name": "PostSocial_feedsAddsocialvalue"
  },
  {
    "type": "post",
    "url": "/pinterest/schedule",
    "title": "Schedule posts for Pinterest",
    "version": "1.0.0",
    "group": "Pinterest",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "post",
            "description": "<p>Upload post</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "date",
            "description": "<p>Post Schedule Time</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "post_content",
            "description": "<p>User's Content or Message or caption</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"success\": true\n    },\n    \"message\": \"Media schedulled successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/pinterest.routes.js",
    "groupTitle": "Pinterest",
    "name": "PostPinterestSchedule"
  },
  {
    "type": "get",
    "url": "/plan",
    "title": "Plan List",
    "version": "1.0.0",
    "group": "Plan",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d88728f9a0bc348890b908e\",\n            \"stripe_plan_id\": \"plan_FrUrUFM12CziKC\",\n            \"title\": \"Silver\",\n            \"slug\": \"silver-plus\",\n            \"price\": 19,\n            \"content\": \"<ul class=\\\"plan-card__features__list\\\"><li class=\\\"plan-card__features__list__item\\\">Automated post scheduling</li><li class=\\\"plan-card__features__list__item\\\">Key performance metrics</li><li class=\\\"plan-card__features__list__item\\\">Ad spend limit of <span class=\\\"_boostSpend\\\">19</span> per month to boost posts        </li></ul>\",\n            \"image\": \"image_1569223310822_f1.jpg\",\n            \"status\": \"Active\",\n            \"inactiveDate\": null,\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-23T07:21:51.761Z\",\n            \"updatedAt\": \"2019-09-23T07:28:19.449Z\"\n        },\n        {\n            \"_id\": \"5d8873349a0bc348890b908f\",\n            \"stripe_plan_id\": \"plan_FrUunNhrIqxRmb\",\n            \"title\": \"Gold\",\n            \"slug\": \"gold-plus\",\n            \"price\": 49,\n            \"content\": \"<ul class=\\\"plan-card__features__list\\\"><li class=\\\"plan-card__features__list__item plan-card__features__list__item--included-features\\\">Everything included in Professional plan</li><li class=\\\"plan-card__features__list__item\\\">Team message assignments</li><li class=\\\"plan-card__features__list__item\\\">Custom analytics</li><li class=\\\"plan-card__features__list__item\\\">Exportable reports</li><li class=\\\"plan-card__features__list__item\\\">Ad spend limit of 49 per month to boost posts            </li></ul>\",\n            \"image\": \"image_1569223475403_refresh_update.png\",\n            \"status\": \"Active\",\n            \"inactiveDate\": null,\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-23T07:24:36.387Z\",\n            \"updatedAt\": \"2019-09-23T07:28:28.247Z\"\n        },\n        {\n            \"_id\": \"5d8873999a0bc348890b9090\",\n            \"stripe_plan_id\": \"plan_FrUvO0Pj2knh2A\",\n            \"title\": \"Platinum\",\n            \"slug\": \"platinum-plus\",\n            \"price\": 99,\n            \"content\": \"<ul class=\\\"plan-card__features__list\\\"><li class=\\\"plan-card__features__list__item plan-card__features__list__item--included-features\\\">Everything included in Team plan</li><li class=\\\"plan-card__features__list__item\\\">Flexible approval workflows</li><li class=\\\"plan-card__features__list__item\\\">1 hour of 1-on-1 onboarding</li><li class=\\\"plan-card__features__list__item\\\">5 social media certifications</li><li class=\\\"plan-card__features__list__item\\\">24/7 support</li><li class=\\\"plan-card__features__list__item\\\">Ad spend limit of 99 per month to boost posts</li></ul>\",\n            \"image\": \"image_1569223577086_test3.jpg\",\n            \"status\": \"Active\",\n            \"inactiveDate\": null,\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-23T07:26:17.844Z\",\n            \"updatedAt\": \"2019-09-23T07:28:36.970Z\"\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlan"
  },
  {
    "type": "get",
    "url": "/plan",
    "title": "Plan Details",
    "version": "1.0.0",
    "group": "Plan",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n \n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/testing.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlan"
  },
  {
    "type": "get",
    "url": "/plan/currentSubscription",
    "title": "Current Subscription",
    "version": "1.0.0",
    "group": "Plan",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"name\": \"Archit Singh\",\n        \"suscription_id\": \"sub_Fq060yU9FOb2Bn\",\n        \"amount\": 19\n    },\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlanCurrentsubscription"
  },
  {
    "type": "get",
    "url": "/plan/downgradeSubscription",
    "title": "Downgrade Subscription List",
    "version": "1.0.0",
    "group": "Plan",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlanDowngradesubscription"
  },
  {
    "type": "get",
    "url": "/plan/upgradeSubscription",
    "title": "Upgrade Subscription List",
    "version": "1.0.0",
    "group": "Plan",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5cd2ac2f43798264eaaf87ac\",\n            \"title\": \"Gold\",\n            \"slug\": \"gold\",\n            \"price\": 49,\n            \"content\": \"<div class=\\\"plan-card__features\\\"><ul class=\\\"plan-card__features__list\\\"><li class=\\\"plan-card__features__list__item plan-card__features__list__item--included-features\\\">Everything included in Professional plan\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Team message assignments\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Custom analytics\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Exportable reports\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Ad spend limit of 49&nbsp;per month to boost posts&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li></ul></div>\",\n            \"image\": \"image_1558104681678_dummy.png\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-05-08T10:15:11.508Z\",\n            \"updatedAt\": \"2019-09-17T11:11:54.112Z\",\n            \"__v\": 0\n        },\n        {\n            \"_id\": \"5cd2b7f37c1b52730dc308af\",\n            \"title\": \"Platinum\",\n            \"slug\": \"platinum\",\n            \"price\": 99,\n            \"content\": \"<div class=\\\"plan-card__features\\\"><ul class=\\\"plan-card__features__list\\\"><li class=\\\"plan-card__features__list__item plan-card__features__list__item--included-features\\\">Everything included in Team plan\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Flexible approval workflows\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    1 hour of 1-on-1 onboarding\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    5 social media certifications\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    24/7 support\\r\\n                                                </li><li class=\\\"plan-card__features__list__item\\\">\\r\\n                                                    Ad spend limit of 99&nbsp;per month to boost posts&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li></ul></div>\",\n            \"image\": \"image_1557313523218_f4.jpg\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-05-08T11:05:23.240Z\",\n            \"updatedAt\": \"2019-09-17T11:12:56.868Z\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlanUpgradesubscription"
  },
  {
    "type": "post",
    "url": "/plan/updateSubscription",
    "title": "Update Subscription",
    "version": "1.0.0",
    "group": "Plan",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "plan",
            "description": "<p>Plan ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Data Fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "PostPlanUpdatesubscription"
  },
  {
    "type": "get",
    "url": "/setting",
    "title": "Get Setting",
    "version": "1.0.0",
    "group": "Setting",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"Contact_Phone\": \"(928) 000-5555\",\n        \"Site_Title\": \"Be a Shark\",\n        \"Site_Logo\": \"site_logo_1555324594326_logo.png\",\n        \"Stripe_Secret_Key\": \"sk_test_XXDuAFknVN4W9lXXXXXXX12\",\n        \"Company_Sign_Up_Price\": \"500\",\n        \"Investment_Price\": \"500\",\n        \"Service_Price\": \"100\"\n    },\n    \"message\": \"Setting fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/setting.routes.js",
    "groupTitle": "Setting",
    "name": "GetSetting"
  },
  {
    "type": "get",
    "url": "/team/list/",
    "title": "Get Team Members",
    "version": "1.0.0",
    "group": "TeamMembers",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5cecf9cabb7b742baaec2c87\",\n            \"firstName\": \"Test\",\n            \"lastName\": \"Demo\",\n            \"image\": \"image_1559034314203_featured-small-circular.jpg\",\n            \"status\": \"Active\",\n            \"designation\": \"CEO\",\n            \"facebook\": \"www\",\n            \"instagram\": \"Test Demo\",\n            \"email\": \"test@demo.com\",\n            \"isDeleted\": false\n        }\n    ],\n    \"current\": 1,\n    \"pages\": 1,\n    \"message\": \"team list fetched successfully\",\n    \"search_param\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/team.routes.js",
    "groupTitle": "TeamMembers",
    "name": "GetTeamList"
  },
  {
    "type": "post",
    "url": "/team/sendInvitationLink/",
    "title": "Send Invitation Link",
    "version": "1.0.0",
    "group": "Team",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Invitation Link Sent.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/team_member.routes.js",
    "groupTitle": "Team",
    "name": "PostTeamSendinvitationlink"
  },
  {
    "type": "post",
    "url": "/team/storeTeamMember/",
    "title": "Store Team Members",
    "version": "1.0.0",
    "group": "Team",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "firstName",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "lastName",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Email Address</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "token",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"facebook\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"facebook_id\": \"\",\n            \"page_id\": \"\",\n            \"page_name\": \"\",\n            \"page_access_token\": \"\"\n        },\n        \"twitter\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"twitter_id\": \"\"\n        },\n        \"pinterest\": {\n            \"first_name\": \"\",\n            \"last_name\": \"\",\n            \"pinterest_id\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"board_name\": \"\",\n            \"board_url\": \"\",\n            \"board_id\": \"\"\n        },\n        \"instagram\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"instagram_id\": \"\"\n        },\n        \"firstName\": \"Abc\",\n        \"lastName\": \"xyz\",\n        \"email\": \"architkumar.singh@webskitters.com\",\n        \"password\": \"$2a$08$Nu2gfS1eFEwIo2v92L9oZ.SRl69Vhx71K/T8YGPcg1Kbax7Nz/upy\",\n        \"contactNumber\": \"\",\n        \"role\": [],\n        \"regType\": \"normal\",\n        \"social_id\": \"\",\n        \"parent_id\": \"5d89fe1839da2f4d72ab455e\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"token\": \"071825f2bad34367cfe000388be8aafd3859dd4503fc356e225591ce23e8d8831014641c0c689da7\",\n        \"stripe_subscription_id\": \"\",\n        \"stripe_customer_id\": \"\",\n        \"stripe_plan_id\": \"\",\n        \"isSubscribed\": false,\n        \"isDeleted\": false,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d8dd15b490f6c30d7a10560\",\n        \"createdAt\": \"2019-09-27T09:07:39.702Z\",\n        \"updatedAt\": \"2019-09-27T09:07:39.702Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Invitation Link Sent.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/team_member.routes.js",
    "groupTitle": "Team",
    "name": "PostTeamStoreteammember"
  },
  {
    "type": "GET",
    "url": "/users/myprofile",
    "title": "User's Profile",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"_id\": \"5c9ce2a1a2737d5dd6c6fadb\",\n        \"firstName\": \"Jerald\",\n        \"lastName\": \"Laury\",\n        \"email\": \"jerald.laury@gmail.com\",\n        \"password\": \"$2a$08$txVW6ny3IhFp7hx6f593Le804.aDW1SV8oa1vnPV/rNmHxzBeJiQy\",\n        \"contactNumber\": \"1234567890\",\n        \"role\": [\n            {\n                \"_id\": \"5c94f263dc38e258151168bb\",\n                \"role\": \"users\",\n                \"desc\": \"<p>This is the Frontend user here.</p>\\r\\n\",\n                \"roleDisplayName\": \"Users\",\n                \"roleType\": \"frontend\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"otp_number\": 2234,\n        \"profile_image\": \"\",\n        \"deviceToken\": \"ghdfkjhg89756ghkj\",\n        \"deviceType\": \"android\",\n        \"token\": \"\",\n        \"isDeleted\": false,\n        \"status\": \"Active\",\n        \"profileVerified\": true,\n        \"emailVerified\": true,\n        \"mobileVerified\": false,\n        \"paid_financial_service\": \"no\",\n        \"visit\": 0,\n        \"createdAt\": \"2019-03-28T15:05:05.607Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Profile Info fetched Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "GetUsersMyprofile"
  },
  {
    "type": "post",
    "url": "/user/checkExpiryOfToken",
    "title": "Check Expiry of Token",
    "version": "1.0.0",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"message\": \"Link active\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUserCheckexpiryoftoken"
  },
  {
    "type": "post",
    "url": "/user/create",
    "title": "Create a user",
    "version": "1.0.0",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "firstName",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "lastName",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Email Address</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "card_number",
            "description": "<p>Card Number</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "exp_month",
            "description": "<p>Expiry Month</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "exp_year",
            "description": "<p>Expiry Year</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "cvc",
            "description": "<p>CVC</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "plan",
            "description": "<p>Plan ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "social_id",
            "description": "<p>Social Media ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "regType",
            "description": "<p>[normal/facebook/google/pinterest/twitter/instagram]</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"_id\": \"5d82153b6eb0a645aeae945b\",\n        \"firstName\": \"Archit\",\n        \"lastName\": \"Singh\",\n        \"email\": \"architkumar.singh@webskitters.com\",\n        \"stripe_subscription_id\": \"sub_FpgjJkhOyxSUr9\",\n        \"stripe_plan_id\": \"plan_FpgjK3Dr639sKp\",\n        \"stripe_customer_id\": \"cus_FpgjgXQiPfb41C\"\n    },\n    \"message\": \"Profile Created Suceessfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUserCreate"
  },
  {
    "type": "post",
    "url": "/user/forgotpassword",
    "title": "Forgot Password",
    "version": "1.0.0",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>User's account Email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Password Reset Link Sent to your mail.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUserForgotpassword"
  },
  {
    "type": "post",
    "url": "/user/resetPassword",
    "title": "Reset Password",
    "version": "1.0.0",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>New Password</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"message\": \"Password Changed Successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUserResetpassword"
  },
  {
    "type": "post",
    "url": "/user/signin",
    "title": "Signin a user",
    "version": "1.0.0",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>User's account Email</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>User's account Password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "social_id",
            "description": "<p>Social media id</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "regType",
            "description": "<p>[normal/facebook/google/pinterest/twitter/instagram]</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkODIxNTNiNmViMGE2NDVhZWFlOTQ1YiIsImlhdCI6MTU2ODgwNzAxNiwiZXhwIjoxNTY4ODkzNDE2fQ.Nv9rJw2lQ1uaVEhGKo0huPZpcJgYYrhHqlGHiCphjEw\",\n    \"data\": {\n        \"firstName\": \"Archit\",\n        \"lastName\": \"Singh\",\n        \"email\": \"architkumar.singh@webskitters.com\",\n        \"password\": \"$2a$08$QrhBpM2XtP6Y5BOrEQLGCu/G9xSR0NSoDb9ZNjO6gHVBtTDogu536\",\n        \"contactNumber\": \"123456\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the User role here.</p>\\r\\n\",\n                \"roleType\": \"admin\",\n                \"_id\": \"5cc302f59c4c3ac9c4937abf\",\n                \"role\": \"user\",\n                \"roleDisplayName\": \"User\",\n                \"id\": \"5cc302f59c4c3ac9c4937abf\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"stripe_subscription_id\": \"sub_FpgjJkhOyxSUr9\",\n        \"stripe_customer_id\": \"cus_FpgjgXQiPfb41C\",\n        \"stripe_plan_id\": \"plan_FpgjK3Dr639sKp\",\n        \"isSubscribed\": true,\n        \"isDeleted\": false,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d82153b6eb0a645aeae945b\",\n        \"plan\": \"5cd1ad8f61841b4402f029cd\",\n        \"createdAt\": \"2019-09-18T11:30:03.675Z\",\n        \"updatedAt\": \"2019-09-18T11:30:03.675Z\",\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully logged in\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUserSignin"
  },
  {
    "type": "post",
    "url": "/users/changeNameOrEmail",
    "title": "Change User's Name Or Email",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "firstName",
            "description": "<p>[optional] new firstName</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "lastName",
            "description": "<p>[optional] new lastName</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>[optional] new email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"firstName\": \"123\",\n        \"lastName\": \"321\",\n        \"email\": \"4444\",\n        \"password\": \"$2a$08$ct2e.kexkgdh.BRK24YwG.C0U1Io6fXHWb0gK/c9adQPDRoLeSmRu\",\n        \"contactNumber\": \"\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the User role here.</p>\\r\\n\",\n                \"roleType\": \"admin\",\n                \"_id\": \"5cc302f59c4c3ac9c4937abf\",\n                \"role\": \"user\",\n                \"roleDisplayName\": \"User\",\n                \"id\": \"5cc302f59c4c3ac9c4937abf\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"token\": \"\",\n        \"stripe_subscription_id\": \"sub_Fpirif3OXEVecX\",\n        \"stripe_customer_id\": \"cus_FpirlSi9D4NuWW\",\n        \"stripe_plan_id\": \"plan_Fpir1bsm0Oldt2\",\n        \"isSubscribed\": true,\n        \"isDeleted\": false,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d82342b37d5515ee81a1b05\",\n        \"plan\": \"5cd1ad8f61841b4402f029cd\",\n        \"createdAt\": \"2019-09-18T13:42:03.644Z\",\n        \"updatedAt\": \"2019-09-18T14:57:56.902Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Email updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersChangenameoremail"
  },
  {
    "type": "post",
    "url": "/users/changepassword",
    "title": "Change User's Password",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "currentPassword",
            "description": "<p>user's Current password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "newPassword",
            "description": "<p>new password</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Password updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersChangepassword"
  },
  {
    "type": "post",
    "url": "/users/deleteaccount",
    "title": "Delete User Account",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"firstName\": \"mrittika\",\n        \"lastName\": \"basu\",\n        \"email\": \"mrittika.basu@webskitters.com\",\n        \"password\": \"$2a$08$4Bpp4Yli3WxG1Rc3n3CSDuzldrMLMfZNFQBAqqvKhv/yqxdWmRc.m\",\n        \"contactNumber\": \"\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the User role here.</p>\\r\\n\",\n                \"roleType\": \"admin\",\n                \"_id\": \"5cc302f59c4c3ac9c4937abf\",\n                \"role\": \"user\",\n                \"roleDisplayName\": \"User\",\n                \"rolepermission\": null,\n                \"user_data\": null,\n                \"id\": \"5cc302f59c4c3ac9c4937abf\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"social_id\": \"\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"token\": \"\",\n        \"stripe_subscription_id\": \"\",\n        \"stripe_customer_id\": \"\",\n        \"stripe_plan_id\": \"plan_FrUunNhrIqxRmb\",\n        \"isSubscribed\": false,\n        \"isDeleted\": true,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d8897869084235e01c36830\",\n        \"plan\": \"5d8873349a0bc348890b908f\",\n        \"createdAt\": \"2019-09-23T09:59:34.646Z\",\n        \"updatedAt\": \"2019-09-23T14:52:16.051Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Your account is deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersDeleteaccount"
  },
  {
    "type": "post",
    "url": "/users/unsubscribe",
    "title": "Cancel Subscription",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"firstName\": \"mrittika\",\n        \"lastName\": \"basu\",\n        \"email\": \"mrittika.basu@webskitters.com\",\n        \"password\": \"$2a$08$4Bpp4Yli3WxG1Rc3n3CSDuzldrMLMfZNFQBAqqvKhv/yqxdWmRc.m\",\n        \"contactNumber\": \"\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the User role here.</p>\\r\\n\",\n                \"roleType\": \"admin\",\n                \"_id\": \"5cc302f59c4c3ac9c4937abf\",\n                \"role\": \"user\",\n                \"roleDisplayName\": \"User\",\n                \"rolepermission\": null,\n                \"user_data\": null,\n                \"id\": \"5cc302f59c4c3ac9c4937abf\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"social_id\": \"\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"token\": \"\",\n        \"stripe_subscription_id\": \"\",\n        \"stripe_customer_id\": \"\",\n        \"stripe_plan_id\": \"plan_FrUunNhrIqxRmb\",\n        \"isSubscribed\": false,\n        \"isDeleted\": false,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d8897869084235e01c36830\",\n        \"plan\": \"5d8873349a0bc348890b908f\",\n        \"createdAt\": \"2019-09-23T09:59:34.646Z\",\n        \"updatedAt\": \"2019-09-23T14:52:16.051Z\",\n        \"__v\": 0\n    },\n    \"message\": \"User unsubscribed\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersUnsubscribe"
  },
  {
    "type": "post",
    "url": "/users/updateprofile",
    "title": "User's Profile Update",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "firstName",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "lastName",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Email Address (required)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "contactNumber",
            "description": "<p>Contact Number</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "date_of_birth",
            "description": "<p>Date of Birth (yyyy-mm-dd)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender 'male' or 'female'</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "net_worth",
            "description": "<p>Net Worth</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "social_security_number",
            "description": "<p>Social Security Number</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"firstName\": \"Jerald\",\n        \"lastName\": \"Laury\",\n        \"email\": \"jerald.laury@gmail.com\",\n        \"password\": \"$2a$08$txVW6ny3IhFp7hx6f593Le804.aDW1SV8oa1vnPV/rNmHxzBeJiQy\",\n        \"contactNumber\": \"1234567890\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the Frontend user here.</p>\\r\\n\",\n                \"roleType\": \"frontend\",\n                \"_id\": \"5c94f263dc38e258151168bb\",\n                \"role\": \"users\",\n                \"roleDisplayName\": \"Users\",\n                \"rolepermission\": null,\n                \"user_data\": null,\n                \"id\": \"5c94f263dc38e258151168bb\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"otp_number\": 2234,\n        \"gender\": \"\",\n        \"address\": \"\",\n        \"latitude\": \"\",\n        \"longitude\": \"\",\n        \"net_worth\": 0,\n        \"social_security_number\": \"\",\n        \"profile_image\": \"\",\n        \"deviceToken\": \"ghdfkjhg89756ghkj\",\n        \"deviceType\": \"android\",\n        \"token\": \"\",\n        \"isDeleted\": false,\n        \"status\": \"Active\",\n        \"profileVerified\": true,\n        \"emailVerified\": true,\n        \"mobileVerified\": false,\n        \"paid_financial_service\": \"no\",\n        \"visit\": 0,\n        \"_id\": \"5c9ce2a1a2737d5dd6c6fadb\",\n        \"createdAt\": \"2019-03-28T15:05:05.607Z\",\n        \"__v\": 0,\n        \"date_of_birth\": \"2019-03-29T10:59:21.583Z\"\n    },\n    \"message\": \"Profile Info updated Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersUpdateprofile"
  },
  {
    "type": "post",
    "url": "/users/updateProfileUser",
    "title": "User's Profile Update Social",
    "version": "1.0.0",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>Social Media Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "access_token",
            "description": "<p>Social Media Access Token</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "expiry_time",
            "description": "<p>Access Token Expiry Time</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "twitter_id",
            "description": "<p>Social Media Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page_id",
            "description": "<p>Page ID (only for facebook)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page_name",
            "description": "<p>Page Name (only for facebook)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page_access_token",
            "description": "<p>Page Access Token (only for facebook)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>First Name (only for pinterest)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last Name (only for pinterest)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pinterest_id",
            "description": "<p>User ID (only for pinterest)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "board_name",
            "description": "<p>Board Name (only for pinterest)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "board_url",
            "description": "<p>Board URL (only for pinterest)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "board_id",
            "description": "<p>Board ID (only for pinterest)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"facebook\": {\"name\":\"Archit\", \"facebook_id\":28, \"expiry_time\": \"12334\", \"access_token\": \"12356\", \"page_id\": \"123\", \"page_name\": \"ABC\", \"page_access_token\": \"3456w\"}\n    \"twitter\": {\"name\":\"Archit\", \"twitter_id\":28, \"expiry_time\": \"12334\", \"access_token\": \"12356\"}\n    \"pinterest\": {\"first_name\":\"Archit\", \"last_name\":\"Archit\", \"pinterest_id\":28, \"board_name\": \"12334\", \"access_token\": \"12356\", \"board_url\": \"231\", \"board_id\": \"123\", \"expiry_time\": \"1213\"}\n    \"instagram\": {\"name\":\"Archit\", \"instagram_id\":28, \"expiry_time\": \"12334\", \"access_token\": \"12356\"}\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"facebook\": {\n            \"name\": \"Archit\",\n            \"access_token\": \"12356\",\n            \"expiry_time\": \"12334\",\n            \"facebook_id\": \"28\"\n        },\n        \"twitter\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"twitter_id\": \"\"\n        },\n        \"pinterest\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"pinterest_id\": \"\"\n        },\n        \"instagram\": {\n            \"name\": \"\",\n            \"access_token\": \"\",\n            \"expiry_time\": \"\",\n            \"instagram_id\": \"\"\n        },\n        \"firstName\": \"Navin\",\n        \"lastName\": \"Vishwakarma\",\n        \"email\": \"navin.vishwakarma@webskitters.com\",\n        \"password\": \"$2a$08$Ua6YuwGVtufc4wNBlKrj/OOR68fKhE6yHibICIRymtTmvVepEEObK\",\n        \"contactNumber\": \"\",\n        \"role\": [\n            {\n                \"desc\": \"<p>This is the User role here.</p>\\r\\n\",\n                \"roleType\": \"admin\",\n                \"_id\": \"5cc302f59c4c3ac9c4937abf\",\n                \"role\": \"user\",\n                \"roleDisplayName\": \"User\",\n                \"id\": \"5cc302f59c4c3ac9c4937abf\"\n            }\n        ],\n        \"regType\": \"normal\",\n        \"social_id\": \"\",\n        \"promo_code\": null,\n        \"profile_image\": \"\",\n        \"no_of_account_access\": 0,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"token\": \"\",\n        \"stripe_subscription_id\": \"sub_Frw8z6EzVIWOXo\",\n        \"stripe_customer_id\": \"cus_Frw5b9yPbcFkta\",\n        \"stripe_plan_id\": \"plan_FrUunNhrIqxRmb\",\n        \"isSubscribed\": false,\n        \"isDeleted\": false,\n        \"paymentStatus\": false,\n        \"initialPaymentStatus\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d89fe1839da2f4d72ab455e\",\n        \"plan\": \"5d8873349a0bc348890b908f\",\n        \"createdAt\": \"2019-09-24T11:29:28.693Z\",\n        \"updatedAt\": \"2019-09-25T08:45:13.443Z\",\n        \"__v\": 0\n    },\n    \"message\": \"User updated Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "Users",
    "name": "PostUsersUpdateprofileuser"
  },
  {
    "type": "post",
    "url": "/visit/save",
    "title": "Save visit count",
    "version": "1.0.0",
    "group": "visit",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Country name</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"count\": 2,\n        \"country\": \"usa\",\n        \"isDeleted\": false,\n        \"status\": true,\n        \"_id\": \"5d234423c49ee96ba1d81739\",\n        \"createdAt\": \"2019-07-08T13:24:51.736Z\",\n        \"updatedAt\": \"2019-07-08T13:25:00.372Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Saved Successfully!!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/visit.routes.js",
    "groupTitle": "visit",
    "name": "PostVisitSave"
  }
] });
