const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
// const multer = require('multer');
// const request_param = multer();

const companyController = require('company_management/controllers/company.api.controller');

namedRouter.all('/company*', auth.authenticateAPI);

/**
 * @api {post} /company/store Company Save
 * @apiVersion 1.0.0
 * @apiGroup Company
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} company_title Company Title
 * @apiParam {string} social_account_info[social_id] Social Account ID
 * @apiParam {string} social_account_info[social_type] Social Account Type
 * @apiParam {string} social_account_info[username] Social Account Username
 * @apiParam {string} social_account_info[access_token] Social Account Access Token
 * @apiParam {string} social_account_info[expiry_time] Access Token Expiry Time
 * @apiParamExample {json} Request-Example:
           {
	"company_title": "Archit",
	"social_account_info": [
		{"social_id": "233", "social_type": "facebook", "username": "Archit", "access_token": "1232", "expiry_time":"1232"},
		{"social_id": "233", "social_type": "twitter", "username": "Archit", "access_token": "1232", "expiry_time":"1232"}
	]
}
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "company_title": "Archit",
        "user_id": "5d89fe1839da2f4d72ab455e",
        "status": "active",
        "isDeleted": false,
        "createdAt": "2019-09-25T11:04:21.654Z",
        "_id": "5d8b49b7a98d1f4c492d51f6",
        "social_account_info": [
            {
                "social_id": "233",
                "social_type": "facebook",
                "username": "Archit",
                "access_token": "1232",
                "expiry_time": "1232",
                "_id": "5d8b49b7a98d1f4c492d51f8"
            },
            {
                "social_id": "233",
                "social_type": "twitter",
                "username": "Archit",
                "access_token": "1232",
                "expiry_time": "1232",
                "_id": "5d8b49b7a98d1f4c492d51f7"
            }
        ],
        "__v": 0
    },
    "message": "Company saved successfully"
}
*/
namedRouter.post("api.company.store", '/company/store', function (req, res) {
    companyController.save(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {get} /company/getDetails Company Get Details
 * @apiVersion 1.0.0
 * @apiGroup Company
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d8b49b7a98d1f4c492d51f6",
        "company_title": "Archit",
        "user_id": "5d89fe1839da2f4d72ab455e",
        "status": "active",
        "isDeleted": false,
        "createdAt": "2019-09-25T11:04:21.654Z",
        "social_account_info": [
            {
                "social_id": "233",
                "social_type": "facebook",
                "username": "Archit",
                "access_token": "1232",
                "expiry_time": "1232",
                "_id": "5d8b49b7a98d1f4c492d51f8"
            },
            {
                "social_id": "233",
                "social_type": "twitter",
                "username": "Archit",
                "access_token": "1232",
                "expiry_time": "1232",
                "_id": "5d8b49b7a98d1f4c492d51f7"
            }
        ],
        "__v": 0
    },
    "message": "Company details fetched successfully"
}
*/
namedRouter.get("api.company.getDetails", '/company/getDetails', function (req, res) {
    companyController.getDetails(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});


module.exports = router;