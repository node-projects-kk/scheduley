const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
var OAuth2 = require('oauth').OAuth2;
var multer = require('multer');
const fs = require('fs');

var facebookController = require('webservice/facebook.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, cb) {
        /* if ((file.fieldname).indexOf('fb_single_image') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/social_post/facebook/image/')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/social_post/facebook/image/');

            }
            cb(null, './public/uploads/social_post/facebook/image/')
        } else if ((file.fieldname).indexOf('fb_multiple_image') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/social_post/facebook/image/')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/social_post/facebook/image/');

            }
            cb(null, './public/uploads/social_post/facebook/image/')
        }
         */
        if (!fs.existsSync('./public/uploads/social_post/facebook/')) {
            // if not create directory
            fs.mkdirSync('./public/uploads/social_post/facebook/');

        }
        cb(null, './public/uploads/social_post/facebook/')
    },
    filename: function (req, file, cb) {
       // console.log("39", file.originalname)
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})

const uploadFile = multer({
    storage: Storage,
   /*  fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/tif' && file.mimetype !== 'image/png' && file.mimetype !== 'image/gif' && file.mimetype !== 'image/bmp') {
            req.fileValidationError = 'Only support jpeg, png, tif, gif, bmp file types.';
            return cb(null, false, new Error('Only support jpeg, png, tif, gif, bmp file types.'));
        }
        cb(null, true);
    } */
});

const request_param = multer({
    storage: Storage
});


var oauth2 = new OAuth2("2478028775765865", "d54c4ebcbbc14dcaa495ea42f19e94d7", "", "https://www.facebook.com/dialog/oauth", "https://graph.facebook.com/oauth/access_token", null);

namedRouter.get('/verify/auth2', function (req, res) {
    var redirect_uri = "https://scheduley.dedicateddevelopers.us/api/facebook/callback";
    var params = {
        'redirect_uri': redirect_uri,
        'scope': 'email'
    };
    res.redirect(oauth2.getAuthorizeUrl(params));
    // let kk = res.redirect(oauth2.getAuthorizeUrl(params));

    // console.log("<><> 13 ==> Route <><>", kk)
});

/**
 * @api {post} /facebook/content Share Content in Facebook
 * @apiVersion 1.0.0
 * @apiGroup Facebook Page
 * @apiParam page_token User Facebook Page Access Token
 * @apiParam page_id User Facebook Page Id
 * @apiParam publish_time Post Schedule Time (format: 2019-08-13 21:00)
 * @apiParam message User's Content or Message
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "id": "101088261235363_112855926725263"
    },
    "message": "Thank You!! You have posted successfully."
}
*/
namedRouter.post('facebook.content', '/facebook/content', request_param.any(), async (req, res) => {
    try {
        const success = await facebookController.FacebookContentShare(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /facebook/singleImage Share Single Image in Facebook
 * @apiVersion 1.0.0
 * @apiGroup Facebook Page
 * @apiParam fb_single_image Upload an Image (supported type: jpeg/png/gif/tif/bmp)
 * @apiParam page_token User Facebook Page Access Token
 * @apiParam page_id User Facebook Page Id
 * @apiParam publish_time Post Schedule Time (format: 2019-08-13 21:00)
 * @apiParam message User's Content or Message or caption
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "id": "101088261235363_112855926725263"
    },
    "message": "Thank You!! You have posted successfully."
}
*/
namedRouter.post('facebook.image', '/facebook/singleImage', uploadFile.any(), async (req, res) => {
    try {
        const success = await facebookController.FacebookSingleImageShare(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /facebook/multipleImage Share Multiple Image in Facebook
 * @apiVersion 1.0.0
 * @apiGroup Facebook Page
 * @apiParam facebook_multiple_image Upload multiple images (supported type: jpeg/png/gif/tif/bmp)
 * @apiParam page_token User Facebook Page Access Token
 * @apiParam page_id User's Facebook Page Id
 * @apiParam publish_time Post Schedule Time (format: 2019-08-13 21:00)
 * @apiParam message User's Content or Message or caption
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "id": "101088261235363_116493396361516",
        "post_supports_client_mutation_id": true
    },
    "message": "Thank You!! Multiple images posted successfully."
}
*/
namedRouter.post('facebook.mimage', '/facebook/multipleImage', uploadFile.any(), async (req, res) => {
    try {
        const success = await facebookController.FacebookMultipleImageShare(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('ererererererre <><>122<Route><>', error);
        res.status(error.status).send(error);
    }
});

namedRouter.all('/facebook*', auth.authenticateAPI);

/**
 * @api {post} /facebook/schedule Schedule posts in Facebook
 * @apiVersion 1.0.0
 * @apiGroup Facebook Page
 * @apiParam post Upload post
 * @apiParam publish_time Post Schedule Time 
 * @apiParam post_content User's Content or Message or caption
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "success": true
    },
    "message": "Media schedulled successfully."
}
*/
namedRouter.post('facebook.schedule', '/facebook/schedule', uploadFile.any(), async (req, res) => {
    try {
        const success = await facebookController.FacebookPostSchedule(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('ererererererre <><>122<Route><>', error);
        res.status(error.status).send(error);
    }
});


module.exports = router;