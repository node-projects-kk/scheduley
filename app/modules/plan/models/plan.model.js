const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["Active", "Inactive"];
const deletestatus = [false, true];

const PlanSchema = new Schema({
	stripe_plan_id: { type: String, default: '' },
	title: { type: String, default: '' },
	slug: { type: String, default: '' },
	price: { type:Schema.Types.Double, default:0},
	content: { type: String, default: '' },
	image:{type:String,default:''},
	status: { type: String, default: "Active", enum: status },
	inactiveDate: { type: Date, default: null },
	isDeleted: { type: Boolean, default: false, enum: deletestatus },
},{timestamps:true, versionKey: false });

// For pagination
PlanSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Plan', PlanSchema);