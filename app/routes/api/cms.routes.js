const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
// const multer = require('multer');
// const request_param = multer();

const cmsController = require('webservice/cms.controller');

/**
 * @api {get} /home Home Page Data
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5ced4d27bffe823e6bb69da3",
            "banner_block": {
                "banner_title": "Banner Title",
                "banner_image": "banner_image_1559134478822_banner.png"
            },
            "middle_content_block": {
                "small_content": "Middle",
                "post_content_header": "Post Content Header",
                "post_content_text": "Post Content Text",
                "post_content_right_image": "post_content_right_image_1559123809006_top-ten-logos.jpg",
                "post_content_middle_header": "Post Content Middle Header",
                "post_content_middle_text": "Post Content Middle Text",
                "post_content_middle_image": "post_content_middle_image_1559134478827_video.jpg",
                "post_content_lower_header": "Post Content Lower Heade",
                "post_content_lower_text": "Post Content Lower Text",
                "post_content_lower_feature": [
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "post_content_lower_feature[2][image]_1568802763603_1200px-NASA_logo.svg.png",
                        "header": "11111111111",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    },
                    {
                        "image": "",
                        "header": "11",
                        "description": "dd"
                    }
                ],
                "middle_content_logo": [
                    {
                        "logo": "middle_content_logo1_1559123808994_images_(1).jpeg"
                    },
                    {
                        "logo": "middle_content_logo2_1559123808994_images_(1).png"
                    },
                    {
                        "logo": "middle_content_logo3_1559123808995_images_(2).jpeg"
                    },
                    {
                        "logo": "middle_content_logo4_1559123808995_images_(4).jpeg"
                    },
                    {
                        "logo": "middle_content_logo5_1559123808996_images_(3).jpeg"
                    },
                    {
                        "logo": "middle_content_logo6_1559123808996_logo.png"
                    }
                ]
            },
            "company_content_block": {
                "header_title": "Company",
                "header_description": "Company Des",
                "post_content_middle_header": "middle com",
                "post_content_middle_text": "middle com des",
                "post_content_middle_image": "comapany_post_content_middle_image_1559134478836_phone.png"
            },
            "pricing_content_block": {
                "header_title": "prcinind",
                "header_text": "prcininddes",
                "header_pricing_business_image": "pricing_header_image_1559134478839_month.png",
                "post_content_middle_header": "Last Header",
                "post_content_middle_text": "P H Text"
            },
            "status": "active",
            "createdAt": "2019-05-28T11:40:57.467Z",
            "casestudies": [
                {
                    "logo_image": "logo_image_1559132150617_dymmy2.jpg",
                    "case_content": "Demo111",
                    "button_text": "test11111111",
                    "button_link": "https://www.google.co.in",
                    "status": "active",
                    "isDeleted": false,
                    "createdAt": "2019-05-28T09:05:14.221Z",
                    "_id": "5cee588b7c540c03da619320",
                    "__v": 0
                },
                {
                    "logo_image": "logo_image_1559140279790_avatar7.png",
                    "case_content": "sample content",
                    "button_text": "new button",
                    "button_link": "https://www.google.com/",
                    "status": "active",
                    "isDeleted": false,
                    "createdAt": "2019-05-29T14:30:14.742Z",
                    "_id": "5cee97b7890f4014598cac3f",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.home", '/home', function (req, res) {
    cmsController.homeList(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});


/**
 * @api {get} /feature Featured Page Data
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5cebc9bbbffe823e6ba54917",
            "banner_block": {
                "title": "Every feature you need to master social media marketing.",
                "image": "banner_image_1559049576675_f5.jpg"
            },
            "middle_content_block": {
                "post_title": "Posts Queue11",
                "post_content": "All your scheduled posts in one place. Drag and drop to reorder.",
                "calendar_title": "Calendar Planner",
                "calendar_content": "Easily view and plan your posts by week or month. Drag and drop to reorder.",
                "phone_title": "Phone Preview",
                "phone_content": "See how your posts and feed will look to your followers on a mobile.",
                "draftpost_title": "Draft Posts",
                "draftpost_content": "Half way through creating your posts and need to walk away? Save them as draft.",
                "post_left_image": "post_left_image_1559055468496_black-silver_business_card__large.jpg",
                "calendar_right_image": "calendar_right_image_1559048978752_1.jpg",
                "phone_left_image": "phone_left_image_1559049041941_man.png",
                "draftpost_right_image": "draftpost_right_image_1559049041943_hotstar-app.jpg"
            },
            "slider_block": {
                "left_title_1": "Upload Sources",
                "left_content_1": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "left_title_2": "Image editor",
                "left_content_2": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "left_title_3": "Post creator",
                "left_content_3": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "left_title_4": "Automated posting",
                "left_content_4": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "slider_files_one": "slider_files_one_1559055033627_Image_from_Skype1.jpg",
                "slider_files_two": "slider_files_1559052308734_img5.jpg",
                "slider_files_three": "slider_files_three_1559055868459_e325011770365ff5593d556755274ff4.jpg",
                "slider_files_four": "slider_files_four_1559055868460_26750a3797416f6f1245b67c46d971c7.jpg"
            },
            "power_tool_block": {
                "tool_heading": "Power Tools",
                "tool_content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "sub_title_1": "Multiple Accounts",
                "sub_content_1": "Manage multiple Instagram accounts from a single scheduley-iit login.",
                "sub_title_2": "Bulk Upload",
                "sub_content_2": "Upload upto 50 posts in one go. Auto set times, save all to your queue or drafts.",
                "sub_title_3": "Teams",
                "sub_content_3": "Collaborate with your team members and set permission for each account.",
                "sub_file_1": "sub_file_1_1559046405673_test6.png",
                "sub_file_2": "sub_file_2_1559055868462_642d9a82a03f88453e3ee6433a05e5ec.jpg",
                "sub_file_3": "sub_file_3_1559046405676_test3.jpg"
            },
            "try_block": {
                "try_title": "Try Scheduley Free for 14 Days11",
                "try_content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas orci tortor, tincidunt et nisi at laoreet maximus nulla. Cras ac ligula vulputate, pretium metus sed, bibendum diam.11",
                "try_button_text": "Get Started11"
            },
            "slug": "product-cms",
            "status": "active",
            "createdAt": "2019-05-27T11:40:57.467Z"
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.feature", '/feature', function (req, res) {
    cmsController.featureList(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {get} /about About Page Data
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5ced058cbffe823e6bb252ea",
            "title": "scheduley-iit about us",
            "short_description": "short descripton",
            "description": "long description",
            "image": "image_1568717373015_autumn.jpg",
            "sub_heading": {
                "title": "sub heading title",
                "description": "sub heading desc",
                "value": [
                    {
                        "value": 1,
                        "value_title": "value 1 title"
                    },
                    {
                        "value": 2,
                        "value_title": "value 2 title"
                    },
                    {
                        "value": 3,
                        "value_title": "value 3 title"
                    }
                ]
            },
            "about_title": "About Us312",
            "about_content": "Lorem Ipsum 123",
            "about_image": "about_image_1568717373087_nature-benefits.jpg",
            "how_it_works": {
                "title": "how it works",
                "description": "how it works desc",
                "step1": {
                    "title": "step 1",
                    "description": "step 1 desc"
                },
                "step2": {
                    "title": "step 2",
                    "description": "step 2 desc"
                },
                "step3": {
                    "title": "Step 3",
                    "description": "Step 3 desc"
                },
                "step4": {
                    "title": "step 4",
                    "description": "step 4 desc123"
                }
            },
            "try_block": {
                "try_title": "Try Scheduley Free for 14 Days",
                "try_content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas orci tortor, tincidunt et nisi at laoreet maximus nulla. Cras ac ligula vulputate, pretium metus sed, bibendum diam."
            },
            "slug": "about-cms",
            "status": "active",
            "createdAt": "2019-05-27T11:40:57.467Z",
            "team_data": [
                {
                    "_id": "5cecf9cabb7b742baaec2c87",
                    "firstName": "Test",
                    "lastName": "Demo",
                    "email": "test@demo.com",
                    "designation": "CEO",
                    "instagram": "Test Demo",
                    "facebook": "www",
                    "image": "image_1559034314203_featured-small-circular.jpg",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-05-28T09:05:14.221Z",
                    "updatedAt": "2019-05-29T06:41:00.466Z",
                    "__v": 0
                },
                {
                    "_id": "5cee5dc1b285d71855e01b25",
                    "firstName": "Archit1",
                    "lastName": "Singh1",
                    "email": "test@gmail.com1",
                    "designation": "Software Developer1",
                    "instagram": "insta1",
                    "facebook": "wwww1",
                    "image": "image_1559125673619_download.jpeg",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-05-29T10:24:01.388Z",
                    "updatedAt": "2019-05-29T10:27:53.640Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.about", '/about', function (req, res) {
    cmsController.aboutList(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

/**
 * @api {get} /faq Faq Page Data
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5ce7b5febffe823e6b814ae4",
            "question": "I am new to the business.",
            "answer": "It saves you a lot of work, as we're running the adsswqwqw",
            "status": "active",
            "is_deleted": false,
            "created_at": "2019-05-20T12:01:35.151Z",
            "__v": 0
        },
        {
            "_id": "5ce7c787bffe823e6b8224d8",
            "question": "Sample Question?",
            "answer": "Sample Answer....!",
            "status": "active",
            "is_deleted": false,
            "created_at": "2019-05-20T12:01:35.151Z",
            "__v": 0
        }
    ],
    "message": "Data Fetched Successfully"
}
*/
namedRouter.get("api.faq", '/faq', function (req, res) {
    cmsController.faqList(req).then(function (success) {
        res.status(success.status).send(success);
    }, function (failure) {
        res.status(failure.status).send(failure);
    });
});

module.exports = router;