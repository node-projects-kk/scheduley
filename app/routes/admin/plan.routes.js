const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const planController = require('plan/controllers/plan.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/plan/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});
const uploadFile = multer({
	storage: Storage,
	fileFilter: (req, file, cb) => {
		// accept image only
		if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
			return cb(new Error('Only image files are allowed!'), false);
		}
		cb(null, true);
	}
});
const request_param = multer();


namedRouter.all('/plan*', auth.authenticate);

/*
// @Route: CMS List
*/
namedRouter.get("plan.list", '/plan/list/:from?/:to?', request_param.any(), async (req, res) => {
	try {
		const success = await planController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword,
				'role': req.query.role
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('plan/views/list.ejs', {
			page_name: 'plan-management',
			page_title: 'Plan List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			from: req.params.from,
			to: req.params.to,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	}
});

/*
// @Route: Render Create CMS
*/
namedRouter.get("plan.create", '/plan/create', async (req, res) => {
	try {
		const success = await planController.create(req);
		return res.render('plan/views/add.ejs', {
			page_name: 'plan-management',
			page_title: 'Create Plan',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.redirect(namedRouter.urlFor('plan.list'));
	}
});

/*
// @Route: Create CMS Action
*/
namedRouter.post("plan.store", '/plan/store', uploadFile.any(), async (req, res) => {
	try {
		// console.log('hi..');
		const success = await planController.store(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	} catch (error) {
		
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('plan.create', {
			id: req.body.paired_user
		}));
	}
});

/*
// @Route: Render Edit CMS
*/
namedRouter.get("plan.edit", '/plan/edit/:id', async (req, res) => {
	try {
		const success = await planController.edit(req);
		return res.render('plan/views/edit.ejs', {
			page_name: 'plan-management',
			page_title: 'Update Plan',
			setting: req.session.setting,
			user: req.user,
			response: success
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update CMS Action
*/
namedRouter.post("plan.update", '/plan/update', uploadFile.any(), async (req, res) => {
	const planId = req.body.plan_id;
	try {
		const success = await planController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('plan.edit', {
			id: planId
		}));
	}
});

/*
// @Route: CMS status change
*/
namedRouter.post('plan.status.change', '/plan/status-change', request_param.any(), async (req, res) => {
	try {
		const success = await planController.statusChange(req);
		req.flash('success', success.message);
		return res.send(success);
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	}
});

/*
// @Route: Delete CMS
*/
namedRouter.get('plan.delete', '/plan/delete/:id', request_param.any(), async (req, res) => {
	try {
		const success = await planController.destroy(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	} catch (error) {
		req.flash('error', failure.message);
		return res.redirect(namedRouter.urlFor('plan.list'));
	}
});

// Export the express.Router() instance
module.exports = router;