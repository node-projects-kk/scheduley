var Q = require('q');
var faqRepo = require('faq/repositories/faq.repository');

/* @Method: list
// @Description: To get all the FAQ from DB
*/

exports.list = async req => {
    try {
        const sortOrder = {
            '_id': -1
        };

        const data = await faqRepo.getAll(req.query, sortOrder);
        
        return {
            "status": 200,
            "data": data,
            "message": "FAQ fetched successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* @Method: create
// @Description: render FAQ create page
*/
exports.create = async req => {
    try {
        return {
            "status": 200,
            data: [],
            "message": "Create FAQ fetched successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* @Method: store
// @Description: FAQ create action
*/

exports.store = async req => {
    try {
        const result_exist = await faqRepo.getByField({
            question: req.body.question
        });

        if (_.isEmpty(result_exist)) {
            const save_result = await faqRepo.save(req.body);
            return {
                status: 200,
                data: save_result,
                message: "FAQ saved successfully"
            }
        } else {
            return {
                status: 500,
                data: [],
                message: "This Question already exists, Please enter another question."
            }
        }
    } catch (error) {
        throw error;
    }
};

/*
// @Method: edit
// @Description:  render FAQ update page
*/
exports.edit = async req => {
    try {
        const data = await faqRepo.getById(req.params.id);
        return {
            "status": 200,
            data: data,
            "message": "Faq fetched successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: update
// @Description: FAQ update action
*/
exports.update = async req => {
    try {
        const result = await faqRepo.getByField({
            'question': req.body.question,
            '_id': {
                $ne: req.body.id
            }
        });

        if (_.isEmpty(result)) {
            const result1 = await faqRepo.updateById(req.body, req.body.id);
            return {
                "status": 200,
                data: result1,
                "message": "FAQ updated Successfully"
            };
        } else {
            return {
                "status": 500,
                data: [],
                "message": "This Question already exists"
            };
        }
    } catch (error) {
        throw error;
    }
};

/* @Method: Delete
// @Description: FAQ Delete action
*/
exports.delete = async req => {
    try {
        const result = await faqRepo.updateById({
            'is_deleted': true
        }, req.params.id);
        return {
            "status": 200,
            data: result,
            "message": "FAQ Deleted Successfully"
        };
    } catch (error) {
        throw error;
    }
};

/* @Method: status_change
// @Description: FAQ status change action
*/
exports.statusChange = async req => {
    try {
        const result = await faqRepo.getById(req.body.id);
        const updatedStatus = (_.has(result, 'status') && result.status == "inactive") ? "active" : "inactive";
        const result2 = await faqRepo.updateById({
            'status': updatedStatus
        }, req.body.id);
        return {
            "status": 200,
            data: result2,
            "message": "FAQ status has changed successfully"
        }
    } catch (error) {
        throw error;
    }
};