const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["Active", "Inactive"];
const deletestatus = [false, true];

const SocialFeedSchema = new Schema({
	user_id:{type: Schema.Types.ObjectId,ref: 'User',default: null},
	social:[{
		provider: {type: String,default: ''},
		token:{type:String,default:''},
		social_id:{type:String,default:''}
	}],
	status: {type: String,default: 'Active',enum: status},
	isDeleted: {type: Boolean,default: false,enum: deletestatus},
},{timestamps:true});

// For pagination
SocialFeedSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('SocialFeed', SocialFeedSchema);