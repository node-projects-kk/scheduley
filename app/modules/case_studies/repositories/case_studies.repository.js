const Case = require('case_studies/models/case_studies.model');
const perPage = config.PAGINATION_PERPAGE;

class CaseRepository {
	constructor() {}

	async getAll(searchQuery, sortOrder = {
		'_id': -1
	}) {
		try {
			const query = [{
				"isDeleted": false,
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'case_content': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};
			return await Case.aggregate([{
					"$sort": sortOrder
				},
				{
					$project: {
						_id: "$_id",
						case_content: "$case_content",
						logo_image: "$logo_image",
						status: "$status",
						isDeleted: "$isDeleted"
					}
				},
				{
					$match: searchQuery
				},
			]).exec();

			// return await Case.aggregatePaginate(aggregate, {
			// 	page: page,
			// 	limit: perPage
			// }); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			return await Case.create(data).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await Case.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Case.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Case.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllActiveTeamMembers(searchQuery, sortOrder = {
		'_id': -1
	}, page) {
		try {
			const query = [{
				"isDeleted": false,
				"status": "active"
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'firstName': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}
			}
			searchQuery = {
				"$and": query
			};
			const aggregate = Case.aggregate([{
					"$sort": sortOrder
				},
				{
					$project: {
						_id: "$_id",
						firstName: "$firstName",
						lastName: "$lastName",
						image: "$image",
						status: "$status",
						designation: "$designation",
						facebook: "$facebook",
						instagram: "$instagram",
						email: "$email",
						isDeleted: "$isDeleted",
					}
				},
				{
					$match: searchQuery
				},
			]);

			return await Case.aggregatePaginate(aggregate, {
				page: page,
				limit: perPage
			}); // { data, pageCount, totalCount }
		} catch (error) {
			return error;
		}
	}

	async getAllCaseStudy() {
		try {
			return await Case.find().exec();
		} catch (error) {
			return error;
		}
	}


}

module.exports = new CaseRepository();