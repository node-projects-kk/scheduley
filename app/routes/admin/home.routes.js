const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const homeController = require('home/controllers/home.controller');

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "./public/uploads/home/");
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({
	storage: Storage
});

const request_param = multer();

namedRouter.all('/home*', auth.authenticate);

/*
// @Route: Home List
*/
namedRouter.get("home.list", '/home/list', request_param.any(), async (req, res) => {
	try {
		const success = await homeController.list(req);
		let searchStr = {
			'keyword': '',
			'role': ''
		};
		success.search_param = "";
		if (_.isEmpty(req.query) == false && (_.has(req.query, "keyword") || _.has(req.query, "role"))) {
			searchStr = {
				'keyword': req.query.keyword
			};
			success.search_param = "&" + querystring.stringify(searchStr);
		}
		// For search string
		return res.render('home/views/list.ejs', {
			page_name: 'home-management',
			page_title: 'home List',
			user: req.user,
			setting: req.session.setting,
			postdata: searchStr,
			response: success
		});
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('home.list'));
	}
});

/*
// @Route: Render Edit Home
*/
namedRouter.get("home.edit", '/home/edit/:id', async (req, res) => {
	try {
		const success = await homeController.edit(req);
		return res.render('home/views/edit.ejs', {
			page_name: 'home-management',
			page_title: 'Update Home',
			user: req.user,
			setting: req.session.setting,
			response: success
		});
	} catch (error) {
		return res.status(failure.status).send(failure);
	}
});

/*
// @Route: Update Home Action
*/
namedRouter.post("home.update", '/home/update', uploadFile.any(), async (req, res) => {
	const homeId = req.body.home_id;
	try {
		const success = await homeController.update(req);
		req.flash('success', success.message);
		return res.redirect(namedRouter.urlFor('home.list'));
	} catch (error) {
		console.log('error', error);
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('home.edit', {
			id: homeId
		}));
	}
});



// Export the express.Router() instance
module.exports = router;