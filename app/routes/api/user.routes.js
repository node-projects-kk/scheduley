const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');

const userController = require('user/controllers/user.api.controller');

//const request_param = multer();
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if ((file.fieldname).indexOf('license') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/license')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/license');

            }
            cb(null, './public/uploads/license')
        } else {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/user')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/user');

            }
            cb(null, './public/uploads/user')
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})
const request_param = multer({
    storage: storage
});

/**
 * @api {post} /user/create Create a user
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam firstName First name
 * @apiParam lastName Last Name
 * @apiParam email Email Address
 * @apiParam password Password
 * @apiParam card_number Card Number
 * @apiParam exp_month Expiry Month
 * @apiParam exp_year Expiry Year
 * @apiParam cvc CVC
 * @apiParam plan Plan ID
 * @apiParam social_id Social Media ID
 * @apiParam regType [normal/facebook/google/pinterest/twitter/instagram]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d82153b6eb0a645aeae945b",
        "firstName": "Archit",
        "lastName": "Singh",
        "email": "architkumar.singh@webskitters.com",
        "stripe_subscription_id": "sub_FpgjJkhOyxSUr9",
        "stripe_plan_id": "plan_FpgjK3Dr639sKp",
        "stripe_customer_id": "cus_FpgjgXQiPfb41C"
    },
    "message": "Profile Created Suceessfully"
}
*/
namedRouter.post("api.user.create", '/user/create', request_param.any(), async (req, res) => {
	try {
		const success = await userController.create(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /user/signin Signin a user
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam email User's account Email
 * @apiParam password User's account Password
 * @apiParam social_id Social media id
 * @apiParam regType [normal/facebook/google/pinterest/twitter/instagram]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkODIxNTNiNmViMGE2NDVhZWFlOTQ1YiIsImlhdCI6MTU2ODgwNzAxNiwiZXhwIjoxNTY4ODkzNDE2fQ.Nv9rJw2lQ1uaVEhGKo0huPZpcJgYYrhHqlGHiCphjEw",
    "data": {
        "firstName": "Archit",
        "lastName": "Singh",
        "email": "architkumar.singh@webskitters.com",
        "password": "$2a$08$QrhBpM2XtP6Y5BOrEQLGCu/G9xSR0NSoDb9ZNjO6gHVBtTDogu536",
        "contactNumber": "123456",
        "role": [
            {
                "desc": "<p>This is the User role here.</p>\r\n",
                "roleType": "admin",
                "_id": "5cc302f59c4c3ac9c4937abf",
                "role": "user",
                "roleDisplayName": "User",
                "id": "5cc302f59c4c3ac9c4937abf"
            }
        ],
        "regType": "normal",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "stripe_subscription_id": "sub_FpgjJkhOyxSUr9",
        "stripe_customer_id": "cus_FpgjgXQiPfb41C",
        "stripe_plan_id": "plan_FpgjK3Dr639sKp",
        "isSubscribed": true,
        "isDeleted": false,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d82153b6eb0a645aeae945b",
        "plan": "5cd1ad8f61841b4402f029cd",
        "createdAt": "2019-09-18T11:30:03.675Z",
        "updatedAt": "2019-09-18T11:30:03.675Z",
        "__v": 0
    },
    "message": "You have successfully logged in"
}
*/
namedRouter.post("api.user.signin", '/user/signin', request_param.any(), async (req, res) => {
    try {
        const success = await userController.signin(req);
        res.status(success.status).send(success);
    } catch (error) {
        console.log("135", error);
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/forgotpassword Forgot Password
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam email User's account Email
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Password Reset Link Sent to your mail."
}
*/
namedRouter.post("api.user.forgotpassword", '/user/forgotpassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.forgotPassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /user/checkExpiryOfToken Check Expiry of Token
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam token Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "message": "Link active"
}
*/
namedRouter.post("api.user.checkExpiryOfToken", '/user/checkExpiryOfToken', request_param.any(), async (req, res) => {
    try {
        const success = await userController.checkExpiryOfToken(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/resetPassword Reset Password
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam token Token
 * @apiParam password New Password
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "message": "Password Changed Successfully."
}
*/
namedRouter.post("api.user.resetPassword", '/user/resetPassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.resetPassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


namedRouter.all('/users*', auth.authenticateAPI);


/**
 * @api {GET} /users/myprofile User's Profile
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5c9ce2a1a2737d5dd6c6fadb",
        "firstName": "Jerald",
        "lastName": "Laury",
        "email": "jerald.laury@gmail.com",
        "password": "$2a$08$txVW6ny3IhFp7hx6f593Le804.aDW1SV8oa1vnPV/rNmHxzBeJiQy",
        "contactNumber": "1234567890",
        "role": [
            {
                "_id": "5c94f263dc38e258151168bb",
                "role": "users",
                "desc": "<p>This is the Frontend user here.</p>\r\n",
                "roleDisplayName": "Users",
                "roleType": "frontend"
            }
        ],
        "regType": "normal",
        "otp_number": 2234,
        "profile_image": "",
        "deviceToken": "ghdfkjhg89756ghkj",
        "deviceType": "android",
        "token": "",
        "isDeleted": false,
        "status": "Active",
        "profileVerified": true,
        "emailVerified": true,
        "mobileVerified": false,
        "paid_financial_service": "no",
        "visit": 0,
        "createdAt": "2019-03-28T15:05:05.607Z",
        "__v": 0
    },
    "message": "Profile Info fetched Successfully"
}
*/

namedRouter.get('api.user.myprofile', '/users/myprofile', async (req, res) => {
    try {
        const success = await userController.getMyProfile(req);
        // console.log(success);
        res.status(success.status).send(success);
    } catch (error) {
        res,
        status(error.status).send(error);
    }
});

/**
 * @api {post} /users/updateprofile User's Profile Update
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam firstName First name
 * @apiParam lastName Last Name
 * @apiParam email Email Address (required)
 * @apiParam contactNumber Contact Number
 * @apiParam date_of_birth Date of Birth (yyyy-mm-dd)
 * @apiParam gender Gender 'male' or 'female'
 * @apiParam address Address
 * @apiParam net_worth Net Worth
 * @apiParam social_security_number Social Security Number
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "firstName": "Jerald",
        "lastName": "Laury",
        "email": "jerald.laury@gmail.com",
        "password": "$2a$08$txVW6ny3IhFp7hx6f593Le804.aDW1SV8oa1vnPV/rNmHxzBeJiQy",
        "contactNumber": "1234567890",
        "role": [
            {
                "desc": "<p>This is the Frontend user here.</p>\r\n",
                "roleType": "frontend",
                "_id": "5c94f263dc38e258151168bb",
                "role": "users",
                "roleDisplayName": "Users",
                "rolepermission": null,
                "user_data": null,
                "id": "5c94f263dc38e258151168bb"
            }
        ],
        "regType": "normal",
        "otp_number": 2234,
        "gender": "",
        "address": "",
        "latitude": "",
        "longitude": "",
        "net_worth": 0,
        "social_security_number": "",
        "profile_image": "",
        "deviceToken": "ghdfkjhg89756ghkj",
        "deviceType": "android",
        "token": "",
        "isDeleted": false,
        "status": "Active",
        "profileVerified": true,
        "emailVerified": true,
        "mobileVerified": false,
        "paid_financial_service": "no",
        "visit": 0,
        "_id": "5c9ce2a1a2737d5dd6c6fadb",
        "createdAt": "2019-03-28T15:05:05.607Z",
        "__v": 0,
        "date_of_birth": "2019-03-29T10:59:21.583Z"
    },
    "message": "Profile Info updated Successfully"
}
*/

namedRouter.post('api.user.updateprofile', '/users/updateprofile', request_param.any(), async (req, res) => {
    try {
        const success = await userController.updateProfile(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/changepassword Change User's Password
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiParam currentPassword user's Current password
 * @apiparam newPassword new password
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Password updated successfully"
}
*/
namedRouter.post("api.user.changepassword", '/users/changepassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/changeNameOrEmail Change User's Name Or Email
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiparam firstName [optional] new firstName
 * @apiparam lastName [optional] new lastName
 * @apiparam email [optional] new email
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "firstName": "123",
        "lastName": "321",
        "email": "4444",
        "password": "$2a$08$ct2e.kexkgdh.BRK24YwG.C0U1Io6fXHWb0gK/c9adQPDRoLeSmRu",
        "contactNumber": "",
        "role": [
            {
                "desc": "<p>This is the User role here.</p>\r\n",
                "roleType": "admin",
                "_id": "5cc302f59c4c3ac9c4937abf",
                "role": "user",
                "roleDisplayName": "User",
                "id": "5cc302f59c4c3ac9c4937abf"
            }
        ],
        "regType": "normal",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "token": "",
        "stripe_subscription_id": "sub_Fpirif3OXEVecX",
        "stripe_customer_id": "cus_FpirlSi9D4NuWW",
        "stripe_plan_id": "plan_Fpir1bsm0Oldt2",
        "isSubscribed": true,
        "isDeleted": false,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d82342b37d5515ee81a1b05",
        "plan": "5cd1ad8f61841b4402f029cd",
        "createdAt": "2019-09-18T13:42:03.644Z",
        "updatedAt": "2019-09-18T14:57:56.902Z",
        "__v": 0
    },
    "message": "Email updated successfully"
}
*/
namedRouter.post("api.user.changeNameOrEmail", '/users/changeNameOrEmail', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changeNameOrEmail(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/unsubscribe Cancel Subscription
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "firstName": "mrittika",
        "lastName": "basu",
        "email": "mrittika.basu@webskitters.com",
        "password": "$2a$08$4Bpp4Yli3WxG1Rc3n3CSDuzldrMLMfZNFQBAqqvKhv/yqxdWmRc.m",
        "contactNumber": "",
        "role": [
            {
                "desc": "<p>This is the User role here.</p>\r\n",
                "roleType": "admin",
                "_id": "5cc302f59c4c3ac9c4937abf",
                "role": "user",
                "roleDisplayName": "User",
                "rolepermission": null,
                "user_data": null,
                "id": "5cc302f59c4c3ac9c4937abf"
            }
        ],
        "regType": "normal",
        "social_id": "",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "token": "",
        "stripe_subscription_id": "",
        "stripe_customer_id": "",
        "stripe_plan_id": "plan_FrUunNhrIqxRmb",
        "isSubscribed": false,
        "isDeleted": false,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d8897869084235e01c36830",
        "plan": "5d8873349a0bc348890b908f",
        "createdAt": "2019-09-23T09:59:34.646Z",
        "updatedAt": "2019-09-23T14:52:16.051Z",
        "__v": 0
    },
    "message": "User unsubscribed"
}
*/
namedRouter.post("api.user.unsubscribe", '/users/unsubscribe', request_param.any(), async (req, res) => {
	try {
		const success = await userController.unsubscribe(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /users/deleteaccount Delete User Account
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "firstName": "mrittika",
        "lastName": "basu",
        "email": "mrittika.basu@webskitters.com",
        "password": "$2a$08$4Bpp4Yli3WxG1Rc3n3CSDuzldrMLMfZNFQBAqqvKhv/yqxdWmRc.m",
        "contactNumber": "",
        "role": [
            {
                "desc": "<p>This is the User role here.</p>\r\n",
                "roleType": "admin",
                "_id": "5cc302f59c4c3ac9c4937abf",
                "role": "user",
                "roleDisplayName": "User",
                "rolepermission": null,
                "user_data": null,
                "id": "5cc302f59c4c3ac9c4937abf"
            }
        ],
        "regType": "normal",
        "social_id": "",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "token": "",
        "stripe_subscription_id": "",
        "stripe_customer_id": "",
        "stripe_plan_id": "plan_FrUunNhrIqxRmb",
        "isSubscribed": false,
        "isDeleted": true,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d8897869084235e01c36830",
        "plan": "5d8873349a0bc348890b908f",
        "createdAt": "2019-09-23T09:59:34.646Z",
        "updatedAt": "2019-09-23T14:52:16.051Z",
        "__v": 0
    },
    "message": "Your account is deleted"
}
*/
namedRouter.post("api.user.deleteuseraccount", '/users/deleteaccount', request_param.any(), async (req, res) => {
	try {
		const success = await userController.deleteUserAccount(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.post("api.user.addcompany", '/users/addcompany', request_param.any(), async (req, res) => {
	try {
		const success = await userController.addCompany(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});


/**
 * @api {post} /users/updateProfileUser User's Profile Update Social
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam {string} name Social Media Name
 * @apiParam {string} access_token Social Media Access Token
 * @apiParam {string} expiry_time Access Token Expiry Time
 * @apiParam {string} twitter_id Social Media Name
 * @apiParam {string} page_id Page ID (only for facebook)
 * @apiParam {string} page_name Page Name (only for facebook)
 * @apiParam {string} page_access_token Page Access Token (only for facebook)
 * @apiParam {string} first_name First Name (only for pinterest)
 * @apiParam {string} last_name Last Name (only for pinterest)
 * @apiParam {string} pinterest_id User ID (only for pinterest)
 * @apiParam {string} board_name Board Name (only for pinterest)
 * @apiParam {string} board_url Board URL (only for pinterest)
 * @apiParam {string} board_id Board ID (only for pinterest)
 * @apiParamExample {json} Request-Example:
            {
                "facebook": {"name":"Archit", "facebook_id":28, "expiry_time": "12334", "access_token": "12356", "page_id": "123", "page_name": "ABC", "page_access_token": "3456w"}
                "twitter": {"name":"Archit", "twitter_id":28, "expiry_time": "12334", "access_token": "12356"}
                "pinterest": {"first_name":"Archit", "last_name":"Archit", "pinterest_id":28, "board_name": "12334", "access_token": "12356", "board_url": "231", "board_id": "123", "expiry_time": "1213"}
                "instagram": {"name":"Archit", "instagram_id":28, "expiry_time": "12334", "access_token": "12356"}
            }
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "facebook": {
            "name": "Archit",
            "access_token": "12356",
            "expiry_time": "12334",
            "facebook_id": "28"
        },
        "twitter": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "twitter_id": ""
        },
        "pinterest": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "pinterest_id": ""
        },
        "instagram": {
            "name": "",
            "access_token": "",
            "expiry_time": "",
            "instagram_id": ""
        },
        "firstName": "Navin",
        "lastName": "Vishwakarma",
        "email": "navin.vishwakarma@webskitters.com",
        "password": "$2a$08$Ua6YuwGVtufc4wNBlKrj/OOR68fKhE6yHibICIRymtTmvVepEEObK",
        "contactNumber": "",
        "role": [
            {
                "desc": "<p>This is the User role here.</p>\r\n",
                "roleType": "admin",
                "_id": "5cc302f59c4c3ac9c4937abf",
                "role": "user",
                "roleDisplayName": "User",
                "id": "5cc302f59c4c3ac9c4937abf"
            }
        ],
        "regType": "normal",
        "social_id": "",
        "promo_code": null,
        "profile_image": "",
        "no_of_account_access": 0,
        "deviceToken": "",
        "deviceType": "",
        "token": "",
        "stripe_subscription_id": "sub_Frw8z6EzVIWOXo",
        "stripe_customer_id": "cus_Frw5b9yPbcFkta",
        "stripe_plan_id": "plan_FrUunNhrIqxRmb",
        "isSubscribed": false,
        "isDeleted": false,
        "paymentStatus": false,
        "initialPaymentStatus": false,
        "status": "Active",
        "_id": "5d89fe1839da2f4d72ab455e",
        "plan": "5d8873349a0bc348890b908f",
        "createdAt": "2019-09-24T11:29:28.693Z",
        "updatedAt": "2019-09-25T08:45:13.443Z",
        "__v": 0
    },
    "message": "User updated Successfully"
}
*/

namedRouter.post('api.user.updateProfileUser', '/users/updateProfileUser', request_param.any(), async (req, res) => {
    try {
        const success = await userController.updateProfileUser(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

module.exports = router;