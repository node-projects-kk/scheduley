var Faq = require('faq/models/faq.model');
var perPage = config.PAGINATION_PERPAGE;
class faqRepository {
    constructor() {}

    async getAll(searchQuery, sortOrder) {
        try {
            const query = [{
                "is_deleted": false
            }];

            if (_.has(searchQuery, "keyword")) {
                if (searchQuery.keyword != '') {
                    var search_string = searchQuery.keyword.trim();
                    query.push({
                        "$or": [{
                            'question': {
                                '$regex': search_string,
                                '$options': 'i'
                            }
                        }]
                    });
                }
            }
            searchQuery = {
                "$and": query
            };
            return await Faq.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $match: searchQuery
                },
                {
                    $project: {
                        _id: "$_id",
                        question: "$question",
                        status: "$status",
                    }
                }

            ]).exec();
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await Faq.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            return await Faq.findOne(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            return await Faq.find(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Plan.findById(id).lean().exec();
            return await Plan.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Faq.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            return await Faq.create(data).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllFaq() {
        try {
            return await Faq.find().lean().exec();
        } catch (error) {
            return error;
        }
    }

};

module.exports = new faqRepository();