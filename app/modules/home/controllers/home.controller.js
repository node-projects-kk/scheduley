const homeRepo = require('home/repositories/home.repository');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var async = require('async');

/* 
// @Method: list
// @Description: To get all the Home from DB
*/
exports.list = async req => {
	try {
		const currentPage = req.query.page || 1;
		const data = await homeRepo.getAll(req.query, {
			'home.isDeleted': 1
		});
		return {
			"status": 200,
			data,
			"message": "Home List Fetched Successfully"
		};
	} catch (error) {
		throw error;
	}
};

exports.edit = async req => {
	try {
		const data = await homeRepo.getById(req.params.id);
		//console.log("30", JSON.stringify(data));
		return {
			"status": 200,
			data,
			"message": "Home Fetched Successfully"
		};
	} catch (error) {
		throw error;
	}
};


exports.update = async req => {
	try {
		const olddata = await homeRepo.getById(req.body.home_id);

		let bannerBlockObj = {};
		let middleContentBlockObj = {};
		let companyContentBlockObj = {};
		let pricingContentBlockObj = {};

		bannerBlockObj.banner_title = req.body.title;
		bannerBlockObj.banner_image = olddata.banner_block.banner_image;

		middleContentBlockObj.small_content = req.body.small_content;

		/* middleContentBlockObj.middle_content_logo1 = olddata.middle_content_block.middle_content_logo1;
		middleContentBlockObj.middle_content_logo2 = olddata.middle_content_block.middle_content_logo2;
		middleContentBlockObj.middle_content_logo3 = olddata.middle_content_block.middle_content_logo3;
		middleContentBlockObj.middle_content_logo4 = olddata.middle_content_block.middle_content_logo4;
		middleContentBlockObj.middle_content_logo5 = olddata.middle_content_block.middle_content_logo5;
		middleContentBlockObj.middle_content_logo6 = olddata.middle_content_block.middle_content_logo6; */

		middleContentBlockObj.post_content_header = req.body.post_content_header;
		middleContentBlockObj.post_content_text = req.body.post_content_text;

		middleContentBlockObj.post_content_right_image = olddata.middle_content_block.post_content_right_image;

		middleContentBlockObj.post_content_middle_header = req.body.post_content_middle_header;
		middleContentBlockObj.post_content_middle_text = req.body.post_content_middle_text;

		middleContentBlockObj.post_content_middle_image = olddata.middle_content_block.post_content_middle_image;
		middleContentBlockObj.post_content_lower_header = req.body.post_content_lower_header;
		middleContentBlockObj.post_content_lower_text = req.body.post_content_lower_text;

		middleContentBlockObj.post_content_lower_feature = [];
		middleContentBlockObj.middle_content_logo = olddata.middle_content_block.middle_content_logo;
		let temp = {};
		olddata.middle_content_block.post_content_lower_feature.map(function (item, index) {
			temp = {
				image : item.image,
				header : item.header,
				description : item.description
			}
			middleContentBlockObj.post_content_lower_feature.push(temp);
		});
		middleContentBlockObj.post_content_lower_feature = req.body.post_content_lower_feature;
			
/* 		middleContentBlockObj.post_content_lower_feature_logo1 = olddata.middle_content_block.post_content_lower_feature_logo1;
		middleContentBlockObj.post_content_lower_feature_logo2 = olddata.middle_content_block.post_content_lower_feature_logo2;
		middleContentBlockObj.post_content_lower_feature_logo3 = olddata.middle_content_block.post_content_lower_feature_logo3;
		middleContentBlockObj.post_content_lower_feature_logo4 = olddata.middle_content_block.post_content_lower_feature_logo4;
		middleContentBlockObj.post_content_lower_feature_logo5 = olddata.middle_content_block.post_content_lower_feature_logo5;
		middleContentBlockObj.post_content_lower_feature_logo6 = olddata.middle_content_block.post_content_lower_feature_logo6;
		middleContentBlockObj.post_content_lower_feature_logo7 = olddata.middle_content_block.post_content_lower_feature_logo7;
		middleContentBlockObj.post_content_lower_feature_logo8 = olddata.middle_content_block.post_content_lower_feature_logo8;

		middleContentBlockObj.post_content_lower_feature_header1 = req.body.post_content_lower_feature_header1;
		middleContentBlockObj.post_content_lower_feature_header2 = req.body.post_content_lower_feature_header2;
		middleContentBlockObj.post_content_lower_feature_header3 = req.body.post_content_lower_feature_header3;
		middleContentBlockObj.post_content_lower_feature_header4 = req.body.post_content_lower_feature_header4;
		middleContentBlockObj.post_content_lower_feature_header5 = req.body.post_content_lower_feature_header5;
		middleContentBlockObj.post_content_lower_feature_header6 = req.body.post_content_lower_feature_header6;
		middleContentBlockObj.post_content_lower_feature_header7 = req.body.post_content_lower_feature_header7;
		middleContentBlockObj.post_content_lower_feature_header8 = req.body.post_content_lower_feature_header8;

		middleContentBlockObj.post_content_lower_feature_description1 = req.body.post_content_lower_feature_description1;
		middleContentBlockObj.post_content_lower_feature_description2 = req.body.post_content_lower_feature_description2;
		middleContentBlockObj.post_content_lower_feature_description3 = req.body.post_content_lower_feature_description3;
		middleContentBlockObj.post_content_lower_feature_description4 = req.body.post_content_lower_feature_description4;
		middleContentBlockObj.post_content_lower_feature_description5 = req.body.post_content_lower_feature_description5;
		middleContentBlockObj.post_content_lower_feature_description6 = req.body.post_content_lower_feature_description6;
		middleContentBlockObj.post_content_lower_feature_description7 = req.body.post_content_lower_feature_description7;
		middleContentBlockObj.post_content_lower_feature_description8 = req.body.post_content_lower_feature_description8;
 */

		companyContentBlockObj.header_title = req.body.company_content_banner_title;
		companyContentBlockObj.header_description = req.body.company_content_header_description;
		companyContentBlockObj.post_content_middle_header = req.body.company_post_content_middle_header;
		companyContentBlockObj.post_content_middle_text = req.body.company_content_middle_description;

		companyContentBlockObj.post_content_middle_image = olddata.company_content_block.post_content_middle_image;

		pricingContentBlockObj.header_title = req.body.pricing_content_banner_title;
		pricingContentBlockObj.header_text = req.body.pricing_content_banner_description;

		pricingContentBlockObj.header_pricing_business_image = olddata.pricing_content_block.header_pricing_business_image;
		pricingContentBlockObj.post_content_middle_header = req.body.pricing_post_content_middle_header;
		pricingContentBlockObj.post_content_middle_text = req.body.pricing_content_middle_description;

		let uploadPath = 'public/uploads/home/';
		let uploadThumbPath = 'public/uploads/home/thumb/';

		console.log("133>>", req.files);
		//process.exit();
		let arr = [];
		
		if (_.has(req, 'files')) {
			if (req.files.length > 0) {
				
				
				var arrval = [];
				req.files.forEach(function (file) {
					console.log("file",file);
					if (file.fieldname.toLowerCase() == "banner_image") {
						if (!_.isEmpty(olddata.banner_block)) {
							if (fs.existsSync(uploadPath + olddata.banner_block.banner_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.banner_block.banner_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.banner_block.banner_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.banner_block.banner_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						bannerBlockObj.banner_image = file.filename;
					}
					for(var s = 0; s < 6; s++){
						if (file.fieldname.toLowerCase() == "middle_content_logo["+s+"][logo]") {
							if (!_.isEmpty(olddata.middle_content_block)) {
								if (fs.existsSync(uploadPath + olddata.middle_content_block.middle_content_logo[s].logo)) {
									const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.middle_content_logo[s].logo);
								}
								if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.middle_content_logo[s].logo)) {
									const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.middle_content_logo[s].logo);
								}
							}
							gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							middleContentBlockObj.middle_content_logo[s].logo = file.filename;
						}
					}

					if (file.fieldname.toLowerCase() == "post_content_right_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.post_content_right_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.post_content_right_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.post_content_right_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.post_content_right_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.post_content_right_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "post_content_middle_image") {
						if (!_.isEmpty(olddata.middle_content_block)) {
							if (fs.existsSync(uploadPath + olddata.middle_content_block.post_content_middle_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.post_content_middle_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.post_content_middle_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.post_content_middle_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						middleContentBlockObj.post_content_middle_image = file.filename;
					}
					
					
					for(var i=0; i<8; i++){
					//for(s=0; s<middleContentBlockObj.post_content_lower_feature.length; s++){
						if (file.fieldname.toLowerCase() == "post_content_lower_feature["+ i +"][image]") { console.log("A");
							if (!_.isEmpty(olddata.middle_content_block.post_content_lower_feature) && olddata.middle_content_block.post_content_lower_feature[i].image != "") {
								if (fs.existsSync(uploadPath + olddata.middle_content_block.post_content_lower_feature[i].image)) {
									const upl_resume = fs.unlinkSync(uploadPath + olddata.middle_content_block.post_content_lower_feature[i].image);
								}
								if (fs.existsSync(uploadThumbPath + olddata.middle_content_block.post_content_lower_feature[i].image)) {
									const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.middle_content_block.post_content_lower_feature[i].image);
								}
							}
							gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							
							middleContentBlockObj.post_content_lower_feature[i].image = file.filename;
							console.log(file.filename);
							arrval.push(i);
						}
						//else{ console.log("B");
						//	middleContentBlockObj.post_content_lower_feature[i].image = req.body.post_content_lower_feature[i]['hidimage'];
						//}
					}
					//console.log("ppp",middleContentBlockObj);
					if (file.fieldname.toLowerCase() == "comapany_post_content_middle_image") {
						if (!_.isEmpty(olddata.company_content_block)) {
							if (fs.existsSync(uploadPath + olddata.company_content_block.post_content_middle_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.company_content_block.post_content_middle_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.company_content_block.post_content_middle_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.company_content_block.post_content_middle_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						companyContentBlockObj.post_content_middle_image = file.filename;
					}

					if (file.fieldname.toLowerCase() == "pricing_header_image") {
						if (!_.isEmpty(olddata.pricing_content_block)) {
							if (fs.existsSync(uploadPath + olddata.pricing_content_block.header_pricing_business_image)) {
								const upl_resume = fs.unlinkSync(uploadPath + olddata.pricing_content_block.header_pricing_business_image);
							}
							if (fs.existsSync(uploadThumbPath + olddata.pricing_content_block.header_pricing_business_image)) {
								const upl_thumb_resume = fs.unlinkSync(uploadThumbPath + olddata.pricing_content_block.header_pricing_business_image);
							}
						}
						gm(uploadPath + file.filename).resize(100).write(uploadThumbPath + file.filename, function (err) {
							if (err) throw new Error(err.message);
						});
						pricingContentBlockObj.header_pricing_business_image = file.filename;
					}

				});
			}
		}
		
		for(var y=0; y<8; y++){
			if (!_.contains(arrval,y)) {
				middleContentBlockObj.post_content_lower_feature[y].image = req.body.post_content_lower_feature[y]['hidimage'];
			}
		}		
		
		req.body.banner_block = bannerBlockObj;
		req.body.middle_content_block = middleContentBlockObj;
		
		
		console.log("301", req.body.middle_content_block); //process.exit();
		
		
		req.body.company_content_block = companyContentBlockObj;
		req.body.pricing_content_block = pricingContentBlockObj;

		

		const result = await homeRepo.updateById(req.body, req.body.home_id);
		return {
			"status": 200,
			data: result,
			"message": "Home updated Successfully"
		};
	} catch (error) {
		throw error;
	}
};