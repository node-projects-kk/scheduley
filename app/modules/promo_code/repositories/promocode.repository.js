const PromoCode = require('promo_code/models/promocode.model');
const perPage = config.PAGINATION_PERPAGE;

class PromoCodeRepository {
	constructor() {}

	async getAll(searchQuery, sort, from, to) {
		try {
			const query = [{
				"isDeleted": false
			}];

			if (_.has(searchQuery, "keyword")) {
				if (searchQuery.keyword != '') {
					const search = searchQuery.keyword.trim();
					query.push({
						"$or": [{
							'title': {
								'$regex': search,
								'$options': 'i'
							}
						}]
					});
				}

			}
			searchQuery = {
				"$and": query
			};

			// console.log("19>>", searchQuery);
			return await PromoCode.aggregate([{
					"$lookup": {
						"from": "plans",
						"localField": "plan_id",
						"foreignField": "_id",
						"as": "plan_details"
					},
				},
				{
					$unwind: {
						path: "$plan_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "transactions",
						"localField": "_id",
						"foreignField": "promo_code",
						"as": "user_promo_info"
					},
				},
				{
					$unwind: {
						path: '$user_promo_info',
						preserveNullAndEmptyArrays: true
					}
				},
				// {
				// 	$match: searchQuery
				// },
				{
					$project: {
						'date': {
							$dateToString: {
								format: "%Y-%m-%d",
								date: "$createdAt"
							}
						},
						'title': 1,
						'status': 1,
						'slug': 1,
						'price': 1,
						'promo_count': 1,
						"promo_count": 1,
						"expiry_date": 1,
						"createdAt": 1,
						"isDeleted": 1,
						"promo_type": 1,
						"plan": 1,
						'user_promo_info': 1,
					}
				},
				{
					$match: {
						"date": {
							$gte: from
						},
						"date": {
							$lte: to
						},
					}
					// searchQuery,
				},
				{
					$group: {
						"_id": "$_id",
						"title": {
							$first: "$title"
						},
						"status": {
							$first: "$status"
						},
						"slug": {
							$first: "$slug"
						},
						"price": {
							$first: "$price"
						},
						"promo_count": {
							$first: "$promo_count"
						},
						"expiry_date": {
							$first: "$expiry_date"
						},
						"createdAt": {
							$first: "$createdAt"
						},
						"isDeleted": {
							$first: "$isDeleted"
						},
						"promo_type": {
							$first: "$promo_type"
						},
						"plan": {
							$first: "$plan_details"
						},
						'user_promo_info': {
							$addToSet: "$user_promo_info"
						},
						"date": {
							$first: "$date"
						}
					}
				},
			]).exec();
		} catch (error) {
			console.log("126", error)
			return error;
		}
	}

	async getById(id) {
		try {
			return await PromoCode.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			return await PromoCode.findOne(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			return await PromoCode.find(params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Plan.findById(id).lean().exec();
			return await PromoCode.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await PromoCode.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getCmsCount() {
		try {
			return await PromoCode.count({
				'status': "Active"
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new PromoCode(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}


	async getPromoCodeUsageCount() {
		try {
			return await PromoCode.aggregate([{
					"$lookup": {
						"from": "users",
						"localField": "_id",
						"foreignField": "promo_code",
						"as": "user_promo_info"
					},
				},
				{
					$unwind: '$user_promo_info'
				},
				{
					$group: {
						'_id': '$_id',
						"promo_type": {
							$first: "$promo_type"
						},
						"title": {
							$first: "$title"
						},
						"slug": {
							$first: "$slug"
						},
						"price": {
							$first: "$price"
						},
						"promo_count": {
							$first: "$promo_count"
						},
						"user_id": {
							$first: "$user_id"
						},

						"plan_id": {
							$first: "$plan_id"
						},
						"status": {
							$first: "$status"
						},
						"isDeleted": {
							$first: "$isDeleted"
						},
						"createdAt": {
							$first: "$createdAt"
						},
						"updatedAt": {
							$first: "$updatedAt"
						},
						'user_promo_info': {
							$addToSet: '$user_promo_info'
						}
					}
				}
			]).exec();
		} catch (error) {
			return error;
		}
	}





}

module.exports = new PromoCodeRepository();