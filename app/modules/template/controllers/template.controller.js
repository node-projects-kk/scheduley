const Q = require('q');
const template_Repo = require('template/repositories/template.repository');
const userRepo = require('user/repositories/user.repository');
const _ = require("underscore");
const perPage = config.PAGINATION_PERPAGE;
nodemailer = require("nodemailer");
const slug = require('slug');

/* @Method: list
// @Description: To get all the template from DB
*/
exports.list = async req => {
    try {
        const currentPage = req.query.page || 1
        const sortOrder = {
            '_id': -1
        };
        const getAll = await template_Repo.getAll(req.query, sortOrder, currentPage);
        // console.log(getAll);
        return {
            "status": 200,
            data: getAll.data,
            current: currentPage,
            pages: Math.ceil(getAll.data.total_pages / config.PAGINATION_PERPAGE),
            "message": "Template Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* @Method: create
// @Description: render template create page
*/

exports.create = async (req, res) => {
    try {
        return {
            "status": 200,
            data: [],
            "message": "Template Fetched Successfully"
        };
    } catch (error) {
        throw error;
    }
};


/* @Method: store
// @Description: template create action
*/
exports.store = async req => {
    console.log('controller', req.body);
    try {
        const templateByField = await template_Repo.getByField({
            title: req.body.title
        });

        if (_.isEmpty(templateByField)) {
            req.body.slug = slug(req.body.title, {
                lower: true,
                replacement: '-',
                symbols: true
            });

            const result = await template_Repo.save(req.body);

            return ({
                "status": 200,
                data: result,
                "message": "Template created successfully"
            });

        } else {
            throw new Error("Template already exists. Please try another.");
        }

    } catch (error) {
        throw error;
    }
};

/*
// @Method: edit
// @Description:  render template update page
*/
exports.edit = async req => {
    try {
        const result = await template_Repo.getById(req.params.id);
        return ({
            "status": 200,
            data: result,
            "message": "Template Fetched Successfully"
        });
    } catch (error) {
        throw error;
    }
};

/* @Method: update
// @Description:template update action
*/
exports.update = async req => {
    try {
        const result1 = await template_Repo.getByField({
            title: req.body.title,
            _id: {
                $ne: req.body.template_id
            }
        })
        if (_.isEmpty(result1)) {
            const result = await template_Repo.updateById(req.body, req.body.template_id);
            return ({
                "status": 200,
                data: result,
                "message": "Template Updated Successfully"
            });
        } else {
            throw new Error('Template already exists. Please try another.');
        }

    } catch (error) {
        throw error;
    };
}
/* @Method: Delete
// @Description: Template Delete action
*/
exports.delete = async req => {
    try {
        const result = await template_Repo.updateById({
            is_deleted: true
        }, req.params.id)

        return ({
            "status": 200,
            data: result,
            "message": "Template Deleted Successfully"
        });

    } catch (error) {
        throw error;
    }
};
/*
// @Method: status_change
// @Description: template status change action
*/
exports.status_change = async req => {
    try {
        const result1 = await template_Repo.getById(req.body.id);

        let serviceStatus = (result1.status === 'Active') ? 'Inactive' : 'Active';

        const result2 = await template_Repo.updateById({
            'status': serviceStatus
        }, req.body.id);

        return ({
            "status": 200,
            data: result2,
            "message": "Template status has changed successfully"
        });
    } catch (error) {
        throw error;
    }
};

/*
// @Method: send_email
// @Description: Send Email action
*/
exports.send_email = async req => {
    try {
        const allTemplates = template_Repo.getAllTemplate();
        if (err)
            return new Error({
                "status": 500,
                data: [],
                "message": err
            });

        const allusers = userRepo.getAllUsers(req.user);

        if (err)
            deferred.reject({
                "status": 500,
                data: [],
                "message": err2
            });

        deferred.resolve({
            "status": 200,
            data: {
                templates: allTemplates,
                users: allusers
            },
            "message": "All data fetched successfully"
        });
    } catch (error) {
        throw error;
    }
};