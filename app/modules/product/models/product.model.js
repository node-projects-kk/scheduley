const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["active", "inactive"];

const ProductSchema = new Schema({
	banner_block: {
		title: {
			type: String,
			default: ''
		},
		image: {
			type: String,
			default: ''
		}
	},
	middle_content_block: {
		post_left_image: {
			type: String,
			default: ''
		},
		post_title: {
			type: String,
			default: ''
		},
		post_content: {
			type: String,
			default: ''
		},
		calendar_right_image: {
			type: String,
			default: ''
		},
		calendar_title: {
			type: String,
			default: ''
		},
		calendar_content: {
			type: String,
			default: ''
		},
		phone_left_image: {
			type: String,
			default: ''
		},
		phone_title: {
			type: String,
			default: ''
		},
		phone_content: {
			type: String,
			default: ''
		},
		draftpost_right_image: {
			type: String,
			default: ''
		},
		draftpost_title: {
			type: String,
			default: ''
		},
		draftpost_content: {
			type: String,
			default: ''
		}
	},
	slider_block: {
		left_title_1: {
			type: String,
			default: ''
		},
		left_content_1: {
			type: String,
			default: ''
		},
		left_title_2: {
			type: String,
			default: ''
		},
		left_content_2: {
			type: String,
			default: ''
		},
		left_title_3: {
			type: String,
			default: ''
		},
		left_content_3: {
			type: String,
			default: ''
		},
		left_title_4: {
			type: String,
			default: ''
		},
		left_content_4: {
			type: String,
			default: ''
		},
		slider_files_one: {
			type: String,
			default: ''
		},
		slider_files_two: {
			type: String,
			default: ''
		},
		slider_files_three: {
			type: String,
			default: ''
		},
		slider_files_four: {
			type: String,
			default: ''
		}
	},
	power_tool_block: {
		tool_heading: {
			type: String,
			default: ''
		},
		tool_content: {
			type: String,
			default: ''
		},
		sub_title_1: {
			type: String,
			default: ''
		},
		sub_content_1: {
			type: String,
			default: ''
		},
		sub_file_1: {
			type: String,
			default: ''
		},
		sub_title_2: {
			type: String,
			default: ''
		},
		sub_content_2: {
			type: String,
			default: ''
		},
		sub_file_2: {
			type: String,
			default: ''
		},
		sub_title_3: {
			type: String,
			default: ''
		},
		sub_content_3: {
			type: String,
			default: ''
		},
		sub_file_3: {
			type: String,
			default: ''
		}
	},
	try_block: {
		try_title: {
			type: String,
			default: ''
		},
		try_content: {
			type: String,
			default: ''
		},
		try_button_text: {
			type: String,
			default: ''
		}
	},
	slug: {
		type: String,
		default: ''
	},
	status: {
		type: String,
		default: "active",
		enum: status
	},
	createdAt: {
		type: Date,
		default: Date.now(),
	}
});

// For pagination
ProductSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Product', ProductSchema);