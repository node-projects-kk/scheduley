define({
  "name": "Scheduley API Documentation",
  "version": "1.0.0",
  "description": "API Scheduley Manager",
  "url": "http://111.93.167.180:1395/api",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-09-27T14:29:54.997Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
