const teamRepo = require('team_management/repositories/team.repository');
const async = require('async');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
var slugify = require('slugify');

/* 
// @Method: list
// @Description: To get all the teams from DB
*/
exports.list = async req => {
	try {
		const currentPage = req.query.page || 1;
		const {
			data,
			pageCount,
			totalCount
		} = await teamRepo.getAllActiveTeamMembers(req.query, {
			'team.isDeleted': 1
		}, (req.query.page || 1));
		return {
			"status": 200,
			data,
			current: currentPage,
			pages: pageCount,
			"message": "team list fetched successfully"
		};
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error
		};
	}
};